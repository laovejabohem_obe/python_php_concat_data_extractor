#this scripts get the content of administration yii2 file and filter the "this->registerJs/registerCss" for extract inner code and variables, this with the goal of a smooth refactorization of the main code
import re
from pprint import pprint
from re import search

from InputAndDataInsertion import InputAndDataInsertion

class TextNewStandarsProcessors:
        already_created_arr = []
        final_input_str = []
        general_line_replace_order={}
        final_array_inputs=[]
        def __init__(self,path):
            self.path = path
        def getFileReferenceForInnerLineProcessing(self):
            with open(self.path,"r") as f:
                counter = 1
                printOpened = False
                phpvarcontent=''
                for  line in f:
                    #open this->registerJs/registerCss" tag and not closed in the same line
                    if(printOpened != True):
                        if (self.hasRegisterOpeningTag(line) and not self.hasEndConst(line) ) :
                            printOpened = True
                    #open this->registerJs/registerCss" tag and closed in the same line
                        elif( self.hasRegisterOpeningTag(line) and self.hasEndConst(line) and not self.hasCommentaryStart(line) ):
                            printOpened = False
                            phpvarcontent = self.getSubstringFromRegExp("[uU]rl::to\((.*?)\]\)|Yii::t\(\'(.*?)\', (.*?)'\)|[\"|\'][\"|\'](.*?)(\s\$[a-zA-Z0-9_]+\s)(.*?)[\'|\"][\'|\"]|[\'|\"](.*?)\$(.*?)[\'|\"]",line,counter,False)
                    else:
                        phpvarcontent = ""
                        #search for closing line for this->registerJs/registerCss"
                        if(bool(re.search("^if",line))):
                            phpvarcontent = self.getSubstringFromRegExp('\((.*)\)',line,counter,False)

                            inputs = self.createInputString(phpvarcontent)
                            self.replaceLineInFile(phpvarcontent['line_str'],inputs,phpvarcontent['line'])
                            counter+=1
                            continue
                        if(bool(re.search('[uU]rl::to\((.*?)\]\)',line))):
                            phpvarcontent=self.getSubstringFromRegExp('[uU]rl::to\((.*?)\]\)',line,counter,False)

                            inputs = self.createInputString(phpvarcontent)
                            self.replaceLineInFile(phpvarcontent['line_str'],inputs,phpvarcontent['line'])
                            if(phpvarcontent != ''):
                                if(len(phpvarcontent['match']) > 0):
                                    inputs = self.createInputString(phpvarcontent)
                                    self.replaceLineInFile(phpvarcontent['line_str'],inputs,phpvarcontent['line'])
                            counter+=1
                            continue
                        phpvarcontent = self.getSubstringFromRegExp("Yii::t\(\'(.*?)\', (.*?)'\)|[\"|\'][\"|\'](.*?)\s\$[a-zA-Z0-9_]+\s(.*?)[\'|\"][\'|\"]|[\'|\"](.*?)\$(.*?)[\'|\"]",line,counter,False)
                        if(phpvarcontent != ''):
                            if(len(phpvarcontent['match']) > 0):
                                inputs = self.createInputString(phpvarcontent)
                                self.replaceLineInFile(phpvarcontent['line_str'],inputs,phpvarcontent['line'])
                    counter+=1
                    continue
            self.changeActualFileContent()
        def changeActualFileContent(self):
                with open(self.path,"r+") as f:
                    lines = f.readlines()
                    for line_number in self.general_line_replace_order.keys():
                        lines[line_number - 1] = self.general_line_replace_order[line_number]
                with open(self.path,'w+') as f:
                    f.writelines( lines )
                with open(self.path,'r') as f:
                    filer=f.read()
                    finalScript = self.createScript(re.findall('\$this\-\>registerJs\([\s\S]*?[D|Y]\)\;',filer))
                    filer = re.sub('\$this\-\>registerJs\([\s\S]*?[D|Y]\)\;','',filer)
                    filer = filer + "\n" + finalScript
                with open(self.path,'w+') as f:
                    f.writelines( filer )
        def getSubstringFromRegExp(self,regexp,string,line,individual_mode=True):
            if(individual_mode):
                try:
                    found = re.match(regexp,string)
                    found = found.group()
                    return {'match':found,'line_str':string,'line':line}
                except Exception as e:
                    return e
            else:
                try:
                    if(re.findall(regexp,string)):
                        found = {'match':[],'line_str':string,'line':line,'str_start':0,'str_end':0}
                        for match in re.finditer(regexp,string,re.IGNORECASE):
                            s = match.start()
                            e = match.end()
                            found['match'].append(string[s:e])
                            found['str_start'] = s
                            found['str_end'] = e
                        m = re.match(regexp,string)
                        return found
                    else:
                        return {'match':[]} 
                except Exception as e:
                    return e
                pass

        def hasRegisterOpeningTag(self,string):
            if(search("\$this->registerJs\('",string) or search("\$this->registerCss\('",string)):
                return True
            else:
                return False
        def containsInnerVarsInsideRegisterTag(self,string):
            if(search("\'(.)\. (.*')",string)):
                return True
            else:
                return False
        def hasEndConst(self,string):
            if(search("view\:\:(.*)\)",string) or search("View\:\:(.*)\)",string)):
                return True
            else:
                return False
        def hasUrlInIt(self,string):
            if(search("Url::to\((.*)]\)",string)):
                return True
            else:
                return False
        def hasTraductionTag(self,string):
            if(search("Yii::t\(\'(.*)\', (.*)'\)",string)):
                return True
            else:
                return False
        def hasEndingConstantAndCuotes(self,string):
            if(search("\'\,View\:\:(.*\)\;)",string)):
                return True
            else:
                return False
        def hasCommentaryStart(self,string):
            if(search("//",string)):
                return True
            else:
                return False
        def innerVarsCuantity(self,string):
            results = re.findall("[uU]rl::to\((.*?)\]\)|Yii::t\(\'(.*?)\', (.*?)'\)",string)
            return len(results)
        def removeLineFromConcatFromLine(line,startSubStr,finishSubStr):
            
            return 0
        def camel_case(self,s):
            s = re.sub(r"(_|-)+", " ", s).title().replace(" ", "")
            return ''.join([s[0].lower(), s[1:]])

        def createInputString(self,found):
            inputs =[]

            for match in found['match']:

                if(bool(re.search("Yii::t(.*?)",match))):
                    found = re.findall("\,(.*?)\)",match)
                    escaped = match.strip()
                    found = re.sub("\'|\?|\.",'',found[0])
                    found = self.camel_case(found)
                    if(self.ifItsAlreadySet(found)):
                        continue
                    else:
                        self.already_created_arr.append(found)
                    final_str ="<input name='%(input_name)s' id='%(input_name)s' type='hidden' value='<?php echo %(php_value)s?>'> </input>" % {"input_name" : found.strip(), "php_value" : match.strip()}
                    inputs.append({'final_input_str':final_str,'var_name' : found.strip(),'substr_escaped':escaped})
                    self.final_array_inputs.append(final_str)
                    
                elif(bool(re.search("[uU]rl",match))):
                    found = re.findall("\[[\'|\"](.*?)[\'|\"]",match)

                    escaped = match
                    found = re.sub("(\/|\/|\-)",' ',found[0])
                    found = re.sub("\.",'',found)
                    found = self.camel_case(found)
                    if(self.ifItsAlreadySet(found)):
                        continue
                    else:
                        self.already_created_arr.append(found)
                    final_str ="<input name='%(input_name)s' id='%(input_name)s' type='hidden' value='<?php echo %(php_value)s?>'> </input>" % {"input_name" : found.strip(), "php_value" : match.strip()}
                    inputs.append({'final_input_str':final_str,'var_name' : found.strip(),'substr_escaped':escaped})
                    self.final_array_inputs.append(final_str)
                    
                elif(bool(re.search("\$[a-zA-Z0-9_]+",match))):
                    found = re.findall("\$[a-zA-Z0-9_]+",match)

                    found = re.sub("\$",'',found[0])
                    found = re.sub("\.",'',found) 
                    found = re.sub("\"",'',found) 
                    found = re.sub("\'",'',found) 
                    found = self.camel_case(found)
                    escaped =found
                    if(self.ifItsAlreadySet(found)):
                        continue
                    else:
                        self.already_created_arr.append(found)
                    match = re.findall('\s\$(.*?)\s',match)
                    final_str ="<input name='%(input_name)s' id='%(input_name)s' type='hidden' value='<?php echo $%(php_value)s?>'> </input>" % {"input_name" : found.strip(), "php_value" : match[0]}
                    inputs.append({'final_input_str':final_str,'var_name' : found.strip(),'substr_escaped':escaped})
                    self.final_array_inputs.append(final_str)
            return inputs
        def ifItsAlreadySet(self,varName):
            return varName in self.already_created_arr
        def replaceLineInFile(self,line,inputs,line_in_file):
            for input in inputs:
                table = str.maketrans({"-":r"\-",
                 "]":  r"\]",
                 "\\": r"\\",
                "^":  r"\^",
                 "$":  r"\$",
                 "*":  r"\*",
                 ".":  r"\.",
                '(':r'\(',
                ')':r'\)',
                '[':r'\[',
                ']':r'\]',
                ']':r'\]',
                "'":r"\'",
                "?":r"\?",
                })
                input['substr_escaped']= input['substr_escaped'].translate(table)
                regexp='[[\'|\"][\'|\"]\s*(%(str_scaped)s)\s*[\'|\"][\'|\"]' % {'str_scaped':input['substr_escaped']}
                if(re.search('[u|U]rl',input['substr_escaped'])):
                    regexp='[\'|\"][\'|\"]\s*\.\s*(%(str_scaped)s)\s*\.\s*[\'|\"][\'|\"]' % {'str_scaped':input['substr_escaped']}
                register_data_file = open('register_data.txt','w+')
                register_data_file.write(line)
                past_line = line
                line = re.sub(regexp,input['var_name'],line)
                print("line")
                print(line)
                if(bool(re.search("if",line))):
                    input['substr_escaped'] = re.findall('\((.*?)\)',line)
                    input['no_var'] = True
                    if(self.checkIfItsInReplaceOrders(line_in_file)):
                        self.general_line_replace_order[line_in_file] =line
                if(line == past_line):
                    regexp='[\'|\"]\s*\.\s*(.*?)\s*\.\s*[\'|\"]'
                    if(re.search('[u|U]rl',input['substr_escaped'])):
                        regexp='[\'|\"][\'|\"]\s*\.\s*(%(str_scaped)s)\s*\.\s*[\'|\"][\'|\"]' % {'str_scaped':input['substr_escaped']}
                    line = re.sub(regexp,input['var_name'],line)
                    line = re.sub("\"|\'",'',line)
                print(line)
                
            if(self.checkIfItsInReplaceOrders(line_in_file)):
                self.general_line_replace_order[line_in_file] =line
            return 0
        def createScript(self,allRegisteredJs):
            inputAndDataInsertion = InputAndDataInsertion(self.path,self.final_array_inputs,self.already_created_arr,allRegisteredJs)
            script = inputAndDataInsertion.createScriptContent()
            return script
        def checkIfItsInReplaceOrders(self,line):
            return line not in self.general_line_replace_order