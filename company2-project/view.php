<?php

use yii\helpers\Html;
use common\models\ProjectCategory;
use common\models\ProjectClass;
use common\models\ProjectType;
use common\models\UserSeasonState;



$this->title = Yii::t('app', 'View PID:  ', [
    'modelClass' => 'Contractor Project',
]) . $model->p_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contractor Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 m-b-30 header-title">
                <b><?= Html::encode($this->title) ?></b>
            </h4>

            <div class="company2-project-update">

                <?= $this->render('_view', [
                    'model'              => $model,
                    'userModel'          => $userModel,
                    //'userRecordModel'    => $userRecordModel,
                    'countries'          => $countries,
                    'seasons'            => $seasons,
                    'region'             => $region,
                    'cities2'            => $cities2,
                    'city2s'             => $city2s,
                    'zipcode'            => $zipcode,
                    'zipcode2'           =>$zipcode2,
                    'client'             => $client,
                    'project_position'   => $project_position,
                    'state'              => $state,
                    'user_city'          => $user_city,
                    'allow_project'      => $allow_project,
                    'project_category'     => ProjectCategory::getProjectCategoryDropDrown(ProjectCategory::COMPANY2,true),
                    'project_class'  => ProjectClass::getProjectClassDropDrown(ProjectClass::COMPANY2,true),
                    'project_type'         => ProjectType::getProjectTypeDropDrown(ProjectType::COMPANY2,true),
                    'clientModel'        => $clientModel,
                    'user_city2'         => $user_city2,
                    'seasonstate' => UserSeasonState::getSeasonStateDropDrownByUserType($model->client->season_id),



                ]) ?>

            </div>
        </div>
    </div>
</div>