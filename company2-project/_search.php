<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//if(Yii::$app->user->can('AS Staff')){$this->registerCss('.search .input-group-sm select.form-control{max-width: 110px !important;}');}

?>
<?php echo Html::a('<i class="fa fa-filter"></i> Filters <i class="fa fa-caret-up angle"></i>', 'javascript:void(0)', ['id' => 'toggle-search-form', 'class' => 'text-center m-t-10 btn btn-primary btn-sm']); ?>
<div id="search-form" class="search row justify-content-start form-group m-b-10">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'layout' => 'inline',
        'options' => ['data-pjax' => '1'],
    ]); ?>
    <?= html::hiddenInput('sort', Yii::$app->request->get('sort')) ?>
    <?= html::hiddenInput('page', Yii::$app->request->get('page')) ?>
    <?= Yii::$app->request->get('lastWeek') ? html::hiddenInput('lastWeek', Yii::$app->request->get('lastWeek')) : "" ?>
    <?= html::hiddenInput('company2-project', Yii::$app->request->get('company2-project')) ?>
    <?= Html::activeHiddenInput($model, 'is_outside_claim'); ?>

    <div class="input-group">
        <?= html::dropDownList('per_page', (Yii::$app->session['common_perpage'] ? Yii::$app->session['common_perpage'] : Yii::$app->params['defaultPageSize']), Yii::$app->params['per-page'], ['onchange' => '$(this).submit()', 'class' => 'form-control form-control-sm']) ?>
        <?php if (Yii::$app->user->can('AS Staff')) : ?>
            <?= html::dropDownList('parent_user_id', Yii::$app->request->get('parent_user_id'), $parent_users, ['prompt' => Yii::t('app', 'P A Company'), 'onchange' => '$(this).submit()', 'class' => 'form-control form-control-sm']) ?>
        <?php endif; ?>
        <?= html::activeDropDownList($model, 'season_id', $seasons,         ['prompt' => Yii::t('app', 'Season'),     'onchange' => 'season_entity_filter(this)', 'class' => 'form-control form-control-sm season-entity-filter']) ?>
        <?= html::activeDropDownList($model, 'region_id', $regions, ['prompt' => Yii::t('app', 'Region'), 'onchange' => 'season_entity_filter(this)', 'class' => 'form-control form-control-sm season-entity-filter']) ?>
        <?= html::activeDropDownList($model, 'city_id', $cities, ['prompt' => Yii::t('app', 'City'), 'onchange' => 'season_entity_filter(this)', 'class' => 'form-control form-control-sm season-entity-filter']) ?>
        <?= html::activeDropDownList($model, 'zipcode_id', $zipcodes,     ['prompt' => Yii::t('app', 'Zip Code'),   'onchange' => 'season_entity_filter(this)', 'class' => 'form-control form-control-sm season-entity-filter']) ?>
        <?php if (!Yii::$app->user->can('Staff1')) : // no need to display for staff1 user
        ?>
            <?= html::activeDropDownList($model, 'staff1_id',     $staff1es,     ['prompt' => Yii::t('app', 'Staff1'),     'onchange' => 'season_entity_filter(this)', 'class' => 'form-control form-control-sm season-entity-filter']) ?>
        <?php endif; ?>
        <?= html::activeDropDownList($model, 'client_id', $clients,             ['prompt' => Yii::t('app', 'Client'),       'onchange' => 'season_entity_filter(this)', 'class' => 'form-control form-control-sm season-entity-filter']) ?>
        <?= html::activeDropDownList($model, 'project_class_id', $project_class, ['prompt' => Yii::t('app', 'Project Class'), 'onchange' => '$(this).submit()', 'class' => 'form-control form-control-sm']) ?>
         <?= html::activeDropDownList($model, 'id_status', $project_status, ['prompt' => Yii::t('app', 'Status'), 'onchange' => '$(this).submit()', 'class' => 'form-control form-control-sm']) ?>
        <?= html::activeDropDownList($model, 'is_active', ['1' => Yii::t('app', 'Active'), '0' => Yii::t('app', 'Inactive')], ['prompt' => Yii::t('app', 'Status'), 'onchange' => '$(this).submit()', 'class' => 'form-control form-control-sm']) ?>
        <?= html::activeTextInput($model, 'search_str', ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Search by Name, Last Name, DOB, Phone #, or Email', 'placeholder' => Yii::t('app', 'Search'), 'onchange' => '$(this).submit()', 'class' => 'form-control form-control-sm']) ?>

        <div class="input-group-append">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary btn-sm']) ?>
            <?php $is_outside_pclaom = (Yii::$app->request->get('Company1ClaimSearch')['is_outside_pclaom']) ? (Yii::$app->request->get('Company1ClaimSearch')['is_outside_claim']) : 0; ?>
            <?= Html::a(Yii::t('app', 'Reset'), ['/company2-project/index', 'Company1ClaimSearch[is_active]' => '1', 'Company1ClaimSearch[is_outside_claim]' => $is_outside_claim, 'company2-project' => Yii::$app->request->get('company2-project')], ['class' => 'btn btn-default btn-sm', 'data-pjax' => '0']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <div class="clearfix"></div>
</div>