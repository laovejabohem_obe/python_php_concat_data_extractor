<?php

use backend\components\UserHelpers;
use common\models\ClaimTask;
use common\models\Company2Project;
use common\models\Company2ProjectTask;
use common\models\Module;
use common\models\ProjectCategory;
use common\models\ProjectClass;
use common\models\ProjectsAttachments;
use common\models\ProjectStatus;
use common\models\ProjectTask;
use common\models\ProjectType;
use common\models\Task;
use common\models\UserSeasonState;
use common\models\ProjectMessages;
use backend\assets\Company2ProjectBundle;

use kartik\editable\Editable;
use kartik\grid\GridView;
use kartik\popover\PopoverX;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\PhaseChecker;
use yii\widgets\Spaceless;
use common\models\CompanyGeneralUrlHandler;
if (Yii::$app->user->can('AS Staff'))
    $this->title = Yii::t('app', 'Contractor Projects');
else {
    $status = 'Manage';
    $status = ((Yii::$app->request->get('company2-project') == 'acp') ? 'Active' : $status);
    $status = ((Yii::$app->request->get('company2-project') == 'icp') ? 'Inactive' : $status);
    $status = ((Yii::$app->request->get('company2-project') == 'ncp') ? 'New' : $status);
    $status = ((Yii::$app->request->get('company2-project') == 'ocp') ? 'Outside' : $status);


    $this->title = Yii::t('app', '{status} Projects', ['status' => $status]);
}
$this->params['breadcrumbs'][] = $this->title;

$messages = Yii::$app->request->get('message');
$message =  str_replace(";", "", $messages);

$id = Yii::$app->request->get('id');
$total = Yii::$app->request->get('total');
$condition_total = Yii::$app->request->get('condition_total');
$color = Yii::$app->request->get('color');
 $task_projects = Task::find()->joinWith('module')
                            ->joinWith('module.department')
                            ->where(['type'=>2])
                            ->andWhere(['task.is_active' => 1])
                            //->all();
                            //->orderby([Task::tableName().'.task_order'])
                            ->orderBy([
                                          Module::tableName().'.module_order' => SORT_ASC,
                                          Task::tableName().'.task_order' => SORT_ASC,

                                        ])->all();

        
$task_project = ArrayHelper::map($task_projects, 'id', function ($task_projects, $defaultValue) {

                 Html::tag('b', Html::encode($task_projects->module->name));
                     return $task_projects->name;
                      }, 'module.name');
                            //var_dump($task_project); die();*/

                     /* $task_project = ArrayHelper::map($task_projects, 'id', 'name');*/
                            //var_dump($task_project); die();*/
/*
$phase = new PhaseChecker();
var_dump($phase->projectStageCheck(null,Yii::$app->request->queryParams['id'] ));
die();
*/

/*
echo '<pre>';
var_dump($task_projects[0]->id);
var_dump($task_projects[0]->name);
echo '</pre>';
die();
*/




Company2ProjectBundle::register($this);
?>
<style type="text/css">
    @media (max-width:1550px) {
        .project_stat_icons_containers {
            flex-wrap: wrap;
        }

        .project_stat_icons {
            margin: 8 !important;
        }
    }

    .project_stat_icons {
        width: auto !important;
    }
</style>
<div class="row">
    <div class="col-md-12 col-lg-4 col-xl-3 side_data_container">
        <?php  //this section its exclusive for the use of this folder main script
        ?>
        <input type="hidden" id="mainTextsIndex1" value="<?php echo Yii::t('app', 'Are you sure update this Note?'); ?>">
        <input type="hidden" id="activeProjectAlert" value="<?php echo Yii::t('app-sweet_alert', 'You want to Activate this Project?') ?>">
        <input type="hidden" id="desactiveProjectAlert" value="<?php echo Yii::t('app-sweet_alert', 'You want to Deactivate this Project?') ?>">
        <input type="hidden" id="url_project_notes" value="<?php echo  Url::to(["/company2-project/change-project-notes"]) ?>">

        <input type="hidden" id="changeStatusUrl" value="<?php echo Url::to(["/company2-project/change-status"]) ?>">
        <input type="hidden" id="alertMainText1" value="<?php echo Yii::t('app-sweet_alert', 'Are you sure?') ?>">
        <input type="hidden" id="alertMainText2" value="<?php echo Yii::t('app-sweet_alert', 'Project is Activated')
                                                        ?>">
        <input type="hidden" id="alertMainText3" value="<?php echo Yii::t('app-sweet_alert', 'Project is Deactivated') ?>">
        <input type="hidden" id="alertMainText4" value="<?php echo Yii::t('app-sweet_alert', 'Project Activated Successfully.') ?>">
        <input type="hidden" id="alertMainText5" value="<?php echo Yii::t('app-sweet_alert', 'Project Deactivated Successfully.') ?>">

        <input type="hidden" id="project_status_options" value="<?php echo json_encode($project_status) ?>">
        <?php  //end of section
        ?>



        <?php echo $this->render('_search2', ['model' => $searchModel,
         'parent_users' => $parent_users,
         'seasons' => $seasons,
         'regions' => $regions,
         'cities2' => $cities2,
         'zipcodes' => $zipcodes,
         'staff1es' => $staff1es,
         'clients' => $clients,
          'project_class' => $project_class,
          'project_status' => $project_status,
          'id' => $id,
          'message' => $message,
          'statistics_status' =>$statistics_status,
          'total' => $total,
          'dataProvider' => $dataProvider]); ?>




    </div>
    <div class="col-md-12 col-lg-8 col-xl-9">




        <?php

        //var_dump($model->id); die();
        if ($model->id > 0) {

            //var_dump($model);die();
            echo $this->render('detail', [
                'model' => $model,
                'id' => $model->id,
                'searchModel11' => $searchModel11,
                'dataProvider22' => $dataProvider22,
                'searchModel22' => $searchModel22,
                'dataProvider2' => $dataProvider2,
                'searchModel3' => $searchModel3,
                'dataProvider3' => $dataProvider3,
                'project_priority' => $project_priority,
                'project_status' => $project_status,
                'message_form' => $message_form,
                'user_id' => $user_id,
                'file_project' => $file_project,
                'due_date' => $due_date,
                'count_message' => $count_message,
                'company_name' => $company_name,
                'parent_id' => $parent_id,
                'depart' => $depart,
                'staff_other' => $staff_other,
                'task_project' => $task_project


            ]);
        } else {

            if (Yii::$app->user->can('Company2 Staff')) {

                $add = '<div align="center">' . Html::a('<i class="fa fa-plus">&nbsp;</i><b>' . Yii::t('app', 'Add Project'), ['/client/createcompany2project'], ['class' => 'btn-sm btn btn-info', 'title' => Yii::t('app', 'Add Project'), 'data-toggle' => 'tooltip', 'data-pjax' => '0',]) . "</div>";
            } else {

                $add = "";
            }

            echo ' <div class="card">
            <div class="card-header" align="center"><h4>Select Project</h4>
            <div class="row" align="center">
            
           
            </div>  
        </div>
        <div class="card-body padding-zero" style="padding-top:0px; max-height:600px; ">
<div align="center"><h4>' . $add . '</h4></div></div></div>';
        }

        ?>
    </div>
</div>
</div>
</div>



<script type="text/javascript">
    function hola(a, d) {
        //a is project_id and b = task_id c = type d = baseurl
        var window_width = 750;
        var window_height = 830;
        var newfeatures = 'scrollbars=yes,resizable=yes';
        var window_top = (screen.height - window_height) / 2;
        var window_left = (screen.width - window_width) / 2;


        newWindow = window.open("" + d + "/uploads/company2project-task-documents/" + a + "" + '?dt=' + new Date().getTime(), '_blank', 'titulo', 'width=' + window_width + ',height=' + window_height + ',top=' + window_top + ',left=' + window_left + ',features=' + newfeatures + '', 'clearcache=yes').focus;

    }
</script>

<?php

$this->registerJs('
var projectJsStatusArray = ' . json_encode($project_status) . ';
', View::POS_END);
?>