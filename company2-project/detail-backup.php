<?php

use Carbon\Carbon;
use common\models\CompanyDepartments;
use common\models\DepartmentStaff;
use common\models\ProjectsAttachments;
use common\models\User;
use kartik\file\FileInput;
use kartik\grid\GridView;
use kartik\select2\Select2;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use backend\components\UserHelpers;


/* @var $this yii\web\View */
/* @var $searchModel app\models\Company2ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$js = <<<JS
$('form#{$message_form->formName()}').on('beforeSubmit', function(e) {
    $('.button--submit').html('Please wait...');
});
JS;

$this->registerJs($js, View::POS_READY, 'dataFormSubmit');



$this->registerJs($js, View::POS_READY, 'dataFormSubmit');

$this->title = 'Projects Supports';
$this->params['breadcrumbs'][] = $this->title;

$user_id = Yii::$app->user->identity->id;



?>
<style type="text/css">


.p {
    width: 400px;
    margin: 0px auto !important;
    padding-top:auto !important;
    padding-bottom:auto !important;
    border: solid 0px #f60;
    font: normal 13px arial, helvetica, sans-serif;
    line-height: 25px;
}

.form-group {
    margin-bottom: 0rem !important;
}

.mt-2 {
    margin-top: 0px !important;
}

.padding-div{

    padding-top:15px;
}


/*element.style {
    position: relative;
    overflow: hidden;
    width: auto;
    height: 500.828px !important;
}*/

</style>



<div class="projects-support-index">
  
     
          
      
             

                    <?php Pjax::begin(['id' => 'projects-support-index']); ?>



                    <?php Pjax::end(); ?>
              
              



          
        <!-- 
<div align="left">
   <br>
   <p><b><font size=3>Overview :</font></b></p>
   <p><font size=2><?php $model->claim_description ?></font></p>
</div>  -->

   



<!-- message -->
                    <div class="row">
                        <div class="col-md-1 padding-div" align="center">
                            <?php echo  Html::a(
                                Html::img(
                                    UserHelpers::getProjectClassImage(32, 32, $model->project_class_id, true),
                                    [
                                        'class' => 'rounded-circle logo-marketing user_avatar',
                                        'style' => 'width:50px;height: 50px; 
                                                border: 1px solid #C00;
                                                border-color:#ebeff2;box-shadow:0 0 0 0
                                                !important;-moz-box-shadow: 0 0 0
                                                0;display:block; margin-left: auto;
                                                margin-right: auto;',
                                        'alt' => 'user-img', 'title' => Yii::t('app', "Project Class"), 'data-toggle' => 'tooltip'
                                    ]
                                )
                            ) . "<br>" . Html::a($model->quantity . " " . $model->projectClass->unit_measure, [
                                '/company2-project/update',
                                'id' => $model->id
                            ]) . ' ';?>
                        </div>
                        <div class="col-md-2 padding-div" align="left">
                            <b class="font-gray">Project P_ID</b><br>
                            <?= $model->p_id ?>
                        </div>
                        <div class="col-md-2 padding-div" align="left">
                            <b class="font-gray">Inspection Date</b><br>
                            <?= Yii::$app->formatter->asDate($model->inspection_date, 'php:m-d-Y') ?>
                        </div>
                        <div class="col-md-2 padding-div" align="left">
                            <b class="font-gray">Start Date</b><br>
                            <?= Yii::$app->formatter->asDate($model->created_at, 'php:m-d-Y') ?>
                        </div>
                        <div class="col-md-2 padding-div" align="left">
                            <b class="font-gray">End Date</b><br>
                            <?= Yii::$app->formatter->asDate($model->due_date, 'php:m-d-Y') ?>
                        </div>
                        <div class="col-md-3 padding-div" align="center">
                            <b class="font-gray">Actions</b><br>
                            <div class="btn-group btn-group-sm">
                            <?= Html::a('<i class="fa ' . ($model->is_active == '1' ? "fa-check" : "fa-close") . '"></i>', 'javascript:void(0)', ['class' => 'btn btn-sm btn-custom ' . ($model->is_active == '1' ? "btn-success" : "btn-danger"), 'onclick' => 'change_status_project(' . $model->id . ', ' . ($model->is_active == '1' ? "0" : "1") . ')', 'title' => $model->projectReverseStatusName, 'data-toggle' => 'tooltip']);?>
                            <?= Html::a('<i class="icon-materials-pallet"></i>', 
                                    ['/material-project/index-material', 
                                    'project_id' => $model->id,
                                    'project_pid' => $model->p_id,
                                    'asDialog' => '1'],
                                     ['class'       => 'btn btn-sm btn-custom btn-primary ','onclick'     => 'return showModal("' . Yii::t('app', 'ADD MATERIALS') . '", $(this).attr("href"), 600, 900); return false;',
                                     'title' => Yii::t('app', 'Add Material'),
                                     'data-toggle' => 'tooltip'
                                 ]);?>
                            <?= Html::a('<i class="icon-contractor"></i>', 
                                    ['/project-affiliate/index-affiliate', 
                                    'project_id' => $model->id,
                                    'project_pid' => $model->p_id,
                                    'asDialog' => '1'],
                                     ['class'       => 'btn btn-sm btn-custom btn-primary ','onclick'     => 'return showModal("' . Yii::t('app', 'ASSIGN PROJECT') . '", $(this).attr("href"), 600, 900); return false;',
                                     'title' => Yii::t('app', 'Assign Projects'),
                                     'data-toggle' => 'tooltip'
                                 ]);?>
                            <?php  if($model->id_status == 5)
                                {
                                    echo Html::a('<i class="fas fa-eye"></i>', ['view', 'id' => $model->id], ['class' => 'btn btn-sm btn-custom btn-primary', 'title' => Yii::t('app', 'View Projects'), 'data-pjax' => '0', 'data-toggle' => 'tooltip']);
                                }else{

                                  
                                    echo Html::a('<i class="fas fa-pencil-alt"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-custom btn-primary', 'title' => Yii::t('app', 'Update Projects'), 'data-pjax' => '0', 'data-toggle' => 'tooltip']);
                                }?>
                            <?php 
                                    $base = common\models\AdministrationHelper::getSiteUrl();
                                    $base = "'".$base."'";
                                    $file = "'".$documents->file."'";

                        $claimTaskDocuments= ProjectsAttachments::find()
                        ->joinWith(company2ProjectTask)
                        ->where(['project_id'=>$model->id])
                        ->all();
                    

                                   $printFormsHtml = '<ul>';

                                    foreach ($claimTaskDocuments as $key => $documents) {

                                        $name_d = str_replace ( '_', ' ', $documents->file);

                                        if ($documents->file != "") {
                                            $file = "'" . $documents->file . "'";

                                            $printFormsHtml .=
                                                '
                                                <div class="row pop">
                                                    <div class="col-8">
                                                        <li><font size=2>'.$name_d.'</font>
                                                     </div>
                                                     <td class="col-4" align="right">' . Html::a('<i class="glyphicon glyphicon-eye-open"></i>', Url::to('#'), [ 'class' => 'btn btn-sm btn-custom btn-info','onclick' => 'hola(' . $file . ',' . $base . ')', 'data-pjax' => '0', 'title' => Yii::t('app', 'View Document'), 'data-toggle' => 'tooltip']) . Html::a('<i class="dripicons-download"></i>', ['company2-project-task/download-document', 'document' => $documents->file, 'name' => $documents->file], ['class' => 'btn btn-sm btn-custom btn-info','data-pjax' => '0', 'title' => Yii::t('app', 'Download Document'), 'data-toggle' => 'tooltip']) 
                                                  . '</div>
                                                </div>
                                                </li>';
                                        }
                                    }

                                    $title = '<div class="row" style="border:0px">
                                        <div class="col-10">
                                           View Documents
                                        </div>
                                        <div class= "col-1 style="text-align:right">
                                            <span class="text-info"><strong><button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project" >X</button></strong></span> 
                                        </div>
                                    </div>';

                                    $printFormsHtml .= '</ul>';

                                   
                    echo Html::a('<i class="fa fa-download fa-2" data-toggle="tooltip" data-placement="top" title="View and Download Documents"></i>', 'javascript:void(0)', [
                                'class'          => 'btn btn-sm btn-custom btn-primary',
                                'title'          => $title,
                                    'data-toggle'    => 'tooltip',
                                    'tabindex'       => '0',
                                    'data-container' => 'body',
                                    'data-toggle'    => 'popover',
                                    'data-trigger'   => 'click focus',
                                    'data-placement' => 'auto',
                                    'data-html'      => 'true',
                                    'data-content'   => $printFormsHtml,
                                    ]);
                            ?>
                            <?php  $printFormsHtml =
                                    '<ul>'.
                                     '<li>'. Html::a('Inspection Form', 
                                                ['/print-form/inspectionform', 
                                                'project_id' => $model->id,'client_id'=>$model->client->id, 'type'=>'contractor'
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'Inspection Form'), 'data-toggle' => 'tooltip'
                                            ]) .
                                        '</li>'.
                                        '<li>' . Html::a('Service Agreement',
                                                [
                                                    '/print-form/index','id' => $model->id, 'user_id' => $model->user_id, 'form' => 11
                                                ],
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                            ]) . 
                                        '</li>' .
                                          '<li>' . Html::a(
                                            'Third Party Authorization',
                                            [
                                                '/print-form/index',
                                                'id' => $model->id, 'user_id' => $model->user_id, 'form' => 3
                                            ],
                                            ['data-pjax' => '0', 'title' => Yii::t('app', 'Third Party Authorization'), 'data-toggle' => 'tooltip']
                                        ) . '</li>' .
                                        '<li>'. Html::a('Material Selection', 
                                                [
                                                    '/print-form/index', 'id' => $model->id,'user_id'=>$model->user_id, 'form'=> '5'
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                            ]) .
                                        '</li>'.
                                        '<li>'. Html::a('Referral Form', 
                                                ['/print-form/index', 'id' => $model->id,'user_id'=>$model->user_id, 'form'=>9
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                            ]) .
                                        '</li>'. 
                                        /*'<li>'. Html::a('Lient Acknowledgement', 
                                                ['/print-form/index', 'id' => $model->id,'user_id'=>$model->user_id, 'form'=>8
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                            ]) .
                                        '</li>'. */
                                        '<li>'. Html::a('Certificate of Completion', 
                                                ['/print-form/index', 
                                                'id' => $model->id,'user_id'=>$model->user_id, 'form'=>7
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                            ]) .
                                        '</li>'.

                                        '<li>' . Html::a(
                                            'Invoice',
                                            /*[
                                                '/print-form/index',
                                                'id' => $model->id, 'user_id' => $model->user_id, 'form' => 8
                                            ],
                                            ['data-pjax' => '0', 'title' => Yii::t('app', 'Invoice'), 'data-toggle' => 'tooltip']*/
                                        ) . '</li>' .
                                       
                                    '</ul>'; 

                                    $title = '<div class="row" style= "border:0px;">
                                        <div class="col-9">
                                            Create Documents
                                        </div>
                                        <div class= "col-1 style="text-align:right">
                                            <span class="text-info"><strong><button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project" >X</button></strong></span> 
                                        </div>
                                    </div>';

                             
                                   
                                    echo  Html::a('<i class="fas fa-file-signature fa-2" data-toggle="tooltip" data-placement="top" title="Create Documents"></i>', 'javascript:void(0)', [
                                    'class'          => 'btn btn-sm btn-custom btn-primary ',
                                    'title'          => $title,
                                    'data-toggle'    => 'tooltip',
                                    'tabindex'       => '0',
                                    'data-container' => 'body',
                                    'data-toggle'    => 'popover',
                                    'data-trigger'   => 'click focus',
                                    'data-placement' => 'auto',
                                    'data-html'      => 'true',
                                    'data-content'   => $printFormsHtml,
                                ]);?>
                                 <?= Html::a('<i class="fas fa-sign-in-alt"></i>', ['site/login-2', 'user_id' => $model->staff_id], ['data-method' => 'post', 'class' => 'btn btn-sm btn-custom btn-info', 'title' => Yii::t('app', 'Signin as this Project Supervisor'), 'data-pjax' => '0', 'data-toggle' => 'tooltip']);?>

                        </div>
                    </div>
                </div>
                    <hr>
                     <!--<div align="left">
                        Discussion: <?php $count_message ?> Messages                     </div>
                </div>-->


                <!-- end form -->
  

           
                        <!-- <div id="slimscrollleft" style="overflow-y:scroll;height:440px; width:auto;" class="chatMessage"> -->
                <div class="scrollDown simplebar-content slimscrollleft " style="max-height:500px;" name ="divu" id ="divu">


                    <?= GridView::widget([
                        'dataProvider' => $dataProvider2,
                        //'filterModel' => $searchModel,
                        'class' => 'table',
                        'responsive' => true,
                        'responsiveWrap' => false,
                        'persistResize' => false,
                        'showHeader' => false,
                        'summary' => '',
                        'rowOptions'   => function ($model, $key)use($id) 
                        {

                           
                                return ['class' => 'table-white',
                                        'style' => 'max-height:400px; !important'
                                    ];

                           
                        },
                        'columns' => [

                            [

                                'attribute'      => 'sender_id',
                                'headerOptions' => ['style' => ['text-align' => 'center']],
                                'label'          => Yii::t('app', 'Created By'),
                                'headerOptions' => ['style' => 'word-wrap: break-word; width: 100%;'],
                                //'contentOptions' => ['class' => 'text-right'],
                                //'contentOptions' => ['style' => 'word-wrap: break-word; width: 85%; white-space: normal;'],
                                'contentOptions' =>

                                function ($model, $index, $widget, $grid) use ($user_id) {

                                    if ($user_id == $model->sender_id) {
                                        $result =   ['style' => 'text-align: right; word-wrap: break-word; width: 100%; white-space: normal; border: 1px solid white;'];
                                    } else {
                                        $result =  ['style' => 'text-align: left; word-wrap: break-word; width: 100%; white-space: normal; border: 1px solid white;'];
                                    }

                                    //var_dump($result); die();

                                    return $result;
                                },
                                'content'        => function ($model) use ($user_id) {
                                    $users = User::find()->where(['id' => $model->sender_id])->one();

                                    $department = CompanyDepartments::find()->where(['staff_id' => $model->sender_id])->one();

                                    //var_dump($model->message); die();

                                    $message_new_replace =  str_replace("<p", "<span", $model->message);
                                    $message_new =  str_replace("</p", "</span", $message_new_replace);

                                    //$message_new =  $model->message;


                                    $user_type = $users->type_id;

                                    if ($user_type == 1) {

                                        $user_send = "<font color= #00B0F0>" . $users->first_name . " " . $users->last_name . "</font>";
                                        $message = "<font color=#00B0F0>" . $message_new . "</font>";
                                    }

                                    if ($user_type == 5 || $user_type == 3) {

                                        $user_send = "<font color= #33E5BC>" . $users->first_name . " " . $users->last_name . "</font>";
                                        $message = "<font color=#33E5BC>" . $message_new . "</font>";
                                    } elseif ($user_type == 2 && $department->staff_id == $model->sender_id) {
                                        $user_send = "<font color= #81C872>" . $users->first_name . " " . $users->last_name . "</font>";
                                        $message = "<font color=#81C872>" . $message_new . "</font>";
                                    } elseif ($user_type == 2 && $model->sender_id != $department->staff_id && Yii::$app->user->identity->id != $model->sender_id) {
                                        $user_send = "<font color= #7266BA>" . $users->first_name . " " . $users->last_name . "</font>";
                                        $message = "<font color=#7266BA>" . $message_new . "</font>";
                                    } elseif ($user_type == 2 && $model->sender_id != $department->staff_id && Yii::$app->user->identity->id == $model->sender_id) {

                                        $user_send = "<font color= #536A7D>" . $users->first_name . " " . $users->last_name . "</font>";
                                        $message = "<font color=#536A7D>" . $message_new . "</font>";
                                    }

                                    /*$html = '
                                        <div class="media">'.
                                        html::img($users->getAvatar(),
                                            ['class' => 'rounded-circle logo-marketing user_avatar',
                                            'style' => 'width:40px;height: 40px; 
                                            border: 1px solid #C00;
                                            border-color:#ebeff2;box-shadow:0 0 0 0
                                            !important;-moz-box-shadow: 0 0 0
                                            0;display:block; margin-left: 0px;
                                            margin-right: 20px;', 
                                            'alt' => 'user-img',
                                            'title' => Yii::t('app', $users->first_name." ".$users->last_name),
                                            'data-toggle' => 'tooltip']).'
                                        <div class="media-body">
                                        <span><b>'.$user_send.'</b></span><br><br>'.$message.'
                                        </div>
                                        </div>';*/




                                    if ($model->sender_id == $user_id) {

                                        $class = "my-message";
                                        $class2 = "float-right";
                                        $speech = "right";

                                        $html2 = '
                                            <div class="chat">
                                                <div class="chat-history">
                                                        <li class="clearfix ">
                                                            <div class="message-data text-right">
                                                                <span class="message-data-time ">
                                                                '.Yii::$app->formatter->asDate($model->created_at, 'php:m-d-Y h:i a')." ".$model->ago.'
                                                                </span>
                                                                '.html::img($users->getAvatar(),
                                                                ['class' => 'rounded-circle logo-marketing user_avatar',
                                                                'style' => 'width:40px;height: 40px; 
                                                                border: 1px solid #C00;
                                                                border-color:#ebeff2;box-shadow:0 0 0 0
                                                                !important;
                                                                -moz-box-shadow: 0 0 0 0;
                                                               ', 
                                                                'alt' => 'user-img',
                                                                'title' => Yii::t('app', $users->first_name." ".$users->last_name),
                                                                'data-toggle' => 'tooltip']).'
                                                            </div>
                                                            <div class="message '.$class.' '.$class2.'">'.$message.' '.Yii::$app->formatter->asDate($model->created_at, 'php:h:i a').'</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>';

                                            $html = '<ul class="conversation-list">
                                                <li class="odd"  style="max-with:800px; !important">
                                                    <div class="chat-avatar">
                                                                </span>
                                                                '.html::img($users->getAvatar(),
                                                                ['class' => 'rounded-circle logo-marketing user_avatar',
                                                                'style' => 'width:40px;height: 40px; 
                                                                border: 1px solid #C00;
                                                                border-color:#ebeff2;box-shadow:0 0 0 0
                                                                !important;
                                                                -moz-box-shadow: 0 0 0 0;
                                                               ', 
                                                                'alt' => 'user-img',
                                                                'title' => Yii::t('app', $users->first_name." ".$users->last_name),
                                                                'data-toggle' => 'tooltip']).'
                                                    </div>
                                                    <div class="conversation-text style="background:#fef5e4; width:600px !important; ">
                                                        <div class="ctext-wrap" style="background:#fef5e4; !important; ">
                                                             <i>'.$user_send.'  
                                                             <span class="font-gray">
                                                             '.Yii::$app->formatter->asDate($model->created_at, 'php:m-d-Y').
                                                             " ".Yii::$app->formatter->asDate($model->created_at, 'php:h:i a')." ".$model->ago.'</span></i> 
                                                            
                                                                '.$message.'
                                                            

                                                        </div>
                                                    </div>
                                                </li>
                                            ';
                                    } else {

                                        /*$class = "other-message";
                                        $class2 = "float-left";
                                        $speech = "left";*/

                                       $html2 = '
                                            <div class="chat">
                                                <div class="chat-history">
                                                    <ul class="m-b-0">
                                                        <li class="clearfix">
                                                            <div class="message-data">
                                                            '.html::img($users->getAvatar()).'
                                                                <span class="message-data-time">
                                                                 '.Yii::$app->formatter->asDate($model->created_at, 'php:m-d-Y')."<br>".$model->ago.'
                                                                 </span>
                                                            </div>
                                                            <div class="message '.$class.'"'.$message.'</div>                                    
                                                        </li>                               
                                                    
                                                    </ul>
                                                </div>
                                            </div>';

                                        $html = '<ul class="conversation-list">
                                            <li class="">
                                                <div class="chat-avatar">
                                        
                                                                </span>
                                                                '.html::img($users->getAvatar(),
                                                                ['class' => 'rounded-circle logo-marketing user_avatar',
                                                                'style' => 'width:40px;height: 40px; 
                                                                border: 1px solid #C00;
                                                                border-color:#ebeff2;box-shadow:0 0 0 0
                                                                !important;
                                                                -moz-box-shadow: 0 0 0 0;
                                                               ', 
                                                                'alt' => 'user-img',
                                                                'title' => Yii::t('app', $users->first_name." ".$users->last_name),
                                                                'data-toggle' => 'tooltip']).'
                                                               
                                                   
                                                </div>
                                                <div class="conversation-text">
                                                    <div class="ctext-wrap">
                                                        <i><span class =" font-gray">'.$user_send.'  
                                                             <span>
                                                             '.Yii::$app->formatter->asDate($model->created_at, 'php:m-d-Y').
                                                             "</span> 
                                                             <span class='font-gray'>".Yii::$app->formatter->asDate($model->created_at, 'php:h:i a')." </span> ".$model->ago.'</i> 
                                                        
                                                            '.$message.'
                                                        
                                                    </div>
                                                </div>
                                            </li>
                                        ';
                                    }
                    



                                            return $html;
                                }
                            ],
                           

                        ],
                    ]); ?>
               
         


                    </div>
                </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <?php 
                    $form = ActiveForm::begin([
                        'id' => $message_form->formName(),
                        'fieldConfig'=>['template'=>' <div class="col mb-2 mb-sm-0"> 
                        {label}{input}
                    </div>
                    ',
                    'inputOptions' => [
                    'placeholder' => 'Enter your text',
                    'class'=>'form-control border-0',
                    ]]
                     ]); ?>
                        <div style="display:none;">
                            <?= $form->field($message_form, 'project_id')->textInput(['value' => $model->id]) ?>
                            <?= $form->field($message_form, 'sender_id')->textInput(['value' => $user_id])->label(false) ?>
                        </div>
                        <!-- form template -->
                                    <div class="row">
                                            <div class="col">
                                                <div class="mt-2 bg-light p-3 rounded">
                                                    <form class="needs-validation" novalidate="" name="chat-form" id="chat-form">
                                                        <div class="row">
                                                            <div class="col mb-2 mb-sm-0">
                                                                 <?= $form->field($message_form, 'message')->textInput(['placeholder' => "Enter your text new", 'class' =>"form-control border-0" ])->label(false) ?>
                                                                <!-- <div class="invalid-feedback">
                                                                    Please enter your messsage
                                                                </div> -->
                                                            </div>
                                                            <div class="col-sm-auto">
                                                                <div class="btn-group">
                                                                    <a href="#" class="btn btn-light"><i class="fe-paperclip"></i></a>
                                                                      <?= Html::submitButton('<i class="fe-send"></i>', ['id' => 'submit_frm_btn', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success',]) ?>
                                                                    <!--<button type="submit" class="btn btn-success chat-send w-100"><i class="fe-send"></i></button>-->
                                                                </div>
                                                            </div>
                                                            <!-- end col -->
                                                        </div>
                                                        <!-- end row-->
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- end col-->
                                        </div>
                                        <!-- end row -->
                        <!-- end form template -->

                        <?php ActiveForm::end(); ?>
</div>
</div>


</div>

</div>




</div>

</div>


                       


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">

/*$(window).on("load", sinjQuery);

function sinjQuery()
{
  
    setTimeout(function(){
        $('#divu').scrollTop( $('#divu').prop("scrollHeight"));
        document.getElementById('divu').scrollTop=5000;

    }, 1000); 



    console.log(($('#divu').prop("scrollHeight") * 100)); 
        console.log("New Changes"); 


 
}*/



    function checkForm(form) {
        //
        // validate form fields
        //

        form.myButton.disabled = true;
        return true;
    }

    function showform() {

        document.getElementById("projectadd").style.display = "block";


    }

    function mostrar() {
        div = document.getElementById('flotante');
        div.style.display = '';
    }

    function cerrar() {
        div = document.getElementById('projectadd');
        div.style.display = 'none';
    }
</script>

<?php
$this->registerJs('

$(document).click(function(event) {
  //if you click on anything except the modal itself or the "open modal" link, close the modal
  if (!$(event.target).closest(".popover,.btn-popover-order").length) {
    //$(".popover").popover("hide");
  }
});
$(document).on("click",".close-popover",function(){
  $(".popover").popover("hide");
}); 



    $(document).on("click", "#view", (function() {
        $.get(
        $(this).data("url"),
        function (data) {
            $("#modalConsulta").html(data);
            $("#consultar").modal();
        }
        );
        }));

        var proprityJsArray = ' . json_encode($project_priority) . ';

        function change_project_priority(id, priority,old_priority,priority_obj)
        {
            var message = "' . Yii::t('app', 'to change the Priority of Project to') . '";
            message += " `" + proprityJsArray[priority] + "`";

            swal({
                title              : "' . Yii::t('app', 'Are you sure?') . '",
                text               : message,
                type               : "warning",
                showCancelButton   : true,
                confirmButtonClass : "btn-warning",
                confirmButtonText  : "Yes",
                cancelButtonText   : "No",
                closeOnConfirm     : true
                },
                function(isConfirm)
                {
                    if(isConfirm)
                    {

                        $.ajax({
                            type: "GET",
                            url: "' . Url::to(["/project/projects-support/change-project-priority"]) . '",
                            data: {"id":id, "priority":priority},
                            success: function(response)
                            {
                                $.pjax.reload("#projects-support-index", {timeout : false}).done(function(){
                                    $.pjax.reload("#main-alert-widget", {timeout : false});
                                    });
                                }
                                });

                            }
                            else
                            {
                                $(priority_obj).val(old_priority);
                            }
                            });
                        }

                        var statusJsArray = ' . json_encode($project_status) . ';

                        function change_project_status(id, status,old_status,status_obj)
                        {
                            var message = "' . Yii::t('app', 'to change the Status of Project to') . '";
                            message += " `" + statusJsArray[status] + "`";

                            swal({
                                title              : "' . Yii::t('app', 'Are you sure?') . '",
                                text               : message,
                                type               : "warning",
                                showCancelButton   : true,
                                confirmButtonClass : "btn-warning",
                                confirmButtonText  : "Yes",
                                cancelButtonText   : "No",
                                closeOnConfirm     : true
                                },
                                function(isConfirm)
                                {
                                    if(isConfirm)
                                    {

                                        $.ajax({
                                            type: "GET",
                                            url: "' . Url::to(["/project/projects-support/change-project-status"]) . '",
                                            data: {"id":id, "status":status},
                                            success: function(response)
                                            {
                                                $.pjax.reload("#projects-support-index", {timeout : false}).done(function(){
                                                    $.pjax.reload("#main-alert-widget", {timeout : false});
                                                    });
                                                }
                                                });

                                            }
                                            else
                                            {
                                                $(status_obj).val(old_status);
                                            }
                                            });
                                        }

                                        $.pjax.reload("#projects-support-index", {timeout : false});
                                        $(document).ajaxComplete(function() {
                                            $("div.popover.fade.show").remove();
                                            $(\'[data-toggle="popover"]\').popover();
                                            });

                                            $("#modal-box").on("hide.bs.modal", function (e) {
                                                $.pjax.reload("#projects-support-index", {timeout : false});
                                                })
                                                ', View::POS_END); ?>