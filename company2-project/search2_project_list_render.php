<?php

use yii\bootstrap\ActiveForm;

use backend\components\UserHelpers;
use common\models\ClaimTask;
use common\models\Company2Project;
use common\models\Company2ProjectTask;
use common\models\Module;
use common\models\ProjectCategory;
use common\models\ProjectClass;
use common\models\ProjectsAttachments;
use common\models\ProjectStatus;
use common\models\ProjectTask;
use common\models\ProjectType;
use common\models\Task;
use common\models\UserSeasonState;
use common\models\ProjectMessages;

use kartik\editable\Editable;
use kartik\grid\GridView;
use kartik\popover\PopoverX;
use yii\data\ActiveDataProvider;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
use yii\widgets\Spaceless;

use common\models\CompanyGeneralUrlHandler;
use yii\widgets\ListView;

//if(Yii::$app->user->can('AS Staff')){$this->registerCss('.search .input-group-sm select.form-control{max-width: 110px !important;}');}

?>
<?php




$page = Yii::$app->request->get('page');
if ($page == ';') {
    $page = 1;
}
?>
 <script src="https://cdn.jsdelivr.net/npm/@floating-ui/core@1.2.2"></script>
<script src="https://cdn.jsdelivr.net/npm/@floating-ui/dom@1.2.3"></script>
 <?php
    $hidden_stats;
    if($message== "All Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value=">'.$message.': '.$statistics_status["total"].'">';}
    if($message== "New Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value="'.$message.': '.$statistics_status["total_new"].'">';}
    if($message== "Pending Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value="'.$message.': '.$statistics_status["total_pending"].'">';}
    if($message== "Approved Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value="'.$message.': '.$statistics_status["total_approved"].'">';}
    if($message== "in-progress Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value="'.$message.': '.$statistics_status["total_progress"].'">';}
    if($message== "Completed Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value="'.$message.': '.$statistics_status["total_completed"].'">';}
    if($message== "Lost Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value="'.$message.': '.$statistics_status["total_lost"].'">';}
    ?>
    <?= 
        ListView::widget([
            'dataProvider' => $dataProvider,
            'options' => ['class' => 'aside_projects'],
            'pager' => [
                'hideOnSinglePage' => false, 'maxButtonCounts' => 8,
                'linkOptions' => ['class' => 'page'],
                'activePageCssClass' => 'selected',
            ],
            'viewParams' => ['total' => $total,'message' => $message,'page' => $page,'condition_total' => $condition_total],
            'layout' => ' 
        '.$hidden_stats.'      
        <div class="projects_selection_container">
               
            <div class="loader_project">
            <div class="lds-ring">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="loader_icon">
                <span>Loading Projects...</span>
            </div>
        </div>{items}</div><div class="pagination_container">{pager}</div>',
            'itemView' => 'InnerItems/_list_projects_view',
            'pager' => [
                'prevPageLabel' => '<',
                'nextPageLabel' => '>',
            ]
        ]);
    ?>



