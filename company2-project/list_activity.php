<?php
use yii\widgets\ListView;

?>

<div class="timeline-2">
    <?= ListView::widget([
        'dataProvider' => $listActivities,
        'itemOptions' => ['class' => 'innerLeadActivities'],
        'itemView' => '_innerActivity',
        'summary' => '',
    ]) ?>
</div>