<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Company2Project */

$this->title = Yii::t('app', 'Create Contractor Project');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contractor Project'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 m-b-30 header-title">
                <b><?= Html::encode($this->title) ?></b>
            </h4>

            <div class="company2-project-create">

                <?= $this->render('_form', [
                    'model' => $model,
                    'userModel'     => $userModel,
                    'countries'     => $countries,
                    'seasons'       => $seasons,
                    'region'    => $region,
                    'cities2'   => $cities2,
                    'zipcode'      => $zipcode,
                    'client'          => $client,
                    'project_position'  => $project_position,
                    'allow_project'    => $allow_project,
                ]) ?>

            </div>
        </div>
    </div>
</div>