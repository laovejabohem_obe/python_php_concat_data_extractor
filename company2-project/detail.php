<?php

use Carbon\Carbon;
use common\models\CompanyDepartments;
use common\models\DepartmentStaff;
use common\models\ProjectsAttachments;
use common\models\Module;
use common\models\Company2ProjectTask;

use common\models\ProjectsMessages;
use common\models\User;
use kartik\file\FileInput;
use kartik\grid\GridView;
use kartik\select2\Select2;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use backend\components\UserHelpers;
use common\models\Task;
use common\models\TaskPhase;

$base = common\models\AdministrationHelper::getSiteUrl();
$base2 = $base;
$base = "'".$base."'";
$project = $dataProvider22->query->one();


//var_dump($base2); die();



/* @var $this yii\web\View */
/* @var $searchModel app\models\Company2ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$js = <<<JS
$('form#{$message_form->formName()}').on('beforeSubmit', function(e) {
    $('.button--submit').html('Please wait...');
});
JS;

$this->registerJs($js, View::POS_READY, 'dataFormSubmit');



$this->registerJs($js, View::POS_READY, 'dataFormSubmit');

$this->title = 'Projects Supports';
$this->params['breadcrumbs'][] = $this->title;

$user_id = Yii::$app->user->identity->id;

 $task_projects = Task::find()->joinWith('module')
                            ->joinWith('module.department')
                            ->where(['type'=>2])
                            ->andWhere(['task.is_active' => 1])
                            //->all();
                            //->orderby([Task::tableName().'.task_order'])
                            ->orderBy([
                                          Module::tableName().'.module_order' => SORT_ASC,
                                          Task::tableName().'.task_order' => SORT_ASC,

                                        ])->all();

        
$task_project = ArrayHelper::map($task_projects, 'id', function ($task_projects, $defaultValue) {

                 Html::tag('b', Html::encode($task_projects->module->name));
                     return $task_projects->name;
                      }, 'module.name');
                            //var_dump($task_project); die();*/

                     /* $task_project = ArrayHelper::map($task_projects, 'id', 'name');*/
                            //var_dump($task_project); die();*/
$phase_check = new \common\models\PhaseChecker();
$phase_stat = $phase_check->projectStageCheck(null,$model->id);
if(is_null($phase_stat['is_done']))
{
if(!is_null($phase_stat['is_new'])){
    if($phase_stat['is_new'] == true)
    {
        $actual_task = Task::getFirstTask(2);
        $task_actual_phase = $actual_task->getTaskPhase()->one();
    }else{
        $task_actual_phase =$phase_stat['task']->getTaskPhase()->one();
    }
}else{
        $actual_task = Task::findOne($phase_stat['task_id']);
    
}

$options = $phase_check->getContextMenuData();

}
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/autosize.js/3.0.20/autosize.min.js" integrity="sha512-EAEoidLzhKrfVg7qX8xZFEAebhmBMsXrIcI0h7VPx2CyAyFHuDvOAUs9CEATB2Ou2/kuWEDtluEVrQcjXBy9yw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<style type="text/css">
    audio {
        vertical-align: bottom;
        width: 23em !important;
    }

    .p {
        width: 400px;
        margin: 0px auto !important;
        padding-top: auto !important;
        padding-bottom: auto !important;
        border: solid 0px #f60;
        font: normal 13px arial, helvetica, sans-serif;
        line-height: 25px;
    }

    .form-group {
        margin-bottom: 0rem !important;
    }

    .mt-2 {
        margin-top: 0px !important;
    }

    .padding-div {

        padding-top: 15px;
    }

    .margin-chat {

        margin-bottom: 2px;
        padding-right: 70px !important
    }

    hr {
        margin-top: 5px !important;
        margin-bottom: 5px !important;
        padding-top: 7px;
        border: 0;
        border-top: 1px solid rgba(0, 0, 0, .1);
    }

    .avatar-sm {
        height: 2.25rem;
        width: 2.25rem;
    }

    .fw-bold {
        font-weight: 700 !important;
    }



    @media only screen and (max-width:786px) {
        .conversation-list .ctext-wrap {
            width: 270px;
        }

        .p-2 {
            padding: 0.65rem !important;
        }

        /*.scroll{height:auto;width:auto;overflow-y:scroll;display:inline-block;}*/


        audio {
            vertical-align: bottom;
            width: 18em !important;
        }

        .conversation-list .conversation-text {
            margin-left: 0;
            user-select: text !important;
        }

        .scroll {
            height: 600px !important;
            width: 310px !important;
            overflow-y: scroll;
            display: inline-block;
        }

    }

    .conversation-list .odd .ctext-wrap:after {
        border-color: rgba(238, 238, 242, 0) !important;
        border-left-color: #fef5e4 !important;
        border-top-color: #fef5e4 !important;
        left: 100% !important;
        margin-right: -1px;
    }

    audio {
        vertical-align: bottom;
        width: 10em;
    }

    video {
        max-width: 100%;
        vertical-align: top;
    }

    /* input {
        border: 1px solid #d9d9d9;
        border-radius: 1px;
        font-size: 2em;
        margin: .2em;
        width: 30%;
    }
    p,
    .inner {
        padding: 1em;
    }
    li {
        border-bottom: 1px solid rgb(189, 189, 189);
        border-left: 1px solid rgb(189, 189, 189);
        padding: .5em;
    }
    label {
        display: inline-block;
        width: 8em;
    }*/

    .recordrtc button {
        font-size: inherit;
    }

    .recordrtc button,
    .recordrtc select {
        vertical-align: middle;
        line-height: 1;
        padding: 2px 5px;
        font-size: inherit;
        margin: 0;
    }

    .recordrtc,
    .recordrtc .header {
        display: block;
        padding-top: 0;
    }

    .recordrtc video {}

    .recordrtc option[disabled] {
        display: none;
    }

    .red {
        color: red;
    }

    /*! CSS Used from: Embedded */
    .scroll {
        height: 1000px;
        overflow-y: scroll;
        display: inline-block;
    }

    .scroll .inner {
        height: 300%;
        width: 100%;
        content: '.';
    }

    .scroll--simple::-webkit-scrollbar {
        width: 10px;
        height: 10px;
        overflow: hidden;
    }

    .scroll--simple::-webkit-scrollbar-track {
        border-radius: 10px;
        background: transparent;
    }

    .scroll--simple::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background: #dee2e6;
    }

    .scroll--simple::-webkit-scrollbar-thumb:hover {
        background: rgba(0, 0, 0, 0.4);
    }

    .scroll--simple::-webkit-scrollbar-thumb:active {
        background: rgba(0, 0, 0, 0.9);
    }
    @media(max-width: 990px)
    {
        .chatbox_and_files_container
        {
                        position: fixed;
                        bottom: 0;
                        z-index: 50;
                        left: 0;
                        right: 0;
        }
        #projects-support-index{
            padding-bottom: 100px;
        }
        .message_box{
            margin-left: 0;
            width: 94%;
        }

}

        .message_box{
            min-height: 20px !important;
            max-height: 100px;
            margin-left:20px;
        }
        @media (max-width:1500px) 
        {
            #chat-buttons{
                flex-wrap: wrap;
            }
        }
       #client_name_identificator
        {
            width: 100%;display: flex;justify-content: center;font-weight: 800;font-size: 20px;color: #4f4f4f;
        }
        @media (min-width:960px)
        {
            #client_name_identificator
            {
                display: none;
            }
        }
</style>
<head>
   <title>RecordRTC Audio+Video Recording & Uploading to PHP Server</title>
    <metas http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <metas charset="utf-8">
    <metas name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <metas http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <linkss rel="stylesheet" href="https://www.webrtc-experiment.com/style.css">

   <script src="https://www.webrtc-experiment.com/RecordRTC.js"></script>
    <script src="https://www.webrtc-experiment.com/gif-recorder.js"></script>
    <script src="https://www.webrtc-experiment.com/getScreenId.js"></script>
    <script src="https://www.webrtc-experiment.com/DetectRTC.js"> </script>

    <script src="https://cdn.jsdelivr.net/npm/@floating-ui/core@1.0.4"></script>
    <script src="https://cdn.jsdelivr.net/npm/@floating-ui/dom@1.0.10"></script>

    <input type="file" name="physic_file_controller" id="temp_file" style="display: none;">
    <!-- for Edige/FF/Chrome/Opera/etc. getUserMedia support -->
    <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
</head>

<?php if(!is_null($phase_stat['task_id'])):?>
    <input   type="hidden" id="task_referral_id" value="<?php echo $phase_stat['task_id']?>">
<?php endif;?> 

<?php if(is_null($phase_stat['is_done'])):?>
<?php if(boolval($task_actual_phase->has_pdf)): ?>
            <input type="hidden" id="pdf_id" value="<?php echo $actual_task->pdf_id?>">
        <?php endif;?>
<?php if ($phase_stat['is_new'] || $phase_stat['phase_type'] == 1 && is_null($phase_stat['pdf_step']) || $phase_stat['phase_type'] == 1 && $phase_stat['pdf_step'] < 2 ):?>
    <div class="new_step_avaible_advice" style="display: none;background: white;border-radius: 8px;height: 50px;padding: 10px;position: fixed;z-index: 2000;box-sizing: border-box;box-shadow: 2px 2px 10px grey;margin-top: -48px;color: grey;max-width: 50%;height: auto;">Hello <?= Yii::$app->user->identity->first_name?> this projects needs a <?= $actual_task->name ?> in order to continue with his process would you like to be guided?<button class="btn btn-success btn-sm new_step_avaible_advice_btn" style="margin-left: 20px;" <?= 'next-form-value="'.$actual_task->pdf_id.'"'?>>Show me what i have to do</button> <button class="btn btn-sm btn-warning not_now_btn">Not now</button>
    </div>
    <?php elseif($phase_stat != false && sizeof($options)) :?>
    <?php  foreach ($options as $option):?>
        <input type="hidden" class="options_input"  <?php foreach($option as $key => $value){ echo "$key='$value'"; }?> >
    <?php endforeach;?>
    <div class="new_step_avaible_advice" style="display: none;background: white;border-radius: 8px;height: 50px;height:auto;padding: 10px;position: fixed;z-index: 2000;box-sizing: border-box;box-shadow: 2px 2px 10px grey;margin-top: -48px;color: grey;">Hello <?= Yii::$app->user->identity->first_name?> this project its ready for continue the next step shall we begin? <button class="btn btn-success btn-sm new_step_avaible_advice_btn" style="margin-left: 20px;">Show me what i have to do</button><button class="btn btn-sm btn-warning not_now_btn" style="margin-left: 20px;">Not now</button>
    </div>
<?php endif?>
<?php else:?>
    <input type="hidden" id="finished" value="1">
<?php endif;?>

<button type="button" id="custom_modal_for_dynamic_cases_activation_btn" class="btn btn-primary d-none" data-toggle="modal" data-target="#custom_modal_for_dynamic_cases" data-whatever="@mdo"></button>
<div class="timer_message" style="display: none;opacity: 0;position: absolute;top: 0;left: 0;z-index: 200; font-size: 12pt; background: rgb(244, 248, 251); padding: 5px 10px; border-radius: 50px; box-shadow: gray 1px 1px 6px -1px; color: grey; font-weight: 500;"></div>
<div class="modal fade" id="custom_modal_for_dynamic_cases" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="custom_dinamyc_modal_submit">Send message</button>
      </div>
    </div>
  </div>
</div>
 <div class="card" style="padding-bottom:0px; margin-bottom:0px">       
  <div class="card-body" style="padding-top:0px; padding-bottom:0px;">
                           <div class="row">
                        <div class="col-md-2 padding-div" align="center">
                            <?php echo  Html::a(
                                Html::img(
                                    UserHelpers::getProjectClassImage(32, 32, $model->project_class_id, true),
                                    [
                                        'class' => 'rounded-circle logo-marketing user_avatar',
                                        'style' => 'width:50px;height: 50px; 
                                                border: 1px solid #C00;
                                                border-color:#ebeff2;box-shadow:0 0 0 0
                                                !important;-moz-box-shadow: 0 0 0
                                                0;display:block; margin-left: auto;margin-bottom:5px;
                                                margin-right: auto;',
                                        'data-html'=>"true",
                                        'alt' => 'user-img', 'title' => $model->projectClass->name."<br>
                                        ".$model->quantity . " " . $model->projectClass->unit_measure.""
                                        , 'data-toggle' => 'tooltip'
                                    ]
                                )
                            
                            ).'<span id="client_name_identificator" >'.   $model->client->name ." ".$model->client->last_name . '</span>'.
                            '<span data-toggle="tooltip" data-placement="top" title="Click to copy this Project number!">
                                                        <a href="#" onclick="copiarAlPortapapeles('."'".$model->p_id."'".');">
                                                           <b>'.$model->p_id.'</b>
                                                        </a>
                                                </span>';?>
                        </div>
                        <div class="col-md-4 action_buttons" align="left"  style="margin-top:auto; margin-bottom:auto;">
                            <b class="font-gray"></b>
                            <br>
                            <div class="btn-group" align="left">

                                <a href="tel:<?= Html::encode($model->client->phone) ?>" class="font-gray", style = "width:50px;height: 30px;margin-bottom: 10px; margin-right:auto; margin-left:auto;" data-toggle="tooltip" title="Call Client" data-placement= "auto"><i class="fe-phone-call font-22"></i></a>

                                <a href="mailto:<?= Html::encode($model->client->email) ?>" class="font-gray", style = "width:50px;height: 30px; margin-bottom: 10px; margin-right:auto; margin-left:auto;" data-toggle="tooltip" title="Sent Email" data-placement= "auto"><i class="fe-mail font-22"></i></a>

                                <?= Html::a('<i class="fe-eye font-22"></i>',['view', 'id' => $model->id, 'asDialog' => 1], ['style' => "margin-bottom: 10px; margin-right:auto; margin-left:auto; width:50px;height: 30px;", 'class' => 'font-gray', 'onclick'=>'return showModal("' . Yii::t('app', 'Project', ['id' => $model->id]) . '" , $(this).attr("href"), 570, 850); return false;', 'title' => Yii::t('app', 'Project Information'), 'data-pjax' => '0', 'data-toggle' => 'tooltip']); ?>

                                <?= Html::a('<i class="icon-weather-right font-22"></i>', "https://weather.com/", [ 
                                'class'=>"font-gray", 
                                'style' => "width:50px;height: 30px; margin-bottom: 10px; margin-right:auto; margin-left:auto;" ,
                                'data-toggle'=>"tooltip" ,
                                'title'=>"Weather Information", 
                                'data-placement'=> "auto" ,
                                'target'=>"_blank"
                                ]);?>

                                 <?= Html::a('<i class="fe-list  font-24"></i>', "https://administrationservices.net/page/Project-Instructions?slug=Project+Instructions", [ 
                                'class'=>"font-gray", 
                                'style' => "width:50px;height: 30px; margin-bottom: 10px; margin-right:auto; margin-left:auto;" ,
                                'data-toggle'=>"tooltip" ,
                                'title'=>"Tasks Information", 
                                'data-placement'=> "auto" ,
                                'target'=>"_blank"
                                ]);?>
                                <!-- ICONOS FE https://icon-sets.iconify.design/fe/page-5.html -->

                               

                               
                            </div>
                        </div>
                     

                        <div class="col-md-2">

                      
                            


                        </div>
                        <div class="col-md-4 padding-div" align="center" style="margin-top:10px;">

                                <b class="font-gray">Actions</b>
                                <br>
                            <div id="chat-buttons" class="btn-group btn-group-sm">
                            <?= Html::a('<i class="fa ' . ($model->is_active == '1' ? "fa-check" : "fa-close") . '"></i>', 'javascript:void(0)', ['class' => 'btn btn-sm btn-custom ' . ($model->is_active == '1' ? "btn-success" : "btn-danger"), 'onclick' => 'change_status_project(' . $model->id . ', ' . ($model->is_active == '1' ? "0" : "1") . ')', 'title' => $model->projectReverseStatusName, 'data-toggle' => 'tooltip']);?>
                            <?php 

                                    echo Html::a('<i class="icon-materials-pallet"></i>', 
                                        ['/material-project/index-material', 
                                        'project_id' => $model->id,
                                        'project_pid' => $model->p_id,
                                        'asDialog' => '1'],
                                         ['class'       => 'btn btn-sm btn-custom btn-primary ','onclick'     => 'return showModal("' . Yii::t('app', 'ADD MATERIALS') . '", $(this).attr("href"), 600, 900); return false;',
                                         'title' => Yii::t('app', 'Add Material'),
                                         'data-toggle' => 'tooltip'
                                     ]);    

                            ?>




                            <?= Html::a('<i class="icon-contractor"></i>', 
                                    ['/project-affiliate/index-affiliate', 
                                    'project_id' => $model->id,
                                    'project_pid' => $model->p_id,
                                    'asDialog' => '1'],
                                     ['class'       => 'btn btn-sm btn-custom btn-primary ','onclick'     => 'return showModal("' . Yii::t('app', 'ASSIGN PROJECT') . '", $(this).attr("href"), 600, 900); return false;',
                                     'title' => Yii::t('app', 'Assign Projects'),
                                     'data-toggle' => 'tooltip'
                                 ]);?>
                            <?php  if($model->id_status == 5)
                                {
                                    /*echo Html::a('<i class="fas fa-eye"></i>', ['view', 'id' => $model->id], ['class' => 'btn btn-sm btn-custom btn-primary', 'title' => Yii::t('app', 'View Projects'), 'data-pjax' => '0', 'data-toggle' => 'tooltip']);*/
                                }else{

                                    if(Yii::$app->user->can('AS Staff')){

                                         echo Html::a('<i class="fas fa-pencil-alt"></i>', ['updateas', 'id' => $model->id, 'contractor_user' => $model->client->parent_user_id, 'client_id' => $model->client_id], ['class' => 'btn btn-sm btn-custom btn-primary', 'title' => Yii::t('app', 'Update Projects'), 'data-pjax' => '0', 'data-toggle' => 'tooltip']);


                                    }

                                    if(Yii::$app->user->can('Company2 Staff')){

                                        echo Html::a('<i class="fas fa-pencil-alt"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-custom btn-primary', 'title' => Yii::t('app', 'Update Projects'), 'data-pjax' => '0', 'data-toggle' => 'tooltip']);

                                        
                                    }

                                  
                                   
                                }?>
                            <?php 
                               
                                    $file = "'".$documents->file."'";

                        $claimTaskDocuments= ProjectsAttachments::find()
                        ->joinWith('company2ProjectTask')
                        //->joinWith(company2Project)
                        ->where(['company2project_task.project_id'=>$model->id])
                        ->orderBy(['created_at' => SORT_DESC,])
                        ->all();
                    

                                   $printFormsHtml = '<ul>';

                                    foreach ($claimTaskDocuments as $key => $documents) {

                                        $name_d = str_replace ( '_', ' ', $documents->file);

                                        if ($documents->file != "") {
                                            $file = "'" . $documents->file . "'";

                                            $printFormsHtml .=
                                                '
                                                <div class="row pop">
                                                    <div class="col-8">
                                                        <li><font size=2>'.$name_d.'</font>
                                                     </div>
                                                     <td class="col-4" align="right">' . Html::a('<i class="glyphicon glyphicon-eye-open"></i>', Url::to('#'), [ 'class' => 'btn btn-sm btn-custom btn-info','onclick' => 'hola(' . $file . ',' . $base . ')', 'data-pjax' => '0', 'title' => Yii::t('app', 'View Document'), 'data-toggle' => 'tooltip']) . Html::a('<i class="dripicons-download"></i>', ['company2-project-task/download-document', 'document' => $documents->file, 'name' => $documents->file], ['class' => 'btn btn-sm btn-custom btn-info','data-pjax' => '0', 'title' => Yii::t('app', 'Download Document'), 'data-toggle' => 'tooltip']) 
                                                  . '</div>
                                                </div>
                                                </li>';
                                        }
                                    }

                                    $title = '<div class="row" style="border:0px">
                                        <div class="col-9">
                                            <font size=2>View Documents</font>
                                        </div>
                                        <div class= "col-2 style="text-align:right">
                                            <span class="text-info"><strong><button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project" >X</button></strong></span> 
                                        </div>
                                    </div>';

                                    $printFormsHtml .= '</ul>';

                                   
                    echo Html::a('<i class="fa fa-download fa-2" data-toggle="tooltip" data-placement="top" title="View and Download Documents"></i>', 'javascript:void(0)', [
                                'class'          => 'btn btn-sm btn-custom btn-primary',
                                'title'          => $title,
                                    'data-toggle'    => 'tooltip',
                                    'tabindex'       => '0',
                                    'data-container' => 'body',
                                    'data-toggle'    => 'popover',
                                    'data-trigger'   => 'click focus',
                                    'data-placement' => 'auto',
                                    'data-html'      => 'true',
                                    'data-content'   => $printFormsHtml,
                                    ]);
                            ?>
                            <?php  $printFormsHtml =
                                    '<ul>'.
                                     '<li class="first_step">'. Html::a('Inspection Form', 
                                                ['/print-form/inspectionform', 
                                                'project_id' => $model->id,'client_id'=>$model->client->id, 'type'=>'contractor'
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'Inspection Form'), 'data-toggle' => 'tooltip'
                                            ]) .
                                        '</li>'.
                                        '<li>' . Html::a('Service Agreement',
                                                [
                                                    '/print-form/index','id' => $model->id, 'user_id' => $model->user_id, 'form' => 11
                                                ],
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                                ,'data-form' =>11
                                            ]) . 
                                        '</li>' .
                                        '<li style="margin-left:6px;">' . Html::a('SA Amendment',
                                                [
                                                    '/print-form/index','id' => $model->id, 'user_id' => $model->user_id, 'form' => 12
                                                ],
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                                ,'data-form' =>12
                                            ]) . 
                                        '</li>' .
                                          '<li>' . Html::a(
                                            'Third Party Authorization',
                                            [
                                                '/print-form/index',
                                                'id' => $model->id, 'user_id' => $model->user_id, 'form' => 3
                                            ],
                                            ['data-pjax' => '0', 'title' => Yii::t('app', 'Third Party Authorization'), 'data-toggle' => 'tooltip'
                                                ,'data-form' =>3
                                        ]
                                        ) . '</li>' .
                                        '<li>'. Html::a('Material Selection', 
                                                [
                                                    '/print-form/index', 'id' => $model->id,'user_id'=>$model->user_id, 'form'=> '5'
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                                ,'data-form' =>5
                                            ]) .
                                        '</li>'.
                                        '<li>'. Html::a('Referral Form', 
                                                ['/print-form/index', 'id' => $model->id,'user_id'=>$model->user_id, 'form'=>9
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                                ,'data-form' =>9
                                            ]) .
                                        '</li>'. 
                                        /*'<li>'. Html::a('Lient Acknowledgement', 
                                                ['/print-form/index', 'id' => $model->id,'user_id'=>$model->user_id, 'form'=>8
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                            ]) .
                                        '</li>'. */
                                        '<li>'. Html::a('Certificate of Completion', 
                                                ['/print-form/index', 
                                                'id' => $model->id,'user_id'=>$model->user_id, 'form'=>7
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                                ,'data-form' =>7
                                            ]) .
                                        '</li>'.

                                        '<li>' . Html::a(
                                            'Invoice',
                                            [
                                                '/print-form/invoice',
                                                'project_id' => $model->id,
                                                'client_id'=>$model->client->id, 
                                                'user_id' => $model->client->parent_user_id,
                                            ],
                                            ['data-pjax' => '0', 'title' => Yii::t('app', 'Invoice'), 'data-toggle' => 'tooltip'
                                            ,'data-form' =>0]
                                        ) . '</li>' .
                                       
                                    '</ul>'; 

                                    $title = '<div class="row" style= "border:0px;">
                                        <div class="col-9">
                                            <font size=2>Create Documents</font>
                                        </div>
                                        <div class= "col-1 style="text-align:right">
                                            <span class="text-info"><strong><button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project" >X</button></strong></span> 
                                        </div>
                                    </div>';

                             
                                   
                                    echo  Html::a('<i class="fas fa-file-signature fa-2" data-toggle="tooltip" data-placement="top" title="Create Documents"></i>', 'javascript:void(0)', [
                                    'class'          => 'btn btn-sm btn-custom btn-primary document_btn ',
                                    'title'          => $title,
                                    'data-toggle'    => 'tooltip',
                                    'tabindex'       => '0',
                                    'data-container' => 'body',
                                    'data-toggle'    => 'popover',
                                    'data-trigger'   => 'click focus',
                                    'data-placement' => 'auto',
                                    'data-html'      => 'true',
                                    'data-content'   => $printFormsHtml,
                                ]);?>
                                 <?= Html::a('<i class="fas fa-sign-in-alt"></i>', ['site/login-2', 'user_id' => $model->staff_id], ['data-method' => 'post', 'class' => 'btn btn-sm btn-custom btn-info', 'title' => Yii::t('app', 'Signin as this Project Supervisor'), 'data-pjax' => '0', 'data-toggle' => 'tooltip']);?>

                        </div>
                    </div>
                </div>
                    <hr>
            
           
            </div>  
       
        <div class="card-body padding-zero" style="padding-top:0px; height:540px; margin-bottom:0px; padding-bottom:0px;">


<div class="projects-support-index">
  
     
          
      
             

                    
              



          
        <!-- 
<div align="left">
   <br>
   <p><b><font size=3>Overview :</font></b></p>
   <p><font size=2><?php $model->claim_description ?></font></p>
</div>  -->

   



<!-- message -->

                     <!--<div align="left">
                        Discussion: <?php $count_message ?> Messages                     </div>
                </div>-->


                <!-- end form -->
  

           
                        <!-- <div id="slimscrollleft" style="overflow-y:scroll;height:440px; width:auto;" class="chatMessage"> -->
    <!---<div class="scrollDownnn simplebar-content slimscrollleft " style="max-height:550px;" name ="divuu" id ="divu">-->

    <div class="scroll scroll--simple" style="min-width: 100%;max-height:550px;" >
    <div class="inner">

<?php Pjax::begin(['id' => 'projects-support-index','class'=> 'chat_messages_container']); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider2,
                        //'filterModel' => $searchModel,
                        'class' => 'table',
                        'responsive' => true,
                        'responsiveWrap' => false,
                        'persistResize' => false,
                        'showHeader' => false,
                        'summary' => '',
                        'rowOptions'   => function ($model, $key)use($id) 
                        {

                           
                                return ['class' => 'table-white',
                                        'style' => 'max-height:400px; !important'
                                    ];

                           
                        },
                        'columns' => [

                            [

                                'attribute'      => 'projectmessage.sender_id',
                                'headerOptions' => ['style' => ['text-align' => 'center']],
                                'label'          => Yii::t('app', 'Created By'),
                                'headerOptions' => ['style' => 'word-wrap: break-word; width: 100%;'],
                                //'contentOptions' => ['class' => 'text-right'],
                                //'contentOptions' => ['style' => 'word-wrap: break-word; width: 85%; white-space: normal;'],
                                'contentOptions' =>

                                function ($model, $index, $widget, $grid) use ($user_id, $id) {

                                    if ($user_id == $model->projectmessage->sender_id) {
                                        $result =   ['style' => 'text-align: right; word-wrap: break-word; width: 100%; white-space: normal; border: 1px solid white;'];
                                    } else {
                                        $result =  ['style' => 'text-align: left; word-wrap: break-word; width: 100%; white-space: normal; border: 1px solid white;'];
                                    }

                                    //var_dump($result); die();

                                    return $result;
                                },
                                'content'        => function ($model) use ($user_id, $base, $base2, $id,$phase_stat) {
                                   
                                    $department = CompanyDepartments::find()->where(['staff_id' => $model->projectmessage->sender_id])->one();

                                    $message = ProjectsMessages::find()
                                    ->where(['project_id' => $id])
                                    ->andWhere(['task_id' => $model->id])
                                    ->orderBy([
                                        ProjectsMessages::tableName() . '.id' => SORT_DESC,

                                    ])->all();


                                      $file = "'".$documents->file."'";

                                   

                                    foreach($message as $mess)
                                    {
                                         $users = User::find()->where(['id' => $mess->sender_id])->one();

                                         $user_type = $users->type_id;

                                        if ($user_type == 1) {

                                            $user_send = "<font color= #00B0F0>" . $users->first_name . " " . $users->last_name . "</font>";
                                            $message = "<font color=#00B0F0>" . $message_new . "</font>";
                                        }

                                        if ($user_type == 5 || $user_type == 3) {

                                            $user_send = "<font color= #33E5BC>" . $users->first_name . " " . $users->last_name . "</font>";
                                            $message = "<font color=#33E5BC>" . $message_new . "</font>";
                                        } elseif ($user_type == 2 && $department->staff_id == $model->projectmessage->sender_id) {
                                            $user_send = "<font color= #81C872>" . $users->first_name . " " . $users->last_name . "</font>";
                                            $message = "<font color=#81C872>" . $message_new . "</font>";
                                        } elseif ($user_type == 2 && $model->projectmessage->sender_id != $department->staff_id && Yii::$app->user->identity->id != $model->projectmessage->sender_id) {
                                            $user_send = "<font color= #7266BA>" . $users->first_name . " " . $users->last_name . "</font>";
                                            $message = "<font color=#7266BA>" . $message_new . "</font>";
                                        } elseif ($user_type == 2 && $model->projectmessage->sender_id != $department->staff_id && Yii::$app->user->identity->id == $model->projectmessage->sender_id) {

                                            $user_send = "<font color= #536A7D>" . $users->first_name . " " . $users->last_name . "</font>";
                                            $message = "<font color=#536A7D>" . $message_new . "</font>";
                                        }

                                    $claimTaskDocuments= ProjectsAttachments::find()
                                    //->joinWith(company2ProjectTask)
                                    ->joinWith(projectsMessages)
                                    //->where(['company2project_task.project_id'=>$mess->project_id])
                                    //->andWhere(['company2project_task.task_id' => $model->id])
                                    ->andWhere(['projects_messages.sender_id' => $mess->sender_id])
                                    ->andWhere(['message_id' => $mess->id])

                                    ->all();

                                    //var_dump($claimTaskDocuments); die();
                                

                                   $printFormsHtml = '';

                                    foreach ($claimTaskDocuments as $key => $documents) {
                                        if(!empty($documents->file_nickname)){
                                            $name_d = $documents->file_nickname;
                                        }else{
                                            $name_d = str_replace ( '_', ' ', $documents->file);
                                        }

                                        if ($documents->file != "") {
                                            $file2 = "'" . $documents->file . "'";

                                        if($documents->file_ext == 'wav'){
                                             $filenameaudio =  $base2.'/uploads/company2project-task-documents/audio'.$mess->id.'.wav';

                                            $link = '<audio preload="auto" src="'.$filenameaudio.'" controls=""></audio>';
                                            $link2 = "";

                                           

                                        }else
                                        {

                                            $link = Html::a('<font size=2>'.$name_d.'</font>', Url::to('#'), [ 'class' => 'link','onclick' => 'hola2(' . $file2 . ',' . $base . ')', 'data-pjax' => '0', 'title' => Yii::t('app', 'View Document'), 'data-toggle' => 'tooltip']);

                                            $link2 = $documents->file_size;
                                        }


                                        if (Yii::$app->user->can('AS Admin')){

                                            $deletes = Html::a('<i style = "color:red;" class="dripicons-trash"></i>', 'javascript:void(0)', ['class' => 'btn btn-link btn-lg text-muted', 'onclick' => 'deleteFile("' . $documents->file . '", ' . $documents->id . ')', 'data-pjax' => '0', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Delete', ['document' => $label])]);
                                        }else{

                                            $deletes = "";
                                        }

                                        if(is_null($documents->is_private_file) || $documents->sender_id == Yii::$app->user->identity->id || Yii::$app->user->can('AS Admin'))
                                        {
                                            $printFormsHtml .=


                                            '   <div class="card mt-2 mb-1 shadow-none border text-start">
                                                        <div class="p-2">
                                                            <div class="row align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="avatar-sm">
                                                                        <span class="avatar-title bg-primary rounded">
                                                                            '.$documents->file_ext.'
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col ps-0">
                                                                    <span>' . $link.
                                                                '</span>
                                                                    <span class="mb-0">'.$link2.'</span>
                                                                </div>
                                                                <div class="col-auto" style="padding-right:5px;">
                                                                    <!-- Button -->
                                                        
                                                                    '.Html::a('<i class="dripicons-download"></i>', ['company2-project-task/download-document', 'document' => $documents->file, 'name' => $documents->file], ['class' => 'btn btn-link btn-lg text-muted','data-pjax' => '0', 'title' => Yii::t('app', 'Download Document'), 'data-toggle' => 'tooltip']) .'
                                                                    </a>
                                                                </div>
                                                                <div class="col-auto" style="padding-left:5px;">
                                                                    <!-- Button -->
                                                        
                                                                    '.$deletes.'
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            ';
                                                '
                                                
                                               
                                                <br>';
                                                }else if($documents->is_private_file == true)
                                                {
                                                $printFormsHtml .=

                                                    '   <div class="card mt-2 mb-1 shadow-none border text-start">
                                                        <div class="p-2">
                                                            <div class="row align-items-center">
                                                                <div class="col-auto">
                                                                    <div class="avatar-sm">
                                                                        <span class="avatar-title bg-primary rounded">
                                                                            <i class="fa fa-lock" aria-hidden="true" style="font-size: 16pt;color: white;"></i>

                                                                                                                                                   </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col ps-0">
                                                                    <span class="mb-0" style="margin-left: 0;font-size: 13pt;font-weight: 500;">'. $name_d . '</span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            ';
                                                '
                                                
                                               
                                                <br>';

                                            }
                                        }
                                    }

                                        //$message_new_replace =  str_replace("<p", "<span", $mess['message']);
                                        //$message_new =  str_replace("</p", "</span", $message_new_replace);
                                  if ($mess->sender_id == Yii::$app->user->identity->id) {

                                            $sender = "right";
                                            //$sender = "left";



                                        }else{

                                            //$sender = "left";
                                            $sender = "right";


                                    }       


                                            $message_new .= ' <div align = "'.$sender.'"> <i>'.$user_send.'  
                                                             <span class="font-gray">
                                                             '.Yii::$app->formatter->asDate($mess['created_at'], 'php:m-d-Y').
                                                             " ".Yii::$app->formatter->asDate($mess['created_at'], 'php:h:i a')." ".$mess->ago.'</span></i> ';


                                            $message_new .= "<b><font size=0.5em;>◉</font></b> ".$mess['message'].'<br>'
                                                                    .$printFormsHtml.'</div>';
                                    }

                                      $title = '<div class="row" style="border:0px">
                                        <div class="col-10">
                                            Description
                                        </div>
                                        <div class= "col-1 style="text-align:right">
                                            <span class="text-info"><strong><button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project" >X</button></strong></span> 
                                        </div>
                                    </div>';

                                        $description = '<span>'.$model->description.'</span>';




                                    if ($model->projectmessage->sender_id == $user_id) {

                                        $class = "my-message";
                                        $class2 = "float-right";
                                        $speech = "right";

                                            $compare_dates = TaskPhase::checkTime($model->id,Yii::$app->request->queryParams['id'],2);
                                            if($compare_dates != false)
                                             {
                                                if($compare_dates['done'])
                                                {
                                                    $timer = '<div class="timer_container" task-id="'. $model->id .'" task-limit-days="'. $compare_dates['task_limit_days'] .'" end-timestamp="'. $compare_dates['end_date'] .'" start-timestamp="'.$compare_dates['starting_date'].'" type ="done" style="float:left"><i class="fa fa-clock-o timer_icon"  style="font-size: 20pt;color: green;position:absolute;font-weight: 200;" aria-hidden="true"></i>
                                                    <i class="fa fa-check" aria-hidden="true" style="margin-top: 13px;color: green;margin-left: 10px;background: #f4f8fb;font-size: 12pt;"></i>
                                                    </div>';

                                                }
                                                else if($compare_dates['pending'])
                                                {
                                                    $timer = '<div class="timer_container" task-id="'. $model->id .'" task-limit-days="'. $compare_dates['task_limit_days'] .'" end-timestamp="'. $compare_dates['end_date'] .'" start-timestamp="'.$compare_dates['starting_date'].'" type ="pending" style="float:left"><i class="fa fa-clock-o timer_icon"  style="font-size: 20pt;color: #6B6B6B;font-weight: 200;" aria-hidden="true"></i></div>';

                                                }
                                                else if($compare_dates['fail'])
                                                {
                                                    $timer = '<div class="timer_container" task-id="'. $model->id .'" task-limit-days="'. $compare_dates['task_limit_days'] .'" end-timestamp="'. $compare_dates['end_date'] .'" start-timestamp="'.$compare_dates['starting_date'].'" type ="fail"><i class="fa fa-clock-o timer_icon"  style="font-size: 20pt;color: red;font-weight: 200;position:absolute;" aria-hidden="true"></i>
                                                    <i class="fa fa-times-circle" aria-hidden="true" style="margin-top: 15px;color: red;margin-left: 10px;background: #f4f8fb;font-size: 12pt;"></i></div>';

                                                }else if($compare_dates['delayed'])
                                            {
                                                   $timer = '<div class="timer_container" task-id="'. $model->id .'" task-limit-days="'. $compare_dates['task_limit_days'] . '"  delayed-timestamp="'. $compare_dates['delayed_at'] .'" end-timestamp="'. $compare_dates['end_date'] .'" start-timestamp="'.$compare_dates['starting_date'].'" type ="delayed"><i class="fa fa-clock-o timer_icon"  style="font-size: 20pt;color: #6B6B6B;font-weight: 200;position:absolute;" aria-hidden="true"></i>
                                                <i class="fa fa-hourglass" aria-hidden="true" style="margin-top: 15px;color: #6B6B6B;margin-left: 13px;background: #f4f8fb;font-size: 9pt;"></i></div>';
                                            }

                                            }

                                            $html = '<ul class="conversation-list">
                                                <li class="odd"  !important">
                                                    <div class="chat-avatar  margin-chat" >
                                                                </span>
                                                                '.html::img($users->getAvatar(),
                                                                ['class' => 'rounded-circle logo-marketing user_avatar',
                                                                'style' => 'width:40px;height: 40px; 
                                                                border: 1px solid #C00;
                                                                border-color:#ebeff2;box-shadow:0 0 0 0
                                                                !important;
                                                                -moz-box-shadow: 0 0 0 0;
                                                               ', 
                                                                'alt' => 'user-img',
                                                                'title' => Yii::t('app', $users->first_name." ".$users->last_name),
                                                                'data-toggle' => 'tooltip']).'
                                                    </div>
                                                    <div class="conversation-text style="background:#fef5e4; width:600px; height:auto;  !important; ">
                                                        <div class="ctext-wrap" style="background:#fef5e4; !important; ">
                                                            '. $timer .'
                                                            <b>Task:</b>  <b><span style="color:#1a73e8 !important; font-size:120% !important;">'.$model->name.'</span></b></em>
                                                            <br>
                                                            <b>Module:</b>  <span style="color:#33b5e5">'.$model->module->name.'</span>
                                                            <br>
                                                            <b>Department:</b> <span style="color:#7266BA">'.$model->module->department->name.'</span> '.Html::a('<i class="fa fa-info-circle" style="color:#4285f4;"> Description </i> ' . "", 'javascript:void(0)', [
                                            //'class'          => 'btn-sm btn-link',
                                            'title'          => $title,
                                            'data-toggle'    => 'tooltip',
                                            'tabindex'       => '0',
                                            'data-container' => 'body',
                                            'data-toggle'    => 'popover',
                                            'data-trigger'   => 'click focus',
                                            'data-placement' => 'auto',
                                            'data-html'      => 'true',
                                            'data-close'      => 'true',
                                            'data-content'   => '<div class="card" style="width:300px; height:200px; padding:5px; color:#4E6379; border-color:#DEE2E6">'.$description.'</div>',
                                            //'data-template' => '<div class="card" style="width:300px; height:300px;">'.$description.'</div>',
                                        ]).'<br>
                                                            <br>
                                                              '.$message_new.'<br><b>Documents</b>
                                                            

                                                        </div>
                                                    </div>
                                                </li>
                                            ';
                                    } else {
                                            $compare_dates = TaskPhase::checkTime($model->id,Yii::$app->request->queryParams['id'],2);
                                            if($compare_dates != false)
                                             {
                                                if($compare_dates['done'])
                                                {
                                                    $timer = '<div class="timer_container" task-id="'. $model->id .'" task-limit-days="'. $compare_dates['task_limit_days'] .'" end-timestamp="'. $compare_dates['end_date'] .'" start-timestamp="'.$compare_dates['starting_date'].'" type ="done"><i class="fa fa-clock-o timer_icon"  style="font-size: 20pt;color: green;font-weight: 200;position:absolute;" aria-hidden="true"></i>
                                                    <i class="fa fa-check" aria-hidden="true" style="margin-top: 13px;color: green;margin-left: 10px;font-size: 12pt;"></i>
                                                    </div>';

                                                }
                                                else if($compare_dates['pending'])
                                                {
                                                    $timer = '<div class="timer_container" task-id="'. $model->id .'" task-limit-days="'. $compare_dates['task_limit_days'] .'" end-timestamp="'. $compare_dates['end_date'] .'" start-timestamp="'.$compare_dates['starting_date'].'" type ="pending"><i class="fa fa-clock-o timer_icon"  style="font-size: 20pt;color: #6B6B6B;font-weight: 200;" aria-hidden="true"></i></div>';

                                                }
                                                else if($compare_dates['fail'])
                                                {
                                                    $timer = '<div class="timer_container" task-id="'. $model->id .'" task-limit-days="'. $compare_dates['task_limit_days'] .'" end-timestamp="'. $compare_dates['end_date'] .'" start-timestamp="'.$compare_dates['starting_date'].'" type ="fail"><i class="fa fa-clock-o timer_icon"  style="font-size: 20pt;color: red;font-weight: 200;position:absolute;" aria-hidden="true"></i>
                                                    <i class="fa fa-times-circle" aria-hidden="true" style="margin-top: 15px;color: red;margin-left: 10px;background: #f4f8fb;font-size: 12pt;"></i></div>';

                                                }else if($compare_dates['delayed'])
                                            {
                                                   $timer = '<div class="timer_container" task-id="'. $model->id .'" task-limit-days="'. $compare_dates['task_limit_days'] . '"  delayed-timestamp="'. $compare_dates['delayed_at'] .'" end-timestamp="'. $compare_dates['end_date'] .'" start-timestamp="'.$compare_dates['starting_date'].'" type ="delayed"><i class="fa fa-clock-o timer_icon"  style="font-size: 20pt;color: #6B6B6B;font-weight: 200;position:absolute;" aria-hidden="true"></i>
                                                <i class="fa fa-hourglass" aria-hidden="true" style="margin-top: 15px;color: #6B6B6B;margin-left: 13px;background: #f4f8fb;font-size: 9pt;"></i></div>';
                                            }

                                            }
                                        $html = '<ul class="conversation-list">
                                            <li class="">
                                                <div class="chat-avatar margin-chat">
                                        
                                                                </span>
                                                                '.html::img($users->getAvatar(),
                                                                ['class' => 'rounded-circle logo-marketing user_avatar',
                                                                'style' => 'width:40px;height: 40px; 
                                                                border: 1px solid #C00;
                                                                border-color:#ebeff2;box-shadow:0 0 0 0
                                                                !important;
                                                                -moz-box-shadow: 0 0 0 0;
                                                               ', 
                                                                'alt' => 'user-img',
                                                                'title' => Yii::t('app', $users->first_name." ".$users->last_name),
                                                                'data-toggle' => 'tooltip']).'
                                                               
                                                   
                                                </div>
                                                <div class="conversation-text">
                                                    <div class="ctext-wrap"><em>
                                                            '. $timer .'
                                                            <b>Task:</b>  <b><span style="color:#1a73e8 !important; font-size:120% !important;">'.$model->name.'</span></b></em>
                                                            <br>
                                                            <b>Module:</b>  <span style="color:#33b5e5">'.$model->module->name.'</span>
                                                            <br>
                                                            <b>Department:</b> <span style="color:#7266BA">'.$model->module->department->name.'</span> '.Html::a('<i class="fa fa-info-circle" style="color:#4285f4;"> Description </i> ' . "", 'javascript:void(0)', [
                                            //'class'          => 'btn-sm btn-link',
                                            'title'          => $title,
                                            'data-toggle'    => 'tooltip',
                                            'tabindex'       => '0',
                                            'data-container' => 'body',
                                            'data-toggle'    => 'popover',
                                            'data-trigger'   => 'click focus',
                                            'data-placement' => 'auto',
                                            'data-html'      => 'true',
                                            'data-close'      => 'true',
                                            'data-content'   => '<div class="card" style="width:300px; height:200px; padding:5px; color:#4E6379; border-color:#DEE2E6">'.$description.'</div>',
                                            //'data-template' => '<div class="card" style="width:300px; height:300px;">'.$description.'</div>',
                                        ]).'<br><br>
                                                        
                                                            '.$message_new.'<br><b>Documents</b><br>
                                                                   
                                                        
                                                    </div>
                                                </div>
                                            </li>
                                        ';
                                    }
                    



                                            return $html;
                                }
                            ],
                           

                        ],
                    ]); ?>
        <?php Pjax::end(); ?>

         </div>


                    </div>
                </div>

        </div>
    </div>
    <div class="row chatbox_and_files_container">
        <div class="col-md-12">


            <?php 
                    $form = ActiveForm::begin([
                        'id' => $message_form->formName(),
                        'fieldConfig'=>['template'=>'
                        {label}{input}
                    
                    ',
                    'inputOptions' => [
                    'placeholder' => 'Enter your comment',
                    'class'=>'form-control border-0',
                    ]]
                     ]); ?>
                        <div style="display:none;">
                            <?= $form->field($message_form, 'project_id')->textInput(['value' => $model->id]) ?>
                            <?= $form->field($message_form, 'sender_id')->textInput(['value' => $user_id])->label(false) ?>
                        </div>
                        <!-- form template -->
                                    <div class="row">
                                            <div class="col">
                                                <div class="mt-2 bg-light p-3 rounded">
                                                    <form class="needs-validation" novalidate="" name="chat-form" id="chat-form">
                                                        <div class="row" align="center" style="margin:auto;">
                                                            <div class="col-sm-12 col-md-12 col-lg-3 mb-2 mb-sm-0" style="padding-right:0px; padding-bottom:10px;" >
                                                                <!-- Task_ID selector -->
                                                                 <?php echo $form->field($message_form, 'task_id')->widget(Select2::classname(), [
                                                                        'options' => [
                                                                            //'placeholder' => 'Select a task to comment', 
                                                                            'id' => 'task_id',
                                                                            'multiple' => false,
                                                                            'onchange' => 'Viewtext(this.value);',

                                                                           
                                                                        ],
                                                                        'theme' => Select2::THEME_KRAJEE_BS4, 
                                                                        'disabled' => false,
                                                                        'data' => $task_project,
                                                                        'language' => 'en',
                                                                        'pluginOptions' => [
                                                                            'allowClear' => true,
                                                                        ],
                                                                    ])->label(false); ?>



                                                            </div>
                                                            <div class="col-md-12 col-lg-6 col-sm-12" style="padding-right:0px; padding-left:5px; display:none;" id = "message_text">
                                                                 <?= $form->field($message_form, 'message')->textarea(['placeholder' => "Enter your comment", 'class' =>"form-control border-0 message_box" ])->label(false) ?>
                                                            </div>
            <div class="btn-group col-md-12 col-lg-3" style="padding-right:0px; padding-left:0px; display:block; margin-left:0px;" id = "voice_text" align="center">
                <a class="btn btn-light" id="audio_rec" style="padding-top:7px; padding-bottom:7px; padding-right:10px; padding-left:10px; margin-right:3px"><i class="fe-mic font-20"></i></a>
                                                                                                        
                                                                               
                                                            
                <?php 
                    $company2project_task= Company2ProjectTask::find()->where(['project_id'=>$model->id])->one();

                    $valid = "delete_audio";

                        if ($company2project_task)
                        {
                            echo  Html::a('<i class="fas fa-trash-alt"></i>', ['/company2-project/deleteaudio', 'id' => $id, 'valid' => $valid ], ['id'=> 'delete-audio', 'style' => 'display:none; padding-top:5px; padding-bottom:5px; padding-right:10px; padding-left:10px; width:39px; margin:2px; ', 'class' => 'btn btn-danger', 'title' => Yii::t('app', 'Delete Audio'), 'data-pjax' => '0', 'data-toggle' => 'tooltip'])
                            .Html::a('<i class="fe-paperclip"></i>', Url::to('#'), [ 'style' => 'margin-right:3px;','class' => 'btn btn-light','onclick' => 'openUpdate2('.$company2project_task->id.','.$model->id.',39, 2,'.$base.')', 'data-pjax' => '0',  'title' => (Yii::t('app', 'Add Files')), 'data-toggle' => 'tooltip']) ;
                        }
                        else
                        {
                            echo Html::a('<i class="fas fa-trash-alt"></i>', ['/company2-project/deleteaudio', 'id' => $id,'valid' => $valid  ], ['id'=> 'delete-audio', 'style' => 'display:none;', 'class' => 'btn btn-light', 'title' => Yii::t('app', 'Delete Audio'), 'data-pjax' => '0', 'data-toggle' => 'tooltip'])."" ;
                        }
                ?>
                <?= Html::submitButton('<i class="fe-send"></i>', ['tittle' => 'Upload Audio','style' => 'padding-top:8px; padding-bottom:8px; padding-right:10px; padding-left:10px', 'id'=>"upload-to-server", 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success',]) ?>
                                               

</div>
                                                        


                                                            <!-- end col -->
                                                        </div>
                                                        <!-- end row-->
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- end col-->
                                        </div>
                                        <!-- end row -->
                        <!-- end form template -->

                        <?php ActiveForm::end(); ?>
</div>
</div>


</div>

</div>




</div>

</div>


                       


<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->

<script type="text/javascript">
/*$(window).on("load", sinjQuery);

function sinjQuery()
{
  
    setTimeout(function(){
        $('#divu').scrollTop( $('#divu').prop("scrollHeight"));
        document.getElementById('divu').scrollTop=5000;

    }, 1000); 



    console.log(($('#divu').prop("scrollHeight") * 100)); 
        console.log("New Changes"); 


 
}*/

function copiarAlPortapapeles(id_elemento) {

  var aux = document.createElement("input");
  aux.setAttribute("value", id_elemento);
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);

var message = "Project Number: "+id_elemento+" has been copied!";

    swal({
        title              : "Project Number: "+id_elemento,
        text               : message,
        type               : "success",
        showCancelButton   : true,
        //confirmButtonClass : "btn-success",
        //confirmButtonText  : "Ok",
        closeOnConfirm     : false,
        timer: 3000

    });
}




function hola2(a, d)
{

              //a is project_id and b = task_id c = type d = baseurl
              var window_width = 750;
              var window_height = 830;
              var newfeatures= 'scrollbars=yes,resizable=yes';
              var window_top = (screen.height-window_height)/2;
              var window_left = (screen.width-window_width)/2;

             
                newWindow=window.open(""+d+"/uploads/company2project-task-documents/"+a+"",'_blank', 'titulo','width=' + window_width + ',height=' + window_height + ',top=' + window_top + ',left=' + window_left + ',features=' + newfeatures + '').focus;
             
}

function openUpdate2(a, b, c, d, e)
{
    if($("#task_id")[0].value == ""){
        swal(
          "You haven't select a task yet",
          'Please select a task before submit any document',
          'error'
        )
        return 0;
    }
    //adding this for async files upload
    localStorage.setItem("message_from_projects",document.querySelector('#projectsmessages-message').value);
    //'a'=>$claim_task->id,'b'=>$project_id,'c'=>$model->id, d= type e = baseurl
    var window_width = 750;
    var window_height = 500;
    var newfeatures= 'scrollbars=yes,resizable=yes';
    var window_top = (screen.height-window_height)/2;
    var window_left = (screen.width-window_width)/2;

    task_id = document.getElementById('task_id').value;

    c = task_id;

    //alert(task_id); 

              if(d == 1)
              {
           showModal("<?php echo Yii::t('app', 'Submit Files', ['id' => $model->id]) ?>" ,""+e+"/claim-task/files-upload?asDialog=1&id="+a+"&project_id="+b+"&task_id="+c+"", 'titulo','width=' + window_width + ',height=' + window_height + ',top=' + window_top + ',left=' + window_left + ',features=' + newfeatures + '', 570, 850);

               
              }
              if(d == 2)
              { 
               showModal("<?php echo Yii::t('app', 'Submit Files', ['id' => $model->id]) ?>" ,""+e+"/company2-project-task/files-upload?asDialog=1&id="+a+"&project_id="+b+"&task_id="+c+"", 'titulo','width=' + window_width + ',height=' + window_height + ',top=' + window_top + ',left=' + window_left + ',features=' + newfeatures + '', 570, 850);

              }
              if(d == 0)
              {
               showModal("<?php echo Yii::t('app', 'Submit Files', ['id' => $model->id]) ?>" ,""+e+"/project-task/files-upload?asDialog=1&id="+a+"&project_id="+b+"&task_id="+c+"", 'titulo','width=' + window_width + ',height=' + window_height + ',top=' + window_top + ',left=' + window_left + ',features=' + newfeatures + '', 570, 850);
              }
              document.querySelector("#modal-box").addEventListener("click", (e)=>{
                          $.pjax.reload("#projects-support-index", {timeout : false}).done(function(){
                              $.pjax.reload("#main-alert-widget", {timeout : false})
                              addContextMenu()
                          });
              });         
}

    //when submitting a file this update the chat displaying the relevant new information
    function updateChatFromFileSubmit()
    {
                if(Boolean(localStorage.getItem('file_submit_status_projects')))
                {
                        swal({
                            title              : "Document uploaded",
                            type               : "success",
                            showCancelButton   : true,
                            closeOnConfirm     : false,
                            timer: 3000
                        });
                        localStorage.removeItem('file_submit_status_projects');
                        document.querySelector('#projectsmessages-message').value = '';
                          $.pjax.reload("#projects-support-index", {timeout : false}).done(function(){
                              $.pjax.reload("#main-alert-widget", {timeout : false})
                          });
                }
    }

    function checkForm(form) {
        //
        // validate form fields
        //

        form.myButton.disabled = true;
        return true;
    }

    function showform() {

        document.getElementById("projectadd").style.display = "block";


    }

    function mostrar() {
        div = document.getElementById('flotante');
        div.style.display = '';
    }

    function cerrar() {
        div = document.getElementById('projectadd');
        div.style.display = 'none';
    }
</script>

<?php
$this->registerJs('



function deleteFile(a, b)
{
    var file = a;
    var project_task_id= b;
    swal({
        title              : "' . Yii::t('app-sweet_alert', 'Are you sure?') . '",
        text               : "' . Yii::t('app-sweet_alert', 'You want to delete this Document?') . '",
        type               : "warning",
        showCancelButton   : true,
        confirmButtonClass : "btn-warning",
        confirmButtonText  : "Yes",
        cancelButtonText   : "No",
        closeOnConfirm     : true
    },
    function(isConfirm){
        if(isConfirm){
            $.ajax({
                url     : "' . Url::to(['company2-project-task/delete-document']) . '",
                data    : {"file" : file, "project_task_id" : project_task_id},
                success : function(response)
                {
                    $(".tooltip").remove();
                    $.pjax.reload("#projects-support-index", {timeout : false}).done(function(){
                        $.pjax.reload("#main-alert-widget", {timeout : false})
                    });
                }
            });
        }
    });
}
function Viewtext(id)
{
     let task_selector = document.querySelector("#task_id");
     console.log(task_selector.value)
     let task_id=localStorage.setItem("task_id",task_selector.value );

     let queryString = window.location.search;
     let urlParams = new URLSearchParams(queryString);

     localStorage.setItem("company_type", 1);
     localStorage.setItem("client_id", urlParams.get("id"));

     var task_title=localStorage.setItem("task_title",document.querySelector(".select2-selection.select2-selection--single").textContent.replace("×","") );

    $("#mySelect").select2({
    closeOnSelect: false
});

  document.getElementById("task_id").value = id; 
  document.getElementById("message_text").style.display = "block";
  /*document.getElementById("buttons-group").style.display = "block";*/
  document.getElementById("voice_text").style.display = "block";
 /*document.getElementById("send").style.display = "block";*/
 /*document.getElementById("audio_btn").style.display = "block";*/

    $(".select2.select2-hidden-accessible").select2("close");

}

$(document).click(function(event) {
  //if you click on anything except the modal itself or the "open modal" link, close the modal
  if (!$(event.target).closest(".popover,.btn-popover-order").length) {
    //$(".popover").popover("hide");
  }
});
$(document).on("click",".close-popover",function(){
  $(".popover").popover("hide");
}); 



    $(document).on("click", "#view", (function() {
        $.get(
        $(this).data("url"),
        function (data) {
            $("#modalConsulta").html(data);
            $("#consultar").modal();
        }
        );
        }));

        var proprityJsArray = ' . json_encode($project_priority) . ';

        function change_project_priority(id, priority,old_priority,priority_obj)
        {
            var message = "' . Yii::t('app', 'to change the Priority of Project to') . '";
            message += " `" + proprityJsArray[priority] + "`";

            swal({
                title              : "' . Yii::t('app', 'Are you sure?') . '",
                text               : message,
                type               : "warning",
                showCancelButton   : true,
                confirmButtonClass : "btn-warning",
                confirmButtonText  : "Yes",
                cancelButtonText   : "No",
                closeOnConfirm     : true
                },
                function(isConfirm)
                {
                    if(isConfirm)
                    {

                        $.ajax({
                            type: "GET",
                            url: "' . Url::to(["/project/projects-support/change-project-priority"]) . '",
                            data: {"id":id, "priority":priority},
                            success: function(response)
                            {
                                $.pjax.reload("#projects-support-index", {timeout : false}).done(function(){
                                    $.pjax.reload("#main-alert-widget", {timeout : false});
                                    });
                                }
                                });

                            }
                            else
                            {
                                $(priority_obj).val(old_priority);
                            }
                            });
                        }

                        var statusJsArray = ' . json_encode($project_status) . ';

                        function change_project_status(id, status,old_status,status_obj)
                        {
                            var message = "' . Yii::t('app', 'to change the Status of Project to') . '";
                            message += " `" + statusJsArray[status] + "`";

                            swal({
                                title              : "' . Yii::t('app', 'Are you sure?') . '",
                                text               : message,
                                type               : "warning",
                                showCancelButton   : true,
                                confirmButtonClass : "btn-warning",
                                confirmButtonText  : "Yes",
                                cancelButtonText   : "No",
                                closeOnConfirm     : true
                                },
                                function(isConfirm)
                                {
                                    if(isConfirm)
                                    {
                                        $.ajax({
                                            type: "GET",
                                            url: "' . Url::to(["/project/projects-support/change-project-status"]) . '",
                                            data: {"id":id, "status":status},
                                            success: function(response)
                                            {
                                                $.pjax.reload("#projects-support-index", {timeout : false}).done(function(){
                                                    $.pjax.reload("#main-alert-widget", {timeout : false});
                                                    });
                                                }
                                                });

                                            }
                                            else
                                            {
                                                $(status_obj).val(old_status);
                                            }
                                            });
                                        }

                                        $.pjax.reload("#projects-support-index", {timeout : false});
                                        $(document).ajaxComplete(function() {
                                            $("div.popover.fade.show").remove();
                                            $(\'[data-toggle="popover"]\').popover();
                                            });

                                            $("#modal-box").on("hide.bs.modal", function (e) {
                                                $.pjax.reload("#projects-support-index", {timeout : false});
                                                })

                                                ', View::POS_END); ?>

                                                 <script>
            (function() {
                var params = {},
                    r = /([^&=]+)=?([^&]*)/g;

                function d(s) {
                    return decodeURIComponent(s.replace(/\+/g, ' '));
                }

                var match, search = window.location.search;
                while (match = r.exec(search.substring(1))) {
                    params[d(match[1])] = d(match[2]);

                    if(d(match[2]) === 'true' || d(match[2]) === 'false') {
                        params[d(match[1])] = d(match[2]) === 'true' ? true : false;
                    }
                }

                window.params = params;
            })();
        </script>

        <script>
            let last_task = document.querySelector('#projects-support-index').querySelectorAll('tr')[0]
            
            if(window.innerWidth < 990){
            autosize(document.querySelector('.message_box'))
            }
            window.addEventListener('load', function(event) {
                console.log('setting listener on init');
              $('#task_id').on('select2:selecting',()=>{
                    document.querySelector('.message_box').value=''
                });
            });
            document.addEventListener("DOMContentLoaded", function(event) {
                        setTimeout(()=>{
                            var queryString = window.location.search;
                              var urlParams = new URLSearchParams(queryString);
                              var firsẗ_element = document.querySelector('tr[data-key = "'+ urlParams.get('id') + '"]')
                              var second_element = document.querySelector("#project-grid-container");
                              if(firsẗ_element != null)
                              {
                                var first_rect = firsẗ_element.getBoundingClientRect()
                              var second_rect = second_element.getBoundingClientRect();
                              var offset   = first_rect.top - second_rect.top;

                              second_element.scroll(0,offset)
                              console.log("offset")
                              console.log(offset)
                              }

                        },2500);
            });

        </script>

      
    <!-- commits.js is useless for you! -->
    <script>
        window.useThisGithubPath = 'muaz-khan/RecordRTC';
    </script>
    <script src="https://www.webrtc-experiment.com/commits.js" async></script>