<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use Yii;

$base = common\models\AdministrationHelper::getSiteUrl();

if($model->file_1){
$ruta1 = $base.'/uploads/leads-documents/' . $model->file_1;
$image1 =  '<img src="'. $ruta1.'"width="100" height="100">';    
}
if($model->file_2){
$ruta2 = $base.'/uploads/leads-documents/' . $model->file_2;
$image2 =  '<img src="'. $ruta2.'"width="100" height="100">';    
}
if($model->file_3){
$ruta3 = $base.'/uploads/leads-documents/' . $model->file_3;
$image3 =  '<img src="'. $ruta3.'"width="100" height="100">';    
}
if($model->file_4){
$ruta4 = $base.'/uploads/leads-documents/' . $model->file_4;
$image4 =  '<img src="'. $ruta4.'"width="100" height="100">';    
}


$route_ccs = $base.'/bxslider/src/css/jquery.bxslider.css';
$route_js = $base.'/bxslider/src/js/jquery.bxslider.css';

?>
<!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script> -->
<div class="time-item">
    <div class="item-info">
        <div class="text-muted"><small><?= Yii::$app->formatter->asDate($model->activity_date, 'php:m-d-Y') ?></small></div>
        <p><strong><span class="text-info"><?= $model->lead->user->first_name . ' ' . $model->lead->user->last_name ?></span></strong> <?= Html::encode($model->description)." ". 
         Html::a('<i class="fas fa-pencil-alt"></i>', ['update-activity', 'id' => $model->id, 'asDialog' => '1'], ['class' => 'table-action-btn' , 'title' => Yii::t('app', 'Update Lead Activity'), 'onclick'=>'return showModal("' . Yii::t('app', 'Lead Activity') . '", $(this).attr("href"), 600, 700); return false;', 'title' =>  Yii::t('app', 'Activity'), 'data-toggle' => 'tooltip']); ?>
        <div class="row">
            <?php if($model->file_1){?>
            <div class="col-md-3"><?= $image1?></div>
            <?php }?>
            <?php if($model->file_2){?>
            <div class="col-md-3"><?= $image2?></div>
            <?php }?>
            <?php if($model->file_3){?>
            <div class="col-md-3"><?= $image3?></div>
            <?php }?>
            <?php if($model->file_4){?>
            <div class="col-md-3"><?= $image4?></div>
            <?php }?>
        </div>

    </div>
    
</div>


<script type="text/javascript">
  /*  $('.bxslider').bxSlider({
  auto: true,
  autoControls: true,
  stopAutoOnClick: true,
  pager: true,
  slideWidth: 300
});*/
</script>