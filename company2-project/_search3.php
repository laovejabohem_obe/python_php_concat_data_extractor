<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use backend\components\UserHelpers;
use common\models\ClaimTask;
use common\models\Company2Project;
use common\models\Company2ProjectTask;
use common\models\Module;
use common\models\ProjectCategory;
use common\models\ProjectClass;
use common\models\ProjectsAttachments;
use common\models\ProjectStatus;
use common\models\ProjectTask;
use common\models\ProjectType;
use common\models\Task;
use common\models\UserSeasonState;
use common\models\ProjectMessages;

use kartik\editable\Editable;
use kartik\grid\GridView;
use kartik\popover\PopoverX;
use yii\data\ActiveDataProvider;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
use yii\widgets\Spaceless;


//if(Yii::$app->user->can('AS Staff')){$this->registerCss('.search .input-group-sm select.form-control{max-width: 110px !important;}');}

?>

<style type="text/css">


.p {
    width: 400px;
    margin: 0px auto !important;
    padding-top:auto !important;
    padding-bottom:auto !important;
    border: solid 0px #f60;
    font: normal 13px arial, helvetica, sans-serif;
    line-height: 25px;
}

.form-group {
    margin-bottom: 0rem !important;
}

 .bg-white {
  background-color:white !important;
  }

  </style>
  <?php


//if(Yii::$app->user->can('AS Staff')){$this->registerCss('.search .input-group-sm select.form-control{max-width: 110px !important;}');}

?>

<div id="search-form" class=" row justify-content-start form-group m-b-10">
    <?php $form = ActiveForm::begin([
        'action' => ['index', 'id' => $id, 'message' => $message, 'total' => $total],
        'method' => 'get',
        //'layout' => 'inline',
        'options' => ['data-pjax' => '1'],
        'fieldConfig'=>['template'=>' <div class="col mb-6 lg-6 mb-sm-0"> 
                        {label}{input}
                    </div>
                    ',
                    'inputOptions' => [
                    'placeholder' => 'Search',
                    'class'=>'form-control border-0',
                ]
            ]
    ]); ?>
    <?= html::hiddenInput('sort', Yii::$app->request->get('sort')) ?>
    <?= html::hiddenInput('page', Yii::$app->request->get('page')) ?>
    <?= Yii::$app->request->get('lastWeek') ? html::hiddenInput('lastWeek', Yii::$app->request->get('lastWeek')) : "" ?>
    <?= html::hiddenInput('company2-project', Yii::$app->request->get('company2-project')) ?>
    <?= Html::activeHiddenInput($model, 'is_outside_claim'); ?>

       
                                            <div class="col">
                                                <div class=" p-3 rounded">
                                                    <form class="needs-validation" novalidate="" name="chat-form" id="chat-form">
                                                        <div class="row">
                                                            <div class="col mb-12 mb-sm-0" style="margin-left:auto; margin-right:auto;">
                                                    
                                                                

                                                                <?= $form->field($model, 'search_str')->textInput(['placeholder' => "Search", 'class' =>"form-control border-0", 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Search by Project Class, Project p_id, Client Name,Last Name,  ,Client Phone #, or Client Email', ])->label(false) ?>
                                                                <!-- <div class="invalid-feedback">
                                                                    Please enter your messsage
                                                                </div> -->
                                                            </div>
                                                            <!--<div class="col-sm-auto">
                                                                <div class="btn-group">
                                                                    <?php  Html::a('<i class="fa fa-refresh"></i>', ['/company2-project/index', 'Company2ProjectSearch[is_active]' => '1', 'Company2Projectearch[is_outside_claim]' => $is_outside_claim, 'company2-project' => Yii::$app->request->get('company2-project')], ['class' => 'btn btn-success', 'data-pjax' => '0']) ?>
                                                                      <?php Html::submitButton('<i class="fe-search"></i>', ['id' => 'submit_frm_btn', 'class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?> -->
                                                                    <!--<button type="submit" class="btn btn-success chat-send w-100"><i class="fe-send"></i></button>-->
                                                              <!--  </div>-->
                                                            
                                                            <!-- end col -->
                                                        </div>
                                                        <!-- end row-->
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- end col-->
                                        <!-- end row -->
    <?php ActiveForm::end(); ?>
    <div class="clearfix"></div>
</div>












