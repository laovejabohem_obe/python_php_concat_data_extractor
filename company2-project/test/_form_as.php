<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use vova07\fileapi\Widget as FileAPI;
?>
<?php if (!$model->isNewRecord && $model->has_moved == 0) : ?>
    <div class="company1-claim-form">
        <h3><?= Yii::t('app', 'Project Information') ?></h3>
        <?php $form = ActiveForm::begin([
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            //             'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => '',
                    'offset' => '',
                    'wrapper' => 'col-sm-12',
                    'error' => '',
                    'hint' => '',
                ]
            ]
        ]); ?>
         <?= $form->errorSummary($model);?>


   <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
         <?= $form->field($model, 'country_id')->dropDownList($countries,['prompt' => Yii::t('app', 'Select Country'), 'onchange' => 'return getStatep(this.value,"contact_info"); return false;']) ?>
        <?= $form->field($model, 'state_id')->dropDownList($state,['prompt' => Yii::t('app', 'Select State'), 'onchange' => 'return getCityp(this.value, "contact_info"); return false;', 'disabled' => 'true']) ?>
        <?= $form->field($model, 'city_id')->dropDownList($user_city,['prompt' => Yii::t('app', 'Select City'), 'disabled' => 'true']) ?>
          <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-6 col-lg-6">
                        <?= $form->field($model, 'address')->textInput(['placeholder' => Yii::t('app', 'Address')]) ?>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <?= $form->field($model, 'address2')->textInput(['placeholder' => Yii::t('app', 'Address2')]) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'zipcode_id')->textInput() ?>
                    </div>
                     <div class="col-md-1" data-toggle="tooltip" data-placement="right" title="Please Click here for a map and directions to the location." >
                         <!-- Link Maps -->
                        <?php if ($model->isNewRecord){}else{echo  $model->map;}?>
                        <!--en link -->
                    </div>
                
            <div class="col-xs-12 col-sm-3 col-md-12 col-lg-12" style="display:block;">
            <?= $form->field($model, 'claim_description')->textarea(['placeholder' => Yii::t('app', 'Notes:')])->label('Notes:') ?>
            </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display:none">
                    <?= $form->field($membershipUserModel, 'selected_membership_offer')->dropDownList($membership_claim,
                        [
                        'onchange' => 'return getMembershipInfos(this.value,"membership_offer"); return false;', 'required' => true,"readonly"=>true]) ?>
                    <span class="membership-message col-xs-12 col-sm-12 col-md-12 col-lg-12 p-0 m-b-5" id="membership_msg"></span>

        </div>
       </div>
        
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="row">     
        <div class="col-xs-12 col-sm-3 col-md-6 col-lg-6">
        <?=  $form->field($model, 'project_category')->dropDownList($project_category,['prompt' => Yii::t('app', 'Select Project Category')]) ?>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-6 col-lg-6">
        <?=  $form->field($model, 'project_type')->dropDownList($project_type,['prompt' => Yii::t('app', 'Select Project Type')]) ?>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-4 col-lg-4">
        <?=  $form->field($model, 'project_class_id')->dropDownList($project_class,['prompt' => Yii::t('app', 'Select Project Class'),'onchange' => 'return getUM(this.value); return false;']) ?>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-4 col-lg-4">
        <?= $form->field($model, 'unit_measure')->textInput(['placeholder' => Yii::t('app', 'UM'), 'disabled' => 'true']) ?>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-4 col-lg-4">
        <?= $form->field($model, 'quantity')->textInput(['placeholder' => Yii::t('app', 'Quantity')]) ?>
        </div>
        <!-- News field -->
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" data-toggle="tooltip" data-placement="bottom" title="Replacement Cost Value">

             <?php
            echo $form->field($model, 'rcv', ['options' => ['class' => 'col-xs-12 padding-left0'], 'template' => '{label}<div class="input-group"> <div class="input-group-addon">$</div> {input} </div>{hint}{error}', 'inputOptions' => [
                'onchange'=>'calculate_cero()','class' => 'form-control just-app']])
            ->label(false)
            ->widget(\yii\widgets\MaskedInput::className(), ['clientOptions' => [
                'alias' => 'decimal',
                'digits' => 2,
                'digitsOptional' => false,
                'radixPoint' => '.',
                'groupSeparator' => ',',
                'autoGroup' => true,
                'removeMaskOnSubmit' => true,

            ]]) ->textInput(['placeholder' => Yii::t('app', 'RCV')]);
            ?>
        </div> 

       <!-- down payment %-->
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" data-toggle="tooltip" data-placement="bottom" title="Down Payment %">
             <?php
            echo $form->field($model, 'down_payment_percentage', ['options' => ['class' => 'col-xs-12 padding-left0'], 'template' => '{label}<div class="input-group"> <div class="input-group-addon">%</div> {input} </div>{hint}{error}', 'inputOptions' => ['onchange'=>'calculte_percentage()','class' => 'form-control just-app']])
            ->label(false)
            ->widget(\yii\widgets\MaskedInput::className(), ['clientOptions' => [
                'alias' => 'decimal',
                'digits' => 2,
                'digitsOptional' => false,
                'radixPoint' => '.',
                'groupSeparator' => ',',
                'autoGroup' => true,
                'removeMaskOnSubmit' => true,

            ]]) ->textInput(['placeholder' => Yii::t('app', 'Down %')]);
            ?>
        </div>
        <!-- end --> 
        
         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" data-toggle="tooltip" data-placement="bottom" title="Down Payment">
             <?php
            echo $form->field($model, 'down_payment', ['options' => ['class' => 'col-xs-12 padding-left0'], 'template' => '{label}<div class="input-group"> <div class="input-group-addon">$</div> {input} </div>{hint}{error}', 'inputOptions' => ['class' => 'form-control just-app']])
            ->label(false)
            ->widget(\yii\widgets\MaskedInput::className(), ['clientOptions' => [
                'alias' => 'decimal',
                'digits' => 2,
                'digitsOptional' => false,
                'radixPoint' => '.',
                'groupSeparator' => ',',
                'autoGroup' => true,
                'removeMaskOnSubmit' => true,

            ]]) ->textInput(['placeholder' => Yii::t('app', 'Down Payment')]);
            ?>
        </div> 


        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" data-toggle="tooltip" data-placement="bottom" title="Net Actual Cash Value Payment">
            <?=
                $form->field($model, 'nacvp', ['options' => ['class' => 'col-xs-12 padding-left0'], 'template' => '{label}<div class="input-group"> <div class="input-group-addon">$</div> {input} </div>{hint}{error}', 'inputOptions' => ['class' => 'form-control just-app']])
                    ->label(false)
                    ->widget(\yii\widgets\MaskedInput::className(), ['clientOptions' => [
                        'alias' => 'decimal',
                        'digits' => 2,
                        'digitsOptional' => false,
                        'radixPoint' => '.',
                        'groupSeparator' => ',',
                        'autoGroup' => true,
                        'removeMaskOnSubmit' => true,

                    ]]) ->textInput(['placeholder' => Yii::t('app', 'NACVP')])
            ?>
        </div> 
              <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <?= $form->field($model, 'coupon')->textInput(['placeholder' => 'Coupon code']) ?>
             <div><span class="membership-message col-xs-12 col-sm-12 col-md-12 col-lg-12 p-0 m-b-5" id="membership_msg_error"></span></div>
             <div><span class="membership-message col-xs-12 col-sm-12 col-md-12 col-lg-12 p-0 m-b-5" id="membership_msg_ok"></span></div>

            <?php $form->field($model, 'depreciation')->textInput(['value' => '0']); ?>

        </div> 
         <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
           <?= HTML::a('Apply','javascript:void(0)',['class' => 'btn-sm btn-success','onClick' => 'validateCoupon()']) ?>
        </div> 

        <div class="col-md-12">
           <div>
            <img style="display:none;width:160px;height: 150px; border:1px solid #EAEAEA; padding: 10px;" name="imagen" id="imagen" alt="Imagenes"></div>

                <div style="display:none;" >
                <div id="image-preview">
                <?= $form->field($model, 'image', ['template' => "{input}{error}"])->widget(
                    FileAPI::className(),[
                        'template' => '@app/views/site/home',
                        'browseGlyphicon' => false,
                        'settings' => [
                            'url'         => ['/client/fileapi-upload2'],
                            'progress' => '.js-progress',
                            'accept' => 'image/*',
                            'multiple' => false,
                            'elements'    => [
                                'active'=>['show' => '.js-upload', 'hide' => '.js-browse'],
                                'preview'=>['el'=> '.js-preview', 'width'=> Yii::$app->params['client_logo_size']['width'], 'height'=> Yii::$app->params['client_logo_size']['height']]
                            ],
                            'imageTransform' => ['maxWidth' => Yii::$app->params['client_image_size']['width'], 'maxHeight' => Yii::$app->params['client_logo_size']['height']],
                            'imageOriginal' => false,
                        ],
                        'preview'    => false,
                        'crop'    => true,
                        'jcropSettings' => [
                            //'minSize' => [Yii::$app->params['client_logo_size']['width'], Yii::$app->params['client_logo_size']['height']],
                            'maxSize' => [Yii::$app->params['client_logo_size']['max_width'], Yii::$app->params['client_logo_size']['max_height']],
                        ],
                        'callbacks' => [
                            'filecomplete' => ['
                            function(event,uiEvt){
                                var file_name = uiEvt.result.name;  
                                $.ajax({
                                    type: "GET",
                                    url: "'. Url::to(["/company1-claim/image"]).'",
                                    async:false,
                                    data: {file_name:file_name, id:"'.$model->id.'"},
                                    success: function(response){
                                        $("#image-preview img").attr({"src" : response});
                                        setTimeout(function(){$(".uploader-progress-bar-logo").width(0)}, 10000);
                                    }
                                });
                            }
                        ']
                        ],
                    ]
                )->label(false);
                ?></div></div></div>
    </div>
</div>
</div>
<h3><?= Yii::t('app', 'Client Information') ?></h3>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($clientModel, 'name')->textInput(['readonly' => true, 'maxlength' => true, 'placeholder' => Yii::t('app', 'First name')]) ?>
                <?= $form->field($clientModel, 'last_name')->textInput(['readonly' => true, 'maxlength' => true, 'placeholder' => Yii::t('app', 'Last name')]) ?>
                <?= $form->field($clientModel, 'nationality')->textInput(['readonly' => true, 'maxlength' => true, 'placeholder' => Yii::t('app', 'Nationality')]) ?>
                <?= $form->field($clientModel, 'email')->textInput(['readonly' => true, 'maxlength' => true, 'placeholder' => Yii::t('app', 'Email')]) ?>
                <?= $form->field($clientModel, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '999-999-9999','options' => ['readonly' => true, 'class' => 'form-control', 'placeholder' => Yii::t('app', 'Phone')],
            ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($clientModel, 'season_id')->dropDownList($seasons,['prompt' => Yii::t('app', 'Season'), 'onchange' => 'get_conferance(this.value)', 'readonly' => true])->label(false) ?>
                <?= $form->field($clientModel, 'state_id')->dropDownList($seasonstate, [ 'readonly' => true,])->label(false) ?>
                <?= $form->field($clientModel, 'region_id')->dropDownList($region,['readonly' => true]); ?>
                <?= $form->field($clientModel, 'city2_id')->dropDownList($city2s,['prompt' => Yii::t('app', 'City'), 'onchange' => 'return getdata("' . Yii::t('app', 'Zip Code') . '",this.value); return false;', 'readonly' => true]); ?>
               
                 <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($clientModel, 'address')->textInput(['readonly' => true, 'maxlength' => true, 'placeholder' => Yii::t('app', 'Address')]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($clientModel, 'address2')->textInput(['readonly' => true, 'maxlength' => true, 'placeholder' => Yii::t('app', 'Address2')]) ?>
                    </div>
                    <div class="col-md-2">
                         <?= $form->field($clientModel, 'zipcode_id')->dropDownList($zipcode2, ['prompt' => Yii::t('app', 'Zip Code'), 'onchange' => 'return allow_client(this.value); return false;', 'readonly' => true]); ?>
                    </div>
                    <div class="col-md-1" data-toggle="tooltip" data-placement="right" title="Please Click here for a map and directions to the location." >
                         <!-- Link Maps -->
                        <?php if ($clientModel->isNewRecord){}else{echo  $clientModel->map;}?>
                        <!--en link -->
                    </div>
                </div>
            </div> 
           
            
            
            <div class="col-sm-6"></div>

            <div id="logo_preview" class="col-sm-6" style="">
                <?= $form->field($clientModel, 'logo', ['template' => "{input}{error}"])->widget(
                    FileAPI::className(),[
                        'template' => '@app/views/site/_avatar_template',
                        'browseGlyphicon' => false,
                        'settings' => [
                            'url'         => ['/client/fileapi-upload'],
                            'progress' => '.js-progress',
                            'accept' => 'image/*',
                            'multiple' => false,
                            'elements'    => [
                                'active'=>['show' => '.js-upload', 'hide' => '.js-browse'],
                                'preview'=>['el'=> '.js-preview', 'width'=> Yii::$app->params['client_logo_size']['width'], 'height'=> Yii::$app->params['client_logo_size']['height']]
                            ],
                            'imageTransform' => ['maxWidth' => Yii::$app->params['client_logo_size']['width'], 'maxHeight' => Yii::$app->params['client_logo_size']['height']],
                            'imageOriginal' => false,
                        ],
                        'preview'    => false,
                        'crop'    => true,
                        'jcropSettings' => [
                            //'minSize' => [Yii::$app->params['client_logo_size']['width'], Yii::$app->params['client_logo_size']['height']],
                            'maxSize' => [Yii::$app->params['client_logo_size']['max_width'], Yii::$app->params['client_logo_size']['max_height']],
                        ],
                        'callbacks' => [
                            'filecomplete' => ['
                            function(event,uiEvt){
                                var file_name = uiEvt.result.name;
                                $.ajax({
                                    type: "GET",
                                    url: "'. Url::to(["/client/ajax-update-client-pic"]).'",
                                    async:false,
                                    data: {file_name:file_name, id:"'.$clientModel->id.'"},
                                    success: function(response){
                                        $("#logo_preview img").attr({"src" : response});
                                        setTimeout(function(){$(".uploader-progress-bar-logo").width(0)}, 10000);
                                    }
                                });
                            }
                        ']
                        ],
                    ]
                )->label(false);
                ?>

            </div>
            <div class="clearfix"></div>
          
        </div>
    <div style="display:none;" >
   
                <?= $form->field($model, 'client_id')->dropDownList($client, ['prompt' => Yii::t('app', 'Client'), 'onchange' => 'return allow_project(this.value); return false;']); ?>
     
                <h2 id="allow_project_msg" class="text-danger <?= ($allow_project) ? "hide" : "" ?>"><?= (!$allow_project) ? Yii::t('app', 'This Client has Reached its Maximum number of Claims. Please contact your Client`s Administrator') : "" ?></h2>
    </div>
    
    <h3><?= Yii::t('app', 'Adjuster Information') ?></h3>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($userModel, 'first_name')->textInput(['readonly' => true,'placeholder' => Yii::t('app', 'First Name')]) ?>
                <?= $form->field($userModel, 'last_name')->textInput(['readonly' => true,'placeholder' => Yii::t('app', 'Last Name')]) ?>
                <?= $form->field($userModel, 'email')->textInput(['readonly' => true,'placeholder' => Yii::t('app', 'Email')]) ?>
                <?= $form->field($userModel, 'dob')->widget(\kartik\date\DatePicker::classname(), [
                    'options' => ['readonly' => true,'placeholder' => Yii::t('app', 'Date Of Birth')],
                    'pluginOptions' => ['format' => 'mm-dd-yyyy', 'startDate' => '-100y', 'autoclose' => true, 'todayHighlight' => true]
                ]); ?>
                <?= $form->field($userModel, 'nationality')->textInput(['readonly' => true,'placeholder' => Yii::t('app', 'Nationality')]) ?>

                <?= $form->field($userModel, 'phone', ['horizontalCssClasses' => ['wrapper' => 'col-sm-12', 'label' => 'col-sm-3']])->widget(\yii\widgets\MaskedInput::className(), [
                    'options' => ['class' => 'form-control', 'readonly' => true,'placeholder' => Yii::t('app', 'Phone Number')],
                    'mask' => '999-999-9999',
                ]) ?>
                    
            <div class="row">
                <div class="col-md-6"  data-toggle="tooltip" data-placement="right" title="Emergency Contact" >    
                    <?= $form->field($userModel, 'emergency_contact')->textInput(['placeholder' => Yii::t('app', 'Emergency Contact')]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($userModel, 'emergency_phone')->widget(\yii\widgets\MaskedInput::className(), [
                    'options' => ['class' => 'form-control', 'placeholder' => Yii::t('app', 'Emergency Phone')],
                    'mask' => '999-999-9999',
                    ]) ?>
                </div>
            </div>
            </div>
            <div class="col-md-6">
                <?= $form->field($userModel, 'country_id')->dropDownList($countries, ['disabled' => true,'prompt' => Yii::t('app', 'Country'), 'onchange' => 'return getState(this.value,"contact_info"); return false;']) ?>
                <?= $form->field($userModel, 'state_id')->dropDownList($state, ['disabled' => true,'prompt' => Yii::t('app', 'Country'), 'onchange' => 'return getCity(this.value, "contact_info"); return false;']) ?>
                <?= $form->field($userModel, 'city_id')->dropDownList($user_city, ['disabled' => true,'prompt' => Yii::t('app', 'City')]) ?>
                <div class="row">
                    <div class="col-md-6">                        
                        <?= $form->field($userModel, 'address')->textInput(['readonly' => true, 'placeholder' => Yii::t('app', 'Address')]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($userModel, 'address1')->textInput(['readonly' => true, 'placeholder' => Yii::t('app', 'Address 2')]) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($userModel, 'zip_code', ['horizontalCssClasses' => ['wrapper' => 'col-sm-12', 'label' => 'col-sm-6']])->textInput(['readonly' => true, 'maxlength' => true, 'placeholder' => Yii::t('app', 'Zip Code')]) ?>
                    </div>
                    <div class="col-md-1" data-toggle="tooltip" data-placement="right" title="Please Click here for a map and directions to the location." >
                         <!-- Link Maps -->
                        <?php if ($userModel->isNewRecord){}else{echo  $userModel->map;}?>
                        <!--en link -->
                    </div>
                </div>


                <div class="row">
                    <div id="user-avatar-preview" class="col-md-4">
                        <?= $form->field($userModel, 'avatar', ['template' => "<div class=''>{input}</div><div class='col-9 p-r-0'>{error}</div><div class='clearfix'></div>"])->widget(
                            FileAPI::className(),
                            [
                                'template' => '@app/views/site/_avatar_template',
                                'browseGlyphicon' => false,
                                'settings' => [
                                    'url'         => ['/user/avatar-upload'],
                                    'progress' => '.js-progress',
                                    'accept' => 'image/*',
                                    'multiple' => false,
                                    'elements'    => [
                                        'active' => ['show' => '.js-upload', 'hide' => '.js-browse'],
                                        'preview' => ['el' => '.js-preview1', 'width' => Yii::$app->params['user_avatar']['width'], 'height' => Yii::$app->params['user_avatar']['height']]
                                    ],
                                    'imageTransform' => ['maxWidth' => Yii::$app->params['user_avatar']['width'], 'maxHeight' => Yii::$app->params['user_avatar']['height']],
                                    'imageOriginal' => false,
                                ],
                                'preview'    => false,
                                'crop'    => true,
                                'jcropSettings' => [
                                    //'minSize' => [Yii::$app->params['user_avatar']['width'], Yii::$app->params['user_avatar']['height']],
                                    'maxSize' => [Yii::$app->params['user_avatar']['max_width'], Yii::$app->params['user_avatar']['max_height']],
                                ],
                                'callbacks' => [
                                    'filecomplete' => ['
                                        function(event,uiEvt){
                                            var file_name = uiEvt.result.name;
                                            $.ajax({
                                                type: "GET",
                                                url: "' . Url::to(["/user/ajax-update-avatar-pic"]) . '",
                                                async:false,
                                                data: {file_name:file_name, id:"' . $userModel->id . '"},
                                                success: function(response){
                                                    $("#user-avatar-preview img").attr({"src" : response});
                                                    setTimeout(function(){$(".uploader-progress-bar").width(0)}, 10000);
                                                }
                                            });
                                        }']
                                ]
                            ]
                        )->label(false); ?>
                        <?= $form->field($userModel, 'gender')->inline()->radioList([1 => Yii::t('app', 'Male'), 0 => Yii::t('app', 'Female')], ['template' => "<div style='min-height:39px'><div class='col-sm-2'>{label}</div><div class='col-sm-10'>{input}</div>\n{hint}\n{error}</div>"]) ?>
                    </div>
                    <div style="display:none;" >
               
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-sm-12 text-right">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'), ['id' => 'submit_frm_btn', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?php if (Yii::$app->user->can('Staff1')) : ?>
                    <?= Html::a(Yii::t('app', 'Cancel'), ['/client/project', 'Company2ClaimSearch[is_active]' => '1'], ['class' => 'btn btn-default', 'data-pjax' => '0']); ?>
                <?php else : ?>
                    <?= Html::a(Yii::t('app', 'Cancel'), ['/company2-project/index', 'Company2ProjectSearch[is_active]' => '1', 'company1-claim' => 'acp'], ['class' => 'btn btn-default', 'data-pjax' => '0']); ?>
                <?php endif; ?>

            </div>
            <div class="clearfix"></div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<?php else : ?>
    <div class="col-sm-12">
        <h2 class="text-danger text-center">
            <?= Yii::t('app', 'Claim Is Moved To Other Client') ?>
        </h2>
    </div>
    <div class="clearfix"></div>
<?php endif; ?>

<script type="text/javascript">
    function validateCoupon()
    {
        var membership = $("#membershipuser-selected_membership_offer").val();
        var coupon = document.getElementById("company2project-coupon").value;
        //membership_msg_error
        //membership_msg_ok

        if(coupon != '')
        {
               $.ajax
               ({
                 async:true,
                 url: 'get-coupon',
                 type: 'GET',
                 dataType: 'html',
                 data: {membership : membership, coupon : coupon},
                 cache: false,
                 success: function(informacion){
                       
                          informacion=informacion.split('|');

                          if(informacion[0] == coupon){
                            discount = informacion[1];
                            document.getElementById("membership_msg_error").innerHTML = "<font color='#2BBBAD'>Coupon valid for a discount of: "+discount+"</font>";    
                          }
                          if(informacion[0] == ""){
                            document.getElementById("company2project-coupon").value="";
                            document.getElementById("membership_msg_error").innerHTML = "<font color='#a94442'>Coupon Invalid</font>";
                          }
                      }
                });
        }


    }


function getUM(project_class_id)
{
 
      $.ajax
    (
    {
             async:true,
             //url: 'estadistica',
              //url: '/site-helper/get-um',
             url: 'get-phone',

             type: 'GET',
             dataType: 'html',
             data: {project_class_id : project_class_id},
             cache: false,
             success: function(informacion){
                   
                      informacion=informacion.split('|');
                      $("#company2project-unit_measure").val(informacion[0]);
                      document.imagen.style.display = "block";
                      document.imagen.src = informacion[1];
                  }
                })
 

}
</script>

<?php
$old_client_id = $model->isNewRecord ? "" : $model->zipcode_id;   //// for validateion



if(is_file(\yii::getAlias('@frontend/web/uploads/user/') . $userModel->avatar)):
    $this->registerJs('$("#user-avatar-preview img").attr({"src" : "' . str_replace('index.php', '', Yii::$app->urlManagerFrontend->getBaseUrl()) . '/uploads/user/' . $userModel->avatar . '"});', view::POS_READY);
endif;

/*if(is_file(\yii::getAlias('@frontend/web/uploads/user/') . $userModel->avatar)):
    $this->registerJs('$("#company1addclient-avatar-preview img").attr({"src" : "' . str_replace('index.php', '', Yii::$app->urlManagerFrontend->getBaseUrl()) . '/uploads/user/' . $userModel->avatar . '"});', view::POS_READY);
endif;*/

if(is_file(\yii::getAlias('@frontend/web/uploads/client/') . $clientModel->logo)):
    $this->registerJs('$("#logo_preview img").attr({"src" : "' . str_replace('index.php', '', Yii::$app->urlManagerFrontend->getBaseUrl()) . '/uploads/client/' . $clientModel->logo. '"});', view::POS_READY);
endif;

if(is_file(\yii::getAlias('@frontend/web/uploads/project/') . $model->image)):
    $this->registerJs('$("#image-preview img").attr({"src" : "' . str_replace('index.php', '', Yii::$app->urlManagerFrontend->getBaseUrl()) . '/uploads/project/' . $model->image. '"});', view::POS_READY);
endif;

$this->registerJs('

function calculate_cero(zipcode_id)
{
        document.getElementById("company2project-down_payment_percentage").value=0;
        document.getElementById("company2project-down_payment").value = 0;
}


function calculte_percentage()
{
    var rcv_r = document.getElementById("company2project-rcv").value;
    rcv = rcv_r.replace(",", "");

    var down_payment_percentage_r =document.getElementById("company2project-down_payment_percentage").value;

    down_payment_percentage = down_payment_percentage_r.replace(",", "");

    if(down_payment_percentage > 100){

           message = "The down payment percentage cannot be greater than 100%!";

                  swal({
                    title              : "DOWN PAYMENT PERCENTAGE",
                    text               : message,
                    type               : "warning",
                    showCancelButton   : true,
                    closeOnConfirm     : false,
                    timer: 3000

                });

        //document.getElementById("company2project-rcv").value=0;
        document.getElementById("company2project-down_payment_percentage").value=0;
        document.getElementById("company2project-down_payment").value = 0;


    }else{

        result_payment_i = rcv * down_payment_percentage;
        result_payment = result_payment_i / 100;
        document.getElementById("company2project-down_payment").value = result_payment;
    }
}

$(function() {
   var project_class_id = $("#company2project-project_class_id").val();

    getUM(project_class_id);
    
});

    function getCity(id , type = "")
    {
        $.ajax({
            type: "GET",
            url: "' . Url::to(["/company2/get-city"]) . '",
            async:false,
            data: {"state_id":id},
            success: function(response)
            {
                if(type == "contact_info"){
                    $("#user-city_id").html(response);
                }else{
                    $("#company2-city_id").html(response);
                }
            }
        });
    }

    /*function getUM(project_class_id)
    {
    //Getting um from project class and asign to um camp
 
    $.ajax({
            url: "'.Url::to(["/site-helper/get-um"]).'",
            data: {project_class_id : project_class_id},
            success: function(response)
            {
             $("#company2project-unit_measure").val(response);
            }
        });

    // 

    }*/

    function getState(country_id , type = "")
    {
        $.ajax({
            url: "' . Url::to(["/company2/states-by-country"]) . '",
            data: {country_id : country_id},
            success: function(response)
            {
                if(type == "contact_info"){
                    $("#user-state_id").html("<option value=\"\">State</option>" + response);
                    $("#user-city_id").html("<option value=\"\">City</option>");
                }else{
                    $("#company2-state_id").html("<option value=\"\">State</option>" + response);
                    $("#company2-city_id").html("<option value=\"\">City</option>");
                }
            }
        });
    }
    $("input[type=\"radio\"][name=\"Company2Claim[project_return]\"]").click(function(){

        if($(this).val() == "1")
        {
            swal({
                title              : "' . Yii::t('app-sweet_alert', 'Are you sure?') . '",
                text               : "' . Yii::t('app-sweet_alert', 'You are returning user? \nif click yes then redirect to login page?') . '",
                type               : "warning",
                showCancelButton   : true,
                confirmButtonClass : "btn-warning",
                confirmButtonText  : "Yes",
                cancelButtonText   : "No",
                closeOnConfirm     : true
            },
            function(isConfirm){
                if(isConfirm){
                    window.location.href = "' . Url::to(["/site/index"]) . '";
                }
            });
        }

    });
    function getdata(get_data_of,id)
    {
        $.ajax({
            type: "GET",
            url: "' . Url::to(["/client/get-conf-tour-zipcode-client"]) . '",
             //async:false,
            data: {"get_data_of":get_data_of,"id":id},
            success: function(response)
            {
                if(get_data_of == "' . Yii::t('app', 'Region') . '")
                {
                    $("#company1project-region_id").html(response);
                }
                if(get_data_of == "' . Yii::t('app', 'City') . '")
                {
                    $("#company1project-city2_id").html(response);
                }
                if(get_data_of == "' . Yii::t('app', 'Zip Code') . '")
                {
                    $("#company1project-zipcode_id").html(response);
                }
                if(get_data_of == "' . Yii::t('app', 'Client') . '")
                {
                    $("#company1project-client_id").html(response);
                }
            }
        });
        if(get_data_of == "Client")
        {
            $.ajax({
                type: "GET",
                url: "' . Url::to(["/client/get-age"]) . '",
                 //async:false,
                dataType:"json",
                data: {"zipcode_id":id},
                success: function(response)
                {
                    jQuery("#user-dob-kvdate").kvDatepicker("setStartDate", response.start_date);
                    jQuery("#user-dob-kvdate").kvDatepicker("setEndDate", response.end_date);
                }
            });
        }
    }

    var project_allow = 0;
    function allow_project(client_id)
    {
        if(client_id)
        {
            var old_client_id = "' . $old_client_id . '";
            $.ajax({
                type: "GET",
                url: "' . Url::to(["/client/allow-project-per-client"]) . '",
                //async:false,
                data: {"client_id":client_id,"old_client_id":old_client_id},
                success: function(response)
                {
                    if(response == 1)
                    {
                        project_allow = 1;
                        alert("' . Yii::t('app', 'This Client has Reached its Maximum number of Claims. Please contact your Client`s Administrator') . '");

                    }
                    else
                    {
                        project_allow = 0;
                    }

                }
            });
        }else
        {
            project_allow = 0;
        }
    }
    $("#submit_frm_btn").click(function(){
        if(project_allow == 1)
        {
            alert("' . Yii::t('app', 'This Client has Reached its Maximum number of Claims. Please contact your Client`s Administrator') . '");
            return false;
        }
        else
        {
            return true;
        }
    });
', View::POS_END);

// disable season confreance city2 zipcode
$this->registerJs('
$("#company1project-season_id").attr({"disabled":"disabled"});
$("#company1project-region_id").attr({"disabled":"disabled"});
$("#company1project-city2_id").attr({"disabled":"disabled"});
$("#company1project-zipcode_id").attr({"disabled":"disabled"});
$("#company1project-client_id").attr({"disabled":"disabled"});
', View::POS_READY);
