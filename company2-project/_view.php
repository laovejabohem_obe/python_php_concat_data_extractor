<?php

use vova07\fileapi\Widget as FileAPI;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
?>
<?php if (!$model->isNewRecord && $model->has_moved == 0) : ?>
    <div class="company1-claim-form">
        <h4><?= Yii::t('app', 'Project Information') ?></h4>
        <?php $form = ActiveForm::begin([
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            //             'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => '',
                    'offset' => '',
                    'wrapper' => 'col-sm-12',
                    'error' => '',
                    'hint' => '',
                ]
            ]
        ]); ?>

        <div class="row">
            <div class="col-md-12">
                <h5>
                    Country: <?= $model->country->name ?><br>
                    State: <?= $model->state->name ?><br>
                    City: <?= $model->city->name ?><br>
                    Zip Code: <?= $model->zipcode_id ?>
                </h5>
            </div>
            <div class="col-md-6">
                <h5>
                    Address 2: <?= $model->address2 ?>
                </h5>
            </div>
            <div class="col-md-12">
            <h5>Complete Address: 
            <?php

            $add = $model->address . ", " . $model->city->name . ", " . $model->state->name . ", " . $model->zipcode_id;

            echo $add; ?>
                
            </h5>
        </div>
        </div>


        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                <h5>
                    Project Category: <?= $model->projectCategory->name ?>
                </h5>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                <h5>
                    Project Type: <?= $model->projectType->name ?>
                </h5>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                <h5>
                    Project Class: <?= $model->projectClass->name ?>
                </h5>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                <h5>
                    Unit Measure: <?= $model->unit_measure ?>
                </h5>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                <h5>
                    Quantity: <?= $model->quantity ?>
                </h5>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">

                <h5>
                    Down Payment Percentage: <?= $model->down_payment_percentage . "%"; ?>
                </h5>

            </div>
            <!-- News field -->
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                <h5>
                    R C V : <?= Yii::$app->formatter->asCurrency($model->rcv); ?>
                </h5>

            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">

                <h5>
                    Down Payment: <?= Yii::$app->formatter->asCurrency($model->down_payment); ?>
                </h5>

            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                <h5>
                    N A C V P: <?= Yii::$app->formatter->asCurrency($model->nacvp); ?>
                </h5>
            </div>
            <div class="col-md-12">
                <div><img style="display:none" name="imagen" id="imagen" alt="Imagenes"></div>

                <div style="display:none;">
                    <div id="image-preview">
                        <?= $form->field($model, 'image', ['template' => "{input}{error}"])->widget(
                            FileAPI::className(),
                            [
                                'template' => '@app/views/site/home',
                                'browseGlyphicon' => false,
                                'settings' => [
                                    'url'         => ['/client/fileapi-upload2'],
                                    'progress' => '.js-progress',
                                    'accept' => 'image/*',
                                    'multiple' => false,
                                    'elements'    => [
                                        'active' => ['show' => '.js-upload', 'hide' => '.js-browse'],
                                        'preview' => ['el' => '.js-preview', 'width' => Yii::$app->params['client_logo_size']['width'], 'height' => Yii::$app->params['client_logo_size']['height']]
                                    ],
                                    'imageTransform' => ['maxWidth' => Yii::$app->params['client_image_size']['width'], 'maxHeight' => Yii::$app->params['client_logo_size']['height']],
                                    'imageOriginal' => false,
                                ],
                                'preview'    => false,
                                'crop'    => true,
                                'jcropSettings' => [
                                    //'minSize' => [Yii::$app->params['client_logo_size']['width'], Yii::$app->params['client_logo_size']['height']],
                                    'maxSize' => [Yii::$app->params['client_logo_size']['max_width'], Yii::$app->params['client_logo_size']['max_height']],
                                ],
                                'callbacks' => [
                                    'filecomplete' => ['
                            function(event,uiEvt){
                                var file_name = uiEvt.result.name;  
                                $.ajax({
                                    type: "GET",
                                    url: "' . Url::to(["/company1-claim/image"]) . '",
                                    async:false,
                                    data: {file_name:file_name, id:"' . $model->id . '"},
                                    success: function(response){
                                        $("#image-preview img").attr({"src" : response});
                                        setTimeout(function(){$(".uploader-progress-bar-logo").width(0)}, 10000);
                                    }
                                });
                            }
                        ']
                                ],
                            ]
                        )->label(false);
                        ?></div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <h4><?= Yii::t('app', 'Client Information') ?></h4>
    <div class="row">
        <div class="col-md-4">
            <h5>
                Name: <?= $clientModel->name ?>
            </h5>
        </div>
        <div class="col-md-4">
            <h5>
                Last Name: <?= $clientModel->last_name ?>
            </h5>
        </div>
        <div class="col-md-4">
            <h5>
                Nationality: <?= $clientModel->nationality ?>
            </h5>
        </div>
        <div class="col-md-4">
            <h5>
                Email: <?= $clientModel->email ?>
            </h5>
        </div>
        <div class="col-md-4">
            <h5>
                Phone: <?= $clientModel->phone ?>
            </h5>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h5>
                Season: <?= $clientModel->season->name ?>
            </h5>
        </div>
        <div class="col-md-4">
            <h5>
                State: <?= $clientModel->state->name ?>
            </h5>
        </div>
        <div class="col-md-4">
            <h5>
                City: <?= $clientModel->city2->name ?>
            </h5>
        </div>
         <div class="col-md-4">
            <h5>
                Region: <?= $clientModel->region->name ?>
            </h5>
        </div>
        <div class="col-md-4">
            <h5>
                Zip Code: <?= $clientModel->zipcode->name ?>
            </h5>
        </div>

        <div class="col-md-12">
            <h5>Complete Address: 
            <?php

            $add = $clientModel->address . ", " . $clientModel->city2->name . ", " . $clientModel->state->name . ", " . $clientModel->zipcode->name;

            echo $add; ?>
                
            </h5>
        </div>
        <div class="col-md-6"></div>
        <div id="logo_preview" class="col-md-6" align="right">
            <?= $form->field($clientModel, 'logo', ['template' => "{input}{error}"])->widget(
                FileAPI::className(),
                [
                    'template' => '@app/views/site/_avatar_template',
                    'browseGlyphicon' => false,
                    'settings' => [
                        'url'         => ['/client/fileapi-upload'],
                        'progress' => '.js-progress',
                        'accept' => 'image/*',
                        'multiple' => false,
                        'elements'    => [
                            'active' => ['show' => '.js-upload', 'hide' => '.js-browse'],
                            'preview' => ['el' => '.js-preview', 'width' => Yii::$app->params['client_logo_size']['width'], 'height' => Yii::$app->params['client_logo_size']['height']]
                        ],
                        'imageTransform' => ['maxWidth' => Yii::$app->params['client_logo_size']['width'], 'maxHeight' => Yii::$app->params['client_logo_size']['height']],
                        'imageOriginal' => false,
                    ],
                    'preview'    => false,
                    'crop'    => true,
                    'jcropSettings' => [
                        //'minSize' => [Yii::$app->params['client_logo_size']['width'], Yii::$app->params['client_logo_size']['height']],
                        'maxSize' => [Yii::$app->params['client_logo_size']['max_width'], Yii::$app->params['client_logo_size']['max_height']],
                    ],
                    'callbacks' => [
                        'filecomplete' => ['
                            function(event,uiEvt){
                                var file_name = uiEvt.result.name;
                                $.ajax({
                                    type: "GET",
                                    url: "' . Url::to(["/client/ajax-update-client-pic"]) . '",
                                    async:false,
                                    data: {file_name:file_name, id:"' . $clientModel->id . '"},
                                    success: function(response){
                                        $("#logo_preview img").attr({"src" : response});
                                        setTimeout(function(){$(".uploader-progress-bar-logo").width(0)}, 10000);
                                    }
                                });
                            }
                        ']
                    ],
                ]
            )->label(false);
            ?>

        </div>
    </div>

    <h4><?= Yii::t('app', 'Supervisor Information') ?></h4>
    <div class="row">
        <div class="col-md-4">
            <h5>
                Name: <?= $userModel->first_name ?>
            </h5>
        </div>
        <div class="col-md-4">
            <h5>
                Last Name: <?= $userModel->last_name ?>
            </h5>
        </div>
        <div class="col-md-4">
            <h5>
                Nationality: <?= $userModel->nationality ?>
            </h5>
        </div>
        <div class="col-md-4">
            <h5>
                Email: <?= $userModel->email ?>
            </h5>
        </div>
        <div class="col-md-4">
            <h5>
                Phone: <?= $userModel->phone ?>
            </h5>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-6"></div>
        <div id="user-avatar-preview" class="col-md-6" align="right">
                    <?= $form->field($userModel, 'avatar', ['template' => "<div class=''>{input}</div><div class='col-9 p-r-0'>{error}</div><div class='clearfix'></div>"])->widget(
                        FileAPI::className(),
                        [
                            'template' => '@app/views/site/_avatar_template',
                            'browseGlyphicon' => false,
                            'settings' => [
                                'url'         => ['/user/avatar-upload'],
                                'progress' => '.js-progress',
                                'accept' => 'image/*',
                                'multiple' => false,
                                'elements'    => [
                                    'active' => ['show' => '.js-upload', 'hide' => '.js-browse'],
                                    'preview' => ['el' => '.js-preview1', 'width' => Yii::$app->params['user_avatar']['width'], 'height' => Yii::$app->params['user_avatar']['height']]
                                ],
                                'imageTransform' => ['maxWidth' => Yii::$app->params['user_avatar']['width'], 'maxHeight' => Yii::$app->params['user_avatar']['height']],
                                'imageOriginal' => false,
                            ],
                            'preview'    => false,
                            'crop'    => true,
                            'jcropSettings' => [
                                //'minSize' => [Yii::$app->params['user_avatar']['width'], Yii::$app->params['user_avatar']['height']],
                                'maxSize' => [Yii::$app->params['user_avatar']['max_width'], Yii::$app->params['user_avatar']['max_height']],
                            ],
                            'callbacks' => [
                                'filecomplete' => ['
                                        function(event,uiEvt){
                                            var file_name = uiEvt.result.name;
                                            $.ajax({
                                                type: "GET",
                                                url: "' . Url::to(["/user/ajax-update-avatar-pic"]) . '",
                                                async:false,
                                                data: {file_name:file_name, id:"' . $userModel->id . '"},
                                                success: function(response){
                                                    $("#user-avatar-preview img").attr({"src" : response});
                                                    setTimeout(function(){$(".uploader-progress-bar").width(0)}, 10000);
                                                }
                                            });
                                        }']
                            ]
                        ]
                    )->label(false); ?>
                </div>
                <div class="col-md-12">
            <h5>
                    Notes: <?= $model->claim_description ?>
            </h5>

        </div>
            
        </div>
        


    </div>
  



<?php /*

        <div class="col-md-6">
            <?= $form->field($userModel, 'first_name')->textInput(['disabled' => true, 'placeholder' => Yii::t('app', 'First Name')]) ?>
            <?= $form->field($userModel, 'last_name')->textInput(['disabled' => true, 'placeholder' => Yii::t('app', 'Last Name')]) ?>
            <?= $form->field($userModel, 'email')->textInput(['disabled' => true, 'placeholder' => Yii::t('app', 'Email')]) ?>
            <?= $form->field($userModel, 'dob')->widget(\kartik\date\DatePicker::classname(), [
                'options' => ['disabled' => true, 'placeholder' => Yii::t('app', 'Date Of Birth')],
                'pluginOptions' => ['format' => 'mm-dd-yyyy', 'startDate' => '-100y', 'autoclose' => true, 'todayHighlight' => true]
            ]); ?>
            <?= $form->field($userModel, 'nationality')->textInput(['disabled' => true, 'placeholder' => Yii::t('app', 'Nationality')]) ?>
            <?= $form->field($userModel, 'gender')->inline()->radioList([1 => Yii::t('app', 'Male'), 0 => Yii::t('app', 'Female')], ['template' => "<div style='min-height:39px'><div class='col-sm-2'>{label}</div><div class='col-sm-10'>{input}</div>\n{hint}\n{error}</div>"]) ?>
            <?= $form->field($userModel, 'emergency_contact')->textInput(['disabled' => true, 'placeholder' => Yii::t('app', 'Emergency Contact')]) ?>
            <?= $form->field($userModel, 'emergency_phone')->widget(\yii\widgets\MaskedInput::className(), [
                'options' => ['class' => 'form-control', 'disabled' => true, 'placeholder' => Yii::t('app', 'Emergency Phone')],
                'mask' => '999-999-9999',
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($userModel, 'country_id')->dropDownList($countries, ['disabled' => true, 'prompt' => Yii::t('app', 'Country'), 'onchange' => 'return getState(this.value,"contact_info"); return false;']) ?>
            <?= $form->field($userModel, 'state_id')->dropDownList($state, ['disabled' => true, 'prompt' => Yii::t('app', 'Country'), 'onchange' => 'return getCity(this.value, "contact_info"); return false;']) ?>
            <?= $form->field($userModel, 'city_id')->dropDownList($user_city, ['disabled' => true, 'prompt' => Yii::t('app', 'City')]) ?>
            <?= $form->field($userModel, 'address')->textInput(['disabled' => true, 'placeholder' => Yii::t('app', 'Address')]) ?>
            <?= $form->field($userModel, 'address1')->textInput(['disabled' => true, 'placeholder' => Yii::t('app', 'Address 2')]) ?>
            <?= $form->field($userModel, 'zip_code', ['horizontalCssClasses' => ['wrapper' => 'col-sm-12', 'label' => 'col-sm-12']])->textInput(['maxlength' => true, 'disabled' => true, 'placeholder' => Yii::t('app', 'Zip Code')]) ?>
            <?= $form->field($userModel, 'phone', ['horizontalCssClasses' => ['wrapper' => 'col-sm-12', 'label' => 'col-sm-12']])->widget(\yii\widgets\MaskedInput::className(), [
                'options' => ['class' => 'form-control', 'disabled' => true, 'placeholder' => Yii::t('app', 'Phone Number')],
                'mask' => '999-999-9999',
            ]) ?>
            */?>

            
    <?php ActiveForm::end(); ?>
    </div>
<?php else : ?>
    <div class="col-sm-12">
        <h2 class="text-danger text-center">
            <?= Yii::t('app', 'Claim Is Moved To Other Client') ?>
        </h2>
    </div>
    <div class="clearfix"></div>
<?php endif; ?>

<script type="text/javascript">
    function getUM(project_class_id) {

        $.ajax({
            async: true,
            //url: 'estadistica',
            //url: '/site-helper/get-um',
            url: 'get-phone',

            type: 'GET',
            dataType: 'html',
            data: {
                project_class_id: project_class_id
            },
            cache: false,
            success: function(informacion) {

                informacion = informacion.split('|');
                $("#company2project-unit_measure").val(informacion[0]);
                document.imagen.style.display = "block";
                document.imagen.src = informacion[1];
            }
        })


    }
</script>

<?php
$old_client_id = $model->isNewRecord ? "" : $model->zipcode_id;   //// for validateion



if (is_file(\yii::getAlias('@frontend/web/uploads/user/') . $userModel->avatar)) :
    $this->registerJs('$("#user-avatar-preview img").attr({"src" : "' . str_replace('index.php', '', Yii::$app->urlManagerFrontend->getBaseUrl()) . '/uploads/user/' . $userModel->avatar . '"});', view::POS_READY);
endif;

/*if(is_file(\yii::getAlias('@frontend/web/uploads/user/') . $userModel->avatar)):
    $this->registerJs('$("#company1addclient-avatar-preview img").attr({"src" : "' . str_replace('index.php', '', Yii::$app->urlManagerFrontend->getBaseUrl()) . '/uploads/user/' . $userModel->avatar . '"});', view::POS_READY);
endif;*/

if (is_file(\yii::getAlias('@frontend/web/uploads/client/') . $clientModel->logo)) :
    $this->registerJs('$("#logo_preview img").attr({"src" : "' . str_replace('index.php', '', Yii::$app->urlManagerFrontend->getBaseUrl()) . '/uploads/client/' . $clientModel->logo . '"});', view::POS_READY);
endif;

if (is_file(\yii::getAlias('@frontend/web/uploads/project/') . $model->image)) :
    $this->registerJs('$("#image-preview img").attr({"src" : "' . str_replace('index.php', '', Yii::$app->urlManagerFrontend->getBaseUrl()) . '/uploads/project/' . $model->image . '"});', view::POS_READY);
endif;

$this->registerJs('

$(function() {
   var project_class_id = $("#company2project-project_class_id").val();

    getUM(project_class_id);
    
});

    function getCity(id , type = "")
    {
        $.ajax({
            type: "GET",
            url: "' . Url::to(["/company2/get-city"]) . '",
            async:false,
            data: {"state_id":id},
            success: function(response)
            {
                if(type == "contact_info"){
                    $("#user-city_id").html(response);
                }else{
                    $("#company2-city_id").html(response);
                }
            }
        });
    }

    /*function getUM(project_class_id)
    {
    //Getting um from project class and asign to um camp
 
    $.ajax({
            url: "' . Url::to(["/site-helper/get-um"]) . '",
            data: {project_class_id : project_class_id},
            success: function(response)
            {
             $("#company2project-unit_measure").val(response);
            }
        });

    // 

    }*/

    function getState(country_id , type = "")
    {
        $.ajax({
            url: "' . Url::to(["/company2/states-by-country"]) . '",
            data: {country_id : country_id},
            success: function(response)
            {
                if(type == "contact_info"){
                    $("#user-state_id").html("<option value=\"\">State</option>" + response);
                    $("#user-city_id").html("<option value=\"\">City</option>");
                }else{
                    $("#company2-state_id").html("<option value=\"\">State</option>" + response);
                    $("#company2-city_id").html("<option value=\"\">City</option>");
                }
            }
        });
    }
    $("input[type=\"radio\"][name=\"Company2Claim[project_return]\"]").click(function(){

        if($(this).val() == "1")
        {
            swal({
                title              : "' . Yii::t('app-sweet_alert', 'Are you sure?') . '",
                text               : "' . Yii::t('app-sweet_alert', 'You are returning user? \nif click yes then redirect to login page?') . '",
                type               : "warning",
                showCancelButton   : true,
                confirmButtonClass : "btn-warning",
                confirmButtonText  : "Yes",
                cancelButtonText   : "No",
                closeOnConfirm     : true
            },
            function(isConfirm){
                if(isConfirm){
                    window.location.href = "' . Url::to(["/site/index"]) . '";
                }
            });
        }

    });
    function getdata(get_data_of,id)
    {
        $.ajax({
            type: "GET",
            url: "' . Url::to(["/client/get-conf-tour-zipcode-client"]) . '",
             //async:false,
            data: {"get_data_of":get_data_of,"id":id},
            success: function(response)
            {
                if(get_data_of == "' . Yii::t('app', 'Region') . '")
                {
                    $("#company1project-region_id").html(response);
                }
                if(get_data_of == "' . Yii::t('app', 'City') . '")
                {
                    $("#company1project-city2_id").html(response);
                }
                if(get_data_of == "' . Yii::t('app', 'Zip Code') . '")
                {
                    $("#company1project-zipcode_id").html(response);
                }
                if(get_data_of == "' . Yii::t('app', 'Client') . '")
                {
                    $("#company1project-client_id").html(response);
                }
            }
        });
        if(get_data_of == "Client")
        {
            $.ajax({
                type: "GET",
                url: "' . Url::to(["/client/get-age"]) . '",
                 //async:false,
                dataType:"json",
                data: {"zipcode_id":id},
                success: function(response)
                {
                    jQuery("#user-dob-kvdate").kvDatepicker("setStartDate", response.start_date);
                    jQuery("#user-dob-kvdate").kvDatepicker("setEndDate", response.end_date);
                }
            });
        }
    }

    var project_allow = 0;
    function allow_project(client_id)
    {
        if(client_id)
        {
            var old_client_id = "' . $old_client_id . '";
            $.ajax({
                type: "GET",
                url: "' . Url::to(["/client/allow-project-per-client"]) . '",
                //async:false,
                data: {"client_id":client_id,"old_client_id":old_client_id},
                success: function(response)
                {
                    if(response == 1)
                    {
                        project_allow = 1;
                        alert("' . Yii::t('app', 'This Client has Reached its Maximum number of Claims. Please contact your Client`s Administrator') . '");

                    }
                    else
                    {
                        project_allow = 0;
                    }

                }
            });
        }else
        {
            project_allow = 0;
        }
    }
    $("#submit_frm_btn").click(function(){
        if(project_allow == 1)
        {
            alert("' . Yii::t('app', 'This Client has Reached its Maximum number of Claims. Please contact your Client`s Administrator') . '");
            return false;
        }
        else
        {
            return true;
        }
    });
', View::POS_END);

// disable season confreance city2 zipcode
$this->registerJs('
$("#company1project-season_id").attr({"disabled":"disabled"});
$("#company1project-region_id").attr({"disabled":"disabled"});
$("#company1project-city2_id").attr({"disabled":"disabled"});
$("#company1project-zipcode_id").attr({"disabled":"disabled"});
$("#company1project-client_id").attr({"disabled":"disabled"});
', View::POS_READY);
