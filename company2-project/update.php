<?php

use yii\helpers\Html;
use common\models\ProjectCategory;
use common\models\ProjectClass;
use common\models\ProjectType;
use common\models\UserSeasonState;



$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Contractor Project',
]) . $model->p_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contractor Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
               <div class="card-header text-uppercase text-card-layout" align="right">

                <?= Html::a('<font size= 6><i class="fa fa-times-circle"></i></font>',['/company2-project/index2',
                     'id' => $model->id, 
                     'Company2ProjectSearch[is_active]' => '1',
                     'Company2ProjectSearch[id_status]' => $model->id_status, 
                     'company2project' => 'c1ca',
                     'message' => 'All Projects', 
                     //'total' => $statistics_status['total_pending'], 
                     //'color' => 'text-orange'
                    ], ['class' => 'closed-red'])  ?>

            </div>
            <br>
            <h4 class="m-t-0 m-b-30 header-title">
                <b><?= Html::encode($this->title) ?></b>
            </h4>

            <div class="company2-project-update">

                <?= $this->render('_form', [
                    'model'              => $model,
                    'userModel'          => $userModel,
                    //'userRecordModel'    => $userRecordModel,
                    'countries'          => $countries,
                    'seasons'            => $seasons,
                    'region'             => $region,
                    'cities2'            => $cities2,
                    'city2s'             => $city2s,
                    'zipcode'            => $zipcode,
                    'zipcode2'           =>$zipcode2,
                    'client'             => $client,
                    'project_position'   => $project_position,
                    'state'              => $state,
                    'user_city'          => $user_city,
                    'allow_project'      => $allow_project,
                    'project_category'     => ProjectCategory::getProjectCategoryDropDrown(ProjectCategory::COMPANY2,true),
                    'project_class'  => ProjectClass::getProjectClassDropDrown(ProjectClass::COMPANY2,true),
                    'project_type'         => ProjectType::getProjectTypeDropDrown(ProjectType::COMPANY2,true),
                    'clientModel'        => $clientModel,
                    'user_city2'         => $user_city2,
                    'membershipUserModel'    => $membershipUserModel,
                    'membership_offer'        => $membership_offer,
                    'membership_claim' => $membership_claim,
                    'seasonstate' => UserSeasonState::getSeasonStateDropDrownByUserType($model->client->season_id),



                ]) ?>

            </div>
        </div>
    </div>
</div>