
<?php
use common\models\CompanyGeneralUrlHandler;
$url_params=Yii::$app->request->queryParams;
$allProjects=CompanyGeneralUrlHandler::getLinkWithCheckedParams(['/company2-project/index2', 
'Company2ProjectSearch[id_status]' => '', 
'company2-project' => 'c1ct', 
'message' => 'All Projects', 
'total' => $statistics_total['total'], 
'condition_total' => 'total_new', 
'color' => 'text-custom'],$url_params);
$newProjects = CompanyGeneralUrlHandler::getLinkWithCheckedParams(['/company2-project/index2', 

'Company2ProjectSearch[is_active]' => '1',
'Company2ProjectSearch[id_status]' => '1',
'company2project' => 'c1ct', 

'message' => 'New Projects',
'total' => $statistics_status['total_new'],
'color' => 'text-aquamarine' ],$url_params);
$pendingProjects =CompanyGeneralUrlHandler::getLinkWithCheckedParams(['/company2-project/index2',
'Company2ProjectSearch[is_active]' => '1',
'Company2ProjectSearch[id_status]' => '2', 
'company2project' => 'c1ca',
'message' => 'Pending Projects', 
'total' => $statistics_status['total_pending'], 
'color' => 'text-orange'],$url_params);
$approvedProjects= CompanyGeneralUrlHandler::getLinkWithCheckedParams(['/company2-project/index2', 
'Company2ProjectSearch[is_active]' => '1',
'Company2ProjectSearch[id_status]' => '5', 
'company2project' => 'c1ci',  
'message' => 'Approved Projects', 
'total' => $statistics_status['total_approved'],
'color' => 'text-blue'
],$url_params);
$inProgressProjects=CompanyGeneralUrlHandler::getLinkWithCheckedParams(['/company2-project/index2', 
'Company2ProjectSearch[is_active]' => '1',
'Company2ProjectSearch[id_status]' => '6',
'company2project' => 'c1ct', 
'message' => 'in-progress Projects', 
'total' => $statistics_status['total_progress'], 
'color' => 'text-pending'],$url_params);
$completedProjects=CompanyGeneralUrlHandler::getLinkWithCheckedParams(['/company2-project/index2',
'Company2ProjectSearch[is_active]' => '1',
'Company2ProjectSearch[id_status]' => '7',
'company2project' => 'c1ca',  
'message' => 'Completed Projects', 
'total' => $statistics_status['total_completed'], 
'color' => 'text-completed'],$url_params);
$lostProjects=CompanyGeneralUrlHandler::getLinkWithCheckedParams(['/company2-project/index2', 
'Company2ProjectSearch[is_active]' => '1',
'Company2ProjectSearch[id_status]' => '8', 
'company2project' => 'c1ca',  
'message' => 'Lost Projects', 
'total' => $statistics_status['total_lost'], 
'color' => 'text-unassigned'],$url_params);


$materialOrder=CompanyGeneralUrlHandler::getLinkWithCheckedParams(['/material-order/index',
'Company2ProjectSearch[is_active]' => '', 
'company2-project' => 'mlp', 
'message' => 'All Material Orders',
'color' => 'text-stock'],$url_params);
$afilliates=CompanyGeneralUrlHandler::getLinkWithCheckedParams(['/project-affiliate/index', 'ProjectAffiliateSearch[affiliate_option]' => '0', 'project-affiliate' => 'mlp', 'message' => 'All Affiliate Projects'],$url_params) ;
?>
<div class="icons_container">
    <div class="icons m-auto">
        <a data-toggle="tooltip" title="All Projects" data-placement= "top" href="<?= $allProjects?>" selected-message="<?=$message?>">
        <i class="icon-projects"></i>
        </a>
        <a data-toggle="tooltip" title="New Projects" data-placement= "top" href="<?= $newProjects?>" selected-message="<?=$message?>">
        <i class="icon-new   "></i>
        </a>
        <a data-toggle="tooltip" title="Pending Projects" data-placement= "top" href="<?= $pendingProjects?>" selected-message="<?=$message?>">
        <i class="icon-sand-clock-2 "></i>
        </a>
        <a data-toggle="tooltip" title="Approved Projects" data-placement= "top" href="<?= $approvedProjects?>" selected-message="<?=$message?>">
        <i class="icon-handshake-2 "></i>
        </a>
        <a data-toggle="tooltip" title="In-progress Projects" data-placement= "top" href="<?= $inProgressProjects?>" selected-message="<?=$message?>">
        <i class="fa fa-refresh "></i>
        </a>
        <a data-toggle="tooltip" title="Completed Projects" data-placement= "top" href="<?= $completedProjects?>" selected-message="<?=$message?>">
        <i class="glyphicon glyphicon-check "></i>
        </a>
        <a data-toggle="tooltip" title="Lost Projects" data-placement= "top" href="<?= $lostProjects?>" selected-message="<?=$message?>">
        <i class="icon-x-mark "></i>
        </a>
        <a data-toggle="tooltip" title="All Material Orders" data-placement= "top" href="<?= $materialOrder?>" selected-message="<?=$message?>">
        <i class="icon-inspections   "></i>
        </a>
        <a data-toggle="tooltip" title="Affiliates" data-placement= "top" href="<?= $afilliates?>" selected-message="<?=$message?>">
        <i class="icon-contractor   "></i>
        </a>
    </div>
    <hr>
    <?php
        if($message== "All Projects"){echo '<span id="result_value" >'.$message.': '.$statistics_status["total"].'</span>';}
        if($message== "New Projects"){echo '<span id="result_value">'.$message.': '.$statistics_status["total_new"].'</span>';}
        if($message== "Pending Projects"){echo '<span id="result_value">'.$message.': '.$statistics_status["total_pending"].'</span>';}
        if($message== "Approved Projects"){echo '<span id="result_value">'.$message.': '.$statistics_status["total_approved"].'</span>';}
        if($message== "in-progress Projects"){echo '<span id="result_value">'.$message.': '.$statistics_status["total_progress"].'</span>';}
        if($message== "Completed Projects"){echo '<span id="result_value">'.$message.': '.$statistics_status["total_completed"].'</span>';}
        if($message== "Lost Projects"){echo '<span id="result_value">'.$message.': '.$statistics_status["total_lost"].'</span>';}
    ?>
</div>