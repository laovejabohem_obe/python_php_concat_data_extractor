<?php
use Yii;
use backend\components\UserHelpers;
use yii\helpers\Html;
use common\models\CompanyGeneralUrlHandler;

    $queryParams=Yii::$app->request->queryParams;
    if(isset($queryParams['id']))
    {
        $selected = $queryParams['id'] == $model->id;
    }
    $class_project = $selected == true ? 'project_selected': 'project_selectable';
    $status ='';

    $controller = Yii::$app->controller;
    if ($condition_total == "total_new") {

        $arrayParams = ['id' => $model['id'], /*'Company2ProjectSearch[id_status]' => $model['id_status'],*/ 'total' => $total, 'message' => $message, 'responsive' => '1', 'page' => $page];
    } else {

        if ($message == "All Projects") {

            $arrayParams = ['id' => $model['id'], 'total' => $total, 'message' => $message, 'responsive' => '1', 'page' => $page];
        } else {

            $arrayParams = ['id' => $model['id'], 'Company2ProjectSearch[is_active]' => '1', 'Company2ProjectSearch[id_status]' => $model['id_status'], 'total' => $total, 'message' => $message, 'responsive' => '1', 'page' => $page];
        }
    }
    $params = array_merge(["{$controller->id}/{$controller->action->id}"], $arrayParams);
    //verify if its a search state if not allows to add the first_page param, this gives information of context
    if ($params['page'] == '' && !isset(Yii::$app->request->queryParams['Company2ProjectSearch']['search_str'])) {
        $params['first_page'] = true;
    }
    //in case of any search you will be redirected to the first page automatically
    if (isset(Yii::$app->request->queryParams['Company2ProjectSearch']['search_str'])) {
        $params['page'] = '';
    }
    $params[0] = 'company2-project/index2';
    if(Yii::$app->user->can('AS Staff'))
    {
        $status_avaible=[];
        switch ($model->projectStatus->name) 
        {
            case 'In-progress':
                $status_avaible=[
                    'Completed' => 7,
                    'Lost' => 8,
                ];
                break;
            case 'Approved':
                $status_avaible=[
                    'In-progress' => 6,
                    'Completed' => 7,
                    'Lost' => 8
                ];
                break;
            default:

                break;
        }
    }


?>
<div class="<?=$class_project?>" data-project-url="<?= CompanyGeneralUrlHandler::concatShowAllParam(Yii::$app->urlManager->createUrl($params),$queryParams)?>" data-key="<?=$model->id?>">
<?=
 UserHelpers::getContractorImage(32, 32, $model->client->company2->id, true,false);           
?>
<div class="project_data" >
                <h6 class="project_title">
                    <?=Yii::$app->user->can('Staff1') ? $model->client->name . ' ' . $model->client->last_name : Html::a($model->client->name . ' ' . $model->client->last_name, [$action . '/client/update', 'id' => $model->client->id, 'comesFrom' => Yii::$app->request->url], ['data-pjax' => '0'])?></h6>
                <div class="extra-data">
                    <span class="ph">PH : <?= $model->client->phone;?> </span>
                    <span class="direction"><?=$model->map2?></span>
                </div>
            </div>
            <?php if($model->is_active == true):?>
            <button class="status_change_selector" project_id="<?= $model->id?>" onclick="openChangeStatusOption(this)" status_identity="<?=strtolower($model->projectStatus->name);?>" ><i class="fa fa-pencil" aria-hidden="true" id_status="<?= $model->id_status?>" ></i>
                <?=$model->projectStatus->name?></button>
                <?php if(isset($status_avaible)): ?>
                    <ul class="status_change_list " project_id="<?= $model->id?>">
                        <?php foreach($status_avaible as $key => $value):?>
                            <button class="status_change_selector changer_selectionable" status_identity="<?= strtolower($key)?>" onclick="change_project_status2(<?=$model->id?>,<?=$model->rcv?>,<?=$value?>,<?=$model->id?>,this)" >
                                <?=$key?>
                            </button>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>
            <?php else:?>
                <button class="status_change_selector inactive" status_identity="Inactive Project" ><i class="fa fa-times" aria-hidden="true" id_status="<?= $model->id_status?>"></i>
                Inactive Project</button>

            <?php endif;?>
</div>