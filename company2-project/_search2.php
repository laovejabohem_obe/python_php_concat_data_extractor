<?php
use Yii;
use yii\bootstrap\ActiveForm;

use backend\components\UserHelpers;
use common\models\ClaimTask;
use common\models\Company2Project;
use common\models\Company2ProjectTask;
use common\models\Module;
use common\models\ProjectCategory;
use common\models\ProjectClass;
use common\models\ProjectsAttachments;
use common\models\ProjectStatus;
use common\models\ProjectTask;
use common\models\ProjectType;
use common\models\Task;
use common\models\UserSeasonState;
use common\models\ProjectMessages;

use kartik\editable\Editable;
use kartik\grid\GridView;
use kartik\popover\PopoverX;
use yii\data\ActiveDataProvider;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
use yii\widgets\Spaceless;

use common\models\CompanyGeneralUrlHandler;
use yii\widgets\ListView;

//if(Yii::$app->user->can('AS Staff')){$this->registerCss('.search .input-group-sm select.form-control{max-width: 110px !important;}');}

?>
<?php




$page = Yii::$app->request->get('page');
if ($page == ';') {
    $page = 1;
}
$searched=Yii::$app->request->queryParams['Company2ProjectSearch']['search_str'];
?>
<script src="https://cdn.jsdelivr.net/npm/@floating-ui/core@1.2.2"></script>
<script src="https://cdn.jsdelivr.net/npm/@floating-ui/dom@1.2.3"></script>
<?= $this->render('InnerItems/project_icons',['message' => $message,'statistics_status' =>$statistics_status]);?>
<?php
    $hidden_stats;
    if($message== "All Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value=">'.$message.': '.$statistics_status["total"].'">';}
    if($message== "New Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value="'.$message.': '.$statistics_status["total_new"].'">';}
    if($message== "Pending Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value="'.$message.': '.$statistics_status["total_pending"].'">';}
    if($message== "Approved Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value="'.$message.': '.$statistics_status["total_approved"].'">';}
    if($message== "in-progress Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value="'.$message.': '.$statistics_status["total_progress"].'">';}
    if($message== "Completed Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value="'.$message.': '.$statistics_status["total_completed"].'">';}
    if($message== "Lost Projects"){$hidden_stats= '<input id="hiddenStats" type="hidden" value="'.$message.': '.$statistics_status["total_lost"].'">';}
?>
    <a class="show_mine" href="<?php echo Yii::$app->getRequest()->getHostInfo() . Yii::$app->getRequest()->getUrl() . '&show_all=' . (isset(Yii::$app->request->queryParams['show_all']) ? Yii::$app->request->queryParams['show_all'] == true ? 0 : 1 : 1)  ?>">
                        <span class="company_show_all_selector"><i class="fa fa-eye" aria-hidden="true"></i> <?php  echo isset(Yii::$app->request->queryParams['show_all']) ? Yii::$app->request->queryParams['show_all'] == true ? 'Show Mine' : 'Show all' : 'Show all'  ?> </span>
            </a>            
<div class="aside_projects" id= 'company2-project-index'>
    <div class="search_container">
        <input class="search_bar" type="text" placeholder="Search" value="<?= !is_null($searched) ? $searched : ''?>">
    </div>

    <?= 
        ListView::widget([
            'dataProvider' => $dataProvider,
            'options' => ['class' => 'aside_projects'],
            'pager' => [
                'hideOnSinglePage' => false, 'maxButtonCounts' => 8,
                'linkOptions' => ['class' => 'page'],
                'activePageCssClass' => 'selected',
            ],
            'viewParams' => ['total' => $total,'message' => $message,'page' => $page,'condition_total' => $condition_total,'stadistic_status' => $statistics_status],
            'layout' => '       
        '.$hidden_stats.'      
        <div class="projects_selection_container">
               
            <div class="loader_project">
            <div class="lds-ring">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="loader_icon">
                <span>Loading Projects...</span>
            </div>
        </div>{items}</div><div class="pagination_container">{pager}</div>',
            'itemView' => 'InnerItems/_list_projects_view',
            'pager' => [
                'prevPageLabel' => '<',
                'nextPageLabel' => '>',
            ]
        ]);
    ?>
    <div class="d-flex justify-content-center mt-3">
    <button onclick="showMoreProjects(this)" class="expand_projects_selection_container">
       <span> Show all projets </span>
    </button>
    </div>
    
</div>


<?php


$this->registerJs('
var arrayParams = ' . json_encode(Yii::$app->request->queryParams) . ';


function change_claim_description(id,notes,status_obj)
{
    title_status = "' . Yii::t('app', 'Are you sure update this Note?') . '";

    var message = "";
    
    swal({
        title              : title_status,
        text               : message,
        type               : "warning",
        showCancelButton   : true,
        confirmButtonClass : "btn-warning",
        confirmButtonText  : "Yes",
        cancelButtonText   : "No",
        closeOnConfirm     : true
    },
    function(isConfirm)
    {
        if(isConfirm)
        {

                $.ajax({
                    type: "GET",
                    url: "' . Url::to(["/company2-project/change-project-notes"]) . '",
                    data: {"id":id, "notes":notes},
                    success: function(response)
                    {
                        $.pjax.reload("#company2-project-index", {timeout : false}).done(function(){
                            $.pjax.reload("#main-alert-widget", {timeout : false});
                        });
                    }
                });

        }
        else
        {
             setTimeout(function(){
                        window.location.reload(1);
                        }, 1000);    
        }
    });
}



function change_status_project(id,status)
{
    var confirm_message = (status == 1) ? "' . Yii::t('app-sweet_alert', 'You want to Activate this Project?') . '" : "' . Yii::t('app-sweet_alert', 'You want to Deactivate this Project?') . '";

    confirm_change_status("#company2-project-index", {
        "id"                          : id,
        "status"                      : status,
        "url"                         : "' . Url::to(["/company2-project/change-status"]) . '",
        "confirm_title"               : "' . Yii::t('app-sweet_alert', 'Are you sure?') . '",
        "activated_title"             : "' . Yii::t('app-sweet_alert', 'Project is Activated') . '",
        "deactivated_title"           : "' . Yii::t('app-sweet_alert', 'Project is Deactivated') . '",
        "confirm_message"             : confirm_message,
        "activated_success_message"   : "' . Yii::t('app-sweet_alert', 'Project Activated Successfully.') . '",
        "deactivated_success_message" : "' . Yii::t('app-sweet_alert', 'Project Deactivated Successfully.') . '",
    });
}

var projectJsStatusArray = ' . json_encode($project_status) . ';


var formatter = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",

});


function change_project_status2(id,rcv,  status,old_status)
{
    rcv_claim = formatter.format(rcv); 

    if(projectJsStatusArray[status] == "Approved")
    {
        var message = "' . Yii::t('app', 'If you want to approve it, click yes, if not, go to update and update the amount of RCV.') . '";
        message += " `" + projectJsStatusArray[status] + "`";
        title_status = "Are you sure you want to approve the collection of this project based on this amount?  ";
        title_status += "" + rcv_claim + "";

    }
    else
    {
        var message = "' . Yii::t('app', 'to change the status of Project to') . '";
        message += " `" + projectJsStatusArray[status] + "`";
        title_status = "' . Yii::t('app', 'Are you sure?') . '";


    }

    swal({
        title              : title_status,
        text               : message,
        type               : "warning",
        showCancelButton   : true,
        confirmButtonClass : "btn-warning",
        confirmButtonText  : "Yes",
        cancelButtonText   : "No",
        closeOnConfirm     : true
    },
    function(isConfirm)
    {
        if(isConfirm)
        {

                $.ajax({
                    type: "GET",
                    url: "' . Url::to(["/company2-project/change-project-status"]) . '",
                    data: {"id":id, "status":status},
                    success: function(response)
                    {
                      redirect_after_update(arrayParams,parseInt(status));
                    }
                });

        }
    });
}

function change_project_status3(id,rcv,  status,old_status, status_obj)
{

    rcv_claim = formatter.format(rcv); 
    var selectedSection = "' . Yii::$app->request->queryParams['message'] . '";

    if(status == 5)
    {
        var message = "' . Yii::t('app', 'If you want to approve it, click yes, if not, go to update and update the amount of RCV.') . '";
        message += " Approved";

        title_status = "Are you sure you want to approve the collection of this project based on this amount?  ";
        title_status += "" + rcv_claim + "";

    }
    else
    {
        var message = "' . Yii::t('app', 'to change the status of Project to') . '";
        message += "Approved";
        title_status = "' . Yii::t('app', 'Are you sure?') . '";
    }
    swal({
        title              : title_status,
        text               : message,
        type               : "warning",
        showCancelButton   : true,
        confirmButtonClass : "btn-warning",
        confirmButtonText  : "Yes",
        cancelButtonText   : "No",
        closeOnConfirm     : true
    },
    function(isConfirm)
    {
        if(isConfirm)
        {

                $.ajax({
                    type: "POST",
                    url: "' . Url::to(["/company2-project/change-project-statusa"]) . '",
                    data: {"id":id, "status":status},
                    success: function(response)
                    {

                      redirect_after_update(arrayParams);

                    }
                });

        }
        else
        {
            $(status_obj).val(1);
        }
    });
}


    var links_message_arr = [
    {message:"Pending Projects",id_status : 2},
    {message:"Approved Projects",id_status : 5},
    {message:"in-progress Projects",id_status : 6},
    {message:"Completed Projects",id_status : 7},
    {message:"Lost Projects",id_status : 8}
]
function redirect_after_update(actual_link,selected_id){
    let base_url = "' . yii\helpers\Url::base() . explode(yii\helpers\Url::base(), yii\helpers\Url::canonical())[1] . '";

    if(actual_link.Company2ProjectSearch.id_status != 5 && actual_link.Company2ProjectSearch.id_status != 6 && actual_link.Company2ProjectSearch.id_status != 7 && actual_link.Company2ProjectSearch.id_status != 8  ){
    let actual_redirect_link = base_url + `&Company2ProjectSearch%5Bis_active%5D=1&Company2ProjectSearch%5Bid_status%5D=${links_message_arr[links_message_arr.findIndex(e => e.message === actual_link.message) +1].id_status}&message=${links_message_arr[links_message_arr.findIndex(e => e.message === actual_link.message) +1].message}`;
    
    window.location.href = actual_redirect_link;
    }else if(actual_link.Company2ProjectSearch.id_status == 5 || actual_link.Company2ProjectSearch.id_status == 6){
    let actual_redirect_link = base_url + `&Company2ProjectSearch%5Bis_active%5D=1&Company2ProjectSearch%5Bid_status%5D=${links_message_arr[links_message_arr.findIndex(e => e.id_status === selected_id)].id_status}&message=${links_message_arr[links_message_arr.findIndex(e => e.id_status === selected_id)].message}`;
        window.location.href = actual_redirect_link;
        console.log(selected_id);
    }    //&Company2ProjectSearch%5Bid_status%5D=7
}




function change_project_status(id, status,old_status,status_obj)
{
    var message = "' . Yii::t('app', 'to change the status of Project to') . '";
        message += " `" + projectJsStatusArray[status] + "`";

    swal({
        title              : "' . Yii::t('app', 'Are you sure?') . '",
        text               : message,
        type               : "warning",
        showCancelButton   : true,
        confirmButtonClass : "btn-warning",
        confirmButtonText  : "Yes",
        cancelButtonText   : "No",
        closeOnConfirm     : true
    },
    function(isConfirm)
    {
        if(isConfirm)
        {

                $.ajax({
                    type: "GET",
                    url: "' . Url::to(["/company2-project/change-project-status"]) . '",
                    data: {"id":id, "status":status},
                    success: function(response)
                    {
                        $.pjax.reload("#company2-project-index", {timeout : false}).done(function(){
                            $.pjax.reload("#main-alert-widget", {timeout : false});
                        });
                    }
                });

        }
        else
        {
            $(status_obj).val(old_status);
        }
    });
}

function changeIdStatus(id,idObj)
{
    var status = $(idObj).val();
    $(idObj).parent().find(".fa-spinner").remove();
    $(idObj).parent().append(\'<i class="fa fa-spinner fa-spin" style="font-size: 20px;"></i>\');
    $(idObj).hide();

    //var message = "' . Yii::t('app', 'Are you sure?') . '";

    //if(confirm(message))
    //{
        $.ajax({
            type: "GET",
            url: "' . Url::to(["company2-project/change-id-status"]) . '",
            data: {"id":id, "status":status},
            success: function(response)
            {
                $(idObj).val(($(idObj).val() == 1) ? 0 : 1);
                $(idObj).parent().find(".fa-spinner").remove();
                $(idObj).show();

                //$.pjax.reload("#company2-project-index", {timeout : false});
                $.pjax.reload("#main-alert-widget", {timeout : false})
            }
        });
    //}
}', View::POS_END);

$this->registerJs('
$(document).ajaxComplete(function() {
    $("div.popover.fade.show").remove();
    $(\'[data-toggle="popover"]\').popover();
});


$("#modal-box").on("hide.bs.modal", function (e) {
    $.pjax.reload("#company2-project-index", {timeout : false});
})

$(document).click(function(event) {
  //if you click on anything except the modal itself or the "open modal" link, close the modal
  if (!$(event.target).closest(".popover,.btn-popover-order").length) {
    //$(".popover").popover("hide");
  }
});
$(document).on("click",".close-popover",function(){
  $(".popover").popover("hide");
});

$(document).ready(
function(){

  $(".kv-expand-header-icon span").removeClass("glyphicon glyphicon-expand")

  $("thead tr th").off("click")
 
});
', View::POS_READY);
