<?php


use common\models\Client;


use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;
use yii\widgets\Spaceless;
use kartik\grid\GridView;
use common\models\Module;
use common\models\ProjectTask;
use common\models\Task;

use common\models\Company2ProjectTask;
use common\models\ClaimTask;
use common\models\ProjectStatus;
use common\models\ProjectsAttachments;
use backend\components\UserHelpers;
use kartik\editable\Editable;
use kartik\popover\PopoverX;
use yii\helpers\HtmlPurifier;


?>
<div class="item" style="cursor:pointer" onclick="showActivityDetails(<?= $model->id ?>,this)">
    <div class="card-box m-b-12">
        <div class="row">
            <div class="col-md-4" style="text-align:center;">
                <?= Html::hiddenInput('LeadId', $model->id, ['class' => 'LeadId']) ?>
                <?= Html::a(
                                            Html::img(
                                               UserHelpers::getProjectClassImage(32, 32, $model->project_class_id, true),
                                              ['class' => 'rounded-circle logo-marketing user_avatar',
                                                'style' => 'width:50px;height: 50px; 
                                                border: 1px solid #C00;
                                                border-color:#ebeff2;box-shadow:0 0 0 0
                                                !important;-moz-box-shadow: 0 0 0
                                                0;display:block; margin-left: auto;
                                                margin-right: auto;', 
                                                'alt' => 'user-img','title' => Yii::t('app',"Project Class"), 'data-toggle' => 'tooltip'])
                                        )."<br>".Html::a($model->quantity." ".$model->projectClass->unit_measure, ['/company2-project/update',
                                         'id' => $model->id]) . ' '; ;
                ?>

            </div>
            <div class="col-md-4">
                <div class="member-info">
                    <?= (Yii::$app->user->can('Staff1')) ? $model->client->name . ' ' . $model->client->last_name : Html::a($model->client->name . ' ' . $model->client->last_name, [$action . '/client/update', 'id' => $model->client->id], ['data-pjax' => '0']).
                                  "<br><font size=2>PH: </font>".$model->client->phone.
                                   "<br>".$model->client->email."<br>".$model->map2; ?>
                </div>    
            </div>
         
            <div class="col-md-1" style="margin-bottom: 10px
            ;">


            <?php 
                            if($model->is_active == 1){

                                switch ($model->projectStatus->name) {
                                    case "Pending":
                                        $classProjectStatus = "btn-pending-project";
                                        $classProjectStatus = "btn-pending-project";
                                        break;
                                    case "Denied":
                                        $classProjectStatus = "btn-denied-project";
                                        break;
                                    case "Contested":
                                        $classProjectStatus = "btn-contested-project";
                                        break;
                                    case "Approved":
                                        $classProjectStatus = "btn-approved-project";
                                        break;
                                    case "In-progress":
                                        $classProjectStatus = "btn-progress-project";
                                        break;
                                    case "Completed":
                                        $classProjectStatus = "btn-completed-project";
                                        break;
                                        case "Lost":
                                        $classProjectStatus = "btn-danger";
                                        break;
                                    default:
                                        $classProjectStatus = "btn-new-project";
                                }


                                $claimStatusHtml = Html::dropDownList('id_status', $model->id_status, $project_status, ['prompt' => 'Select','class' => 'form-control', 'onChange' => 'change_project_status2(' . $model->id . ',' . $model->rcv . ',

                                 this.value, 
                                 ' . $model->id_status . ', this)']);
                                

                            
                            if($model->id_status == 5 || $model->id_status == 6)
                            { 

                                    echo Html::a('<i class="fas fa-pencil-alt"></i> '.$model->projectStatus->name, 'javascript:void(0)', [
                                    'class'          => 'btn btn-sm-project '.$classProjectStatus.' btn-popover-order btn-rounded-project',
                                    'title'          => 'Change Project Status &nbsp;&nbsp; <span class="text-info"><strong></strong></span> <button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project">X</button>',
                                    'data-toggle'    => 'tooltip',
                                    'tabindex'       => '0',
                                    'data-container' => 'body',
                                    'data-toggle'    => 'popover',
                                    'data-trigger'   => 'click focus',
                                    'data-placement' => 'button',
                                    'data-html'      => 'true',
                                    'data-close'      => 'true',
                                    'data-content'   => $claimStatusHtml,
                                ]);

                            } 

                             if ($model->id_status == 1) {
                                    echo "<p class = 'btn btn-sm-project btn-new-project btn-popover-order btn-rounded-project'>New</div>";
                                }


                                if ($model->id_status == 2) {
                                    echo "<p class = 'btn btn-sm-project btn-pending-project btn-popover-order btn-rounded-project'>Pending</div>";
                                }

                                if ($model->id_status == 7) {
                                    echo "<p class = 'btn btn-sm-project btn-completed-project btn-popover-order btn-rounded-project'>Completed</div>";
                                }

                                if ($model->id_status == 8) {
                                    echo "<p class = 'btn btn-sm-project btn-lost-project btn-popover-order btn-danger'>Lost</div>";
                                }
                            }else{

                                    echo "<b>Inactive<br>Project</b>";
                                }

            ?>

                    <?php
                /*switch ($model->projectStatus->name) {
                    case "New":
                        $classLeadStatus = "new-lead";
                        break;
                    case "In-progress":
                        $classLeadStatus = "progress-lead";
                        break;
                    case "Hot":
                        $classLeadStatus = "hot-lead";
                        break;
                    case "Cold":
                        $classLeadStatus = "cold-lead";
                        break;
                    case "Lost":
                        $classLeadStatus = "lost-lead";
                        break;
                    case "Closed":
                        $classLeadStatus = "closed-lead";
                        break;
                    default:
                        $classLeadStatus = "info";
                }*/

                /* switch ($model->projectStatus->name) {
                                    case "Pending":
                                        $classProjectStatus = "btn-pending-project";
                                        $classProjectStatus = "btn-pending-project";
                                        break;
                                    case "Denied":
                                        $classProjectStatus = "btn-denied-project";
                                        break;
                                    case "Contested":
                                        $classProjectStatus = "btn-contested-project";
                                        break;
                                    case "Approved":
                                        $classProjectStatus = "btn-approved-project";
                                        break;
                                    case "In-progress":
                                        $classProjectStatus = "btn-progress-project";
                                        break;
                                    case "Completed":
                                        $classProjectStatus = "btn-completed-project";
                                        break;
                                        case "Lost":
                                        $classProjectStatus = "btn-danger";
                                        break;
                                    default:
                                        $classProjectStatus = "btn-new-project";
                                }*/
                ?>
            
            </div>
          
        


        </div>   
</div>

<script type="text/javascript">
function openWin(a, d)
{
              //a is project_id and b = task_id c = type d = baseurl
              var window_width = 750;
              var window_height = 830;
              var newfeatures= 'scrollbars=yes,resizable=yes';
              var window_top = (screen.height-window_height)/2;
              var window_left = (screen.width-window_width)/2;

             
                  newWindow=window.open(""+d+"/uploads/leads-documents/"+a+"",'_blank', 'titulo','width=' + window_width + ',height=' + window_height + ',top=' + window_top + ',left=' + window_left + ',features=' + newfeatures + '').focus;
             
}
</script> 