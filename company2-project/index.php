<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;
use yii\widgets\Spaceless;
use kartik\grid\GridView;
use common\models\Module;
use common\models\ProjectTask;
use common\models\Task;

use common\models\Company2ProjectTask;
use common\models\ClaimTask;
use common\models\ProjectStatus;
use common\models\ProjectsAttachments;
use backend\components\UserHelpers;
use kartik\editable\Editable;
use kartik\popover\PopoverX;
use yii\helpers\HtmlPurifier;
use backend\assets\Company2ProjectBundle;

Company2ProjectBundle::register($this);

if (Yii::$app->user->can('AS Staff'))
    $this->title = Yii::t('app', 'Contractor Projects');
else {
    $status = 'Manage';
    $status = ((Yii::$app->request->get('company2-project') == 'acp') ? 'Active' : $status);
    $status = ((Yii::$app->request->get('company2-project') == 'icp') ? 'Inactive' : $status);
    $status = ((Yii::$app->request->get('company2-project') == 'ncp') ? 'New' : $status);
    $status = ((Yii::$app->request->get('company2-project') == 'ocp') ? 'Outside' : $status);
    

    $this->title = Yii::t('app', '{status} Projects', ['status' => $status]);
}
$this->params['breadcrumbs'][] = $this->title;

$message = Yii::$app->request->get('message');

?>


<style type="text/css">
    body {
    height: auto;
    padding:10px;
}
.well{
    max-width: 400px;
    margin: 0 auto;
    position: fixed;
    top: 10px;
}
.popover{
    max-width: 100%; 
}

p  {
 width: 400px;
 margin: 10px auto;
 padding: 5px;
 border: solid 0px #f60;
 font: normal 13px arial, helvetica, sans-serif;
 line-height: 25px;
 }

/* For desktop: */
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

@media only screen and (max-width: 768px) {
  /* For mobile phones: */
  [class*="col-"] {
    width: 100%;
  }

  .pop {
    margin-left: -15px;
    margin-right: -55px;
}
} 

ul {
    padding-inline-start: 10px !important;
}




</style>



<!-- new header -->
    <!-- dashboard -->
        <!-- material and afiliate -->
        <div class="row">
             <div class="col-md-6 col-lg-6 col-xl-3">
                <a href="<?= Url::to(['/company2-project/index', 'Company2ProjectSearch[is_active]' => '', 'company1claim' => 'c1ct', 'message' => 'All Projects']) ?>">
                    <div id="store-revenue" class="widget-panel widget-style-2 bg-white mm ">
                        <i class="icon-projects text-custom"></i>
                        <h2 class="m-0 text-dark font-600">
                            <?= Yii::$app->formatter->asDecimal($statistics_total['total']) ?>    
                        </h2>
                        <div class="text-muted m-t-5"><?= Yii::t('app', ' Projects') ?></div>
                    </div>
                </a>
            </div>
             <div class="col-md-6 col-lg-6 col-xl-3">
                <a href="<?= Url::to(['/material-order/index', 'Company2ProjectSearch[is_active]' => '', 'company2-project' => 'mlp', 'message' => 'All Orders']) ?>">
                    <div id="store-revenue" class="widget-panel widget-style-2 bg-white mm ">
                        <i class="icon-inspections text-stock"></i>
                        <h2 class="m-0 text-dark font-600">
                            <?= Yii::$app->formatter->asDecimal($total_order) ?>    
                        </h2>
                        <div class="text-muted m-t-5"><?= Yii::t('app', 'Orders') ?></div>
                    </div>
                </a>
            </div>
             <div class="col-md-6 col-lg-6 col-xl-3">
                <a href="<?= Url::to(['/project-affiliate/index', 'ProjectAffiliateSearch[affiliate_option]' => '0', 'project-affiliate' => 'mlp', 'message' => 'All Affiliate Projects']) ?>">
                    <div id="store-revenue" class="widget-panel widget-style-2 bg-white mm ">
                        <i class="icon-contractor text-pink"></i>
                        <h2 class="m-0 text-dark font-600">
                            <?= Yii::$app->formatter->asDecimal($total_afilliate) ?>    
                        </h2>
                        <div class="text-muted m-t-5"><?= Yii::t('app', ' Affiliates') ?></div>
                    </div>
                </a>
            </div>
            <!-- Status Project -->
            <div class="col-md-6 col-lg-6 col-xl-3">
                <a href="<?= Url::to(['/company2-project/index', 'Company2ProjectSearch[id_status]' => '1', 'company2project' => 'c1ct', 'message' => 'New Projects']) ?>">
                    <div id="store-revenue" class="widget-panel widget-style-2 bg-white mm ">
                        <i class="icon-new text-aquamarine"></i>
                        <h2 class="m-0 text-dark font-600">
                            <?= Yii::$app->formatter->asDecimal($statistics_status['total_new']) ?>    
                        </h2>
                        <div class="text-muted m-t-5"><?= Yii::t('app', ' New Projects') ?></div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
                <a href="<?= Url::to(['/company2-project/index', 'Company2ProjectSearch[id_status]' => '2', 'company2project' => 'c1ca',  'message' => 'Pending Projects']) ?>">
                    <div id="store-stock" class="widget-panel widget-style-2 bg-white mm">
                        <i class="fas fa-question text-orange "></i>
                        <h2 class="m-0 text-dark font-600">
                          <?= Yii::$app->formatter->asDecimal($statistics_status['total_pending']) ?>
                        </h2>
                        <div class="text-muted m-t-5"><?= Yii::t('app', 'Pending Projects') ?></div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
            <a href="<?= Url::to(['/company2-project/index', 'Company2ProjectSearch[id_status]' => '5', 'company2project' => 'c1ci',  'message' => 'Approved Projects']) ?>">
                    <div id="store-orders" class="widget-panel widget-style-2 bg-white mm">
                        <i class="icon-handshake-2 text-blue"></i>
                        <h2 class="m-0 text-dark font-600">
                           <?= Yii::$app->formatter->asDecimal($statistics_status['total_approved']) ?>
                        </h2>
                        <div class="text-muted m-t-5"><?= Yii::t('app', 'Approved Projects') ?></div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
                <a href="<?= Url::to(['/company2-project/index', 'Company2ProjectSearch[id_status]' => '6', 'company2project' => 'c1ct', 'message' => 'in-progress Projects']) ?>">
                    <div id="store-revenue" class="widget-panel widget-style-2 bg-white mm ">
                        <i class="icon-sand-clock  text-pending"></i>
                        <h2 class="m-0 text-dark font-600">
                            <?= Yii::$app->formatter->asDecimal($statistics_status['total_progress']) ?>    
                        </h2>
                        <div class="text-muted m-t-5"><?= Yii::t('app', ' In Progress Projects') ?></div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
                <a href="<?= Url::to(['/company2-project/index', 'Company2ProjectSearch[id_status]' => '7', 'company2project' => 'c1ca',  'message' => 'Completed Projects']) ?>">
                    <div id="store-stock" class="widget-panel widget-style-2 bg-white mm">
                        <i class="glyphicon glyphicon-check text-completed "></i>
                        <h2 class="m-0 text-dark font-600">
                          <?= Yii::$app->formatter->asDecimal($statistics_status['total_completed']) ?>
                        </h2>
                        <div class="text-muted m-t-5"><?= Yii::t('app', 'Completed Projects') ?></div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
            <a href="<?= Url::to(['/company2-project/index', 'Company2ProjectSearch[id_status]' => '8', 'company2project' => 'c1ci',  'message' => 'Lost Projects']) ?>">
                    <div id="store-orders" class="widget-panel widget-style-2 bg-white mm">
                        <i class="fa fa-window-close-o text-unassigned"></i>
                        <h2 class="m-0 text-dark font-600">
                           <?= Yii::$app->formatter->asDecimal($statistics_status['total_lost']) ?>
                        </h2>
                        <div class="text-muted m-t-5"><?= Yii::t('app', 'Lost Projects') ?></div>
                    </div>
                </a>
            </div>
            <!-- End Status Project-->
            <!-- end afiliate -->
            <!--<div class="col-md-6 col-lg-6 col-xl-3">
                <a href="<?= Url::to(['/company2-project/index', 'Company2ProjectSearch[is_active]' => '', 'company1claim' => 'c1ct', 'message' => 'All Projects']) ?>">
                    <div id="store-revenue" class="widget-panel widget-style-2 bg-white mm ">
                        <i class="icon-projects text-custom"></i>
                        <h2 class="m-0 text-dark font-600">
                            <?php Yii::$app->formatter->asDecimal($statistics['total']) ?>    
                        </h2>
                        <div class="text-muted m-t-5"><?= Yii::t('app', ' Projects') ?></div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
                <a href="<?= Url::to(['/company2-project/index', 'Company2ProjectSearch[is_active]' => '1', 'company1claim' => 'c1ca',  'message' => 'Active Projects']) ?>">
                    <div id="store-stock" class="widget-panel widget-style-2 bg-white mm">
                        <i class="icon-projects text-completed "></i>
                        <h2 class="m-0 text-dark font-600">
                          <?php Yii::$app->formatter->asDecimal($statistics['total_active']) ?>
                        </h2>
                        <div class="text-muted m-t-5"><?= Yii::t('app', 'Active Projects') ?></div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
            <a href="<?php Url::to(['/company2-project/index', 'Company2ProjectSearch[is_active]' => '0', 'company1claim' => 'c1ci',  'message' => 'Inactive Projects']) ?>">
                    <div id="store-orders" class="widget-panel widget-style-2 bg-white mm">
                        <i class="icon-projects text-unassigned"></i>
                        <h2 class="m-0 text-dark font-600">
                           <?php Yii::$app->formatter->asDecimal($statistics['total_inactive']) ?>
                        </h2>
                        <div class="text-muted m-t-5"><?php Yii::t('app', 'Inactive Projects') ?></div>
                    </div>
                </a>
            </div> -->
            <!-- <div class="col-md-6 col-lg-6 col-xl-3">
             <a href=" /*Url::to(['tickets-support/index','status_t' => 'tkc', 'TicketsSupportSearch[status_ticket]' => 2, 'tickets-support'=>'tkc'])*/ ?>">
                <div id="store-products" class="widget-panel widget-style-2 bg-white mm">
                    <i class="md    glyphicon glyphicon-check text-completed"></i>
                    <h2 class="m-0 text-dark font-600">
                         /*Yii::$app->formatter->asDecimal($statistics['total'])*/ ?> 
                    </h2>
                    <div class="text-muted m-t-5"> /*Yii::t('app', 'Closed') * </div>
                </div>
            </a>
            </div> -->
    </div>

    <!-- end -->
      
       <br>
<!-- end new header -->
          <div class="company2-project-index">
                 <div class="card card-primary">
          <?php if (Yii::$app->user->can('Company2 Staff') && Yii::$app->request->get('company2-project') != 'ocp') : ?>
                    <?php /*?><?= Html::a('<i class="fa fa-plus">&nbsp;</i>Add', ['create'], ['class' => 'btn-sm btn btn-primary', 'data-pjax' => '0']) ?><?php /**/ ?>
                    <?= Html::a('<i class="fa fa-plus">&nbsp;</i><b>' . Yii::t('app', 'Add Projects'), ['/client/createcompany2project'], ['class' => 'btn-sm btn btn-info', 'title' => Yii::t('app', 'Add Projects'), 'data-toggle' => 'tooltip','data-pjax' => '0', ]).'</b>' ?>
                <?php endif; ?>
            <div class="card-header text-uppercase text-card-layout">
                <div class="row">
                    <div class="col-md-6">
                       <b><?= $message ?></b>
                  </div>
                </div>
       </div>
       <div class="card-body" style="padding-top:10px;">
        <div class="col-sm-12">



           <?php Pjax::begin(['id' => 'company2-project-index']); ?>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
        
          
                    <?php echo $this->render('_search3', ['model' => $searchModel, 'parent_users' => $parent_users, 'seasons' => $seasons, 'regions' => $regions, 'cities2' => $cities2, 'zipcodes' => $zipcodes, 'staff1es' => $staff1es, 'clients' => $clients, 'project_class' => $project_class, 'project_status' => $project_status, 'id'=>$id, 'message' => $message, 'total'=> $total, 'dataProvider' => $dataProvider]); ?>
                <?= GridView::widget([
                    'id'           => 'project-grid',
                    'dataProvider' => $dataProvider,
                    'resizableColumns'=>false,
                    'options' => [ 'style' => 'table-layout:fixed;' ],
                    'responsive' => true,
                    'responsiveWrap' => false,
                    'persistResize' => false, 

                    'summary' => '',
                    'rowOptions' => function ($model) {
                        return ($model->is_active == "0") ? ['class' => 'danger'] : '';

                    },

                    'columns' => [
                        [
                            'class' => 'kartik\grid\ExpandRowColumn',
                            'visible'        => (Yii::$app->user->can('Company2 Admin')),
                            'value' => function ($model, $key, $index, $column)
                             {
                                return GridView::ROW_COLLAPSED;
                            },
                            'detail' => function ($model, $key, $index, $column) {
                
                                $query = Module::find()->joinWith('department')->where(['module.is_active'=>1,'type'=>2]);
                                $dataProvider = new ActiveDataProvider([
                                            'query' => $query,
                                            'pagination' => ['pageSize' => Yii::$app->session['common_perpage']],
                                            'sort' => [
                                                'defaultOrder' => ['module_order' => SORT_ASC]
                                            ]
                                        ]);
                                return $this->render('/module/module', [
                                 'dataProvider' => $dataProvider,
                                 'project_id'=>$key,
                                 'type'=>2,
                                ]);
                                //return Yii::$app->controller->renderPartial('department/index');
                            },
                            'headerOptions' => ['class' => 'kartik-sheet-style'] ,
                            'expandOneOnly' => true
                        ],
                        [
                            'attribute'      => 'company2_id',
                            'label'          => Yii::t('app', 'Contractor'),
                            'contentOptions' => ['class' => ''],
                            'visible'        => (Yii::$app->user->can('AS Staff')),
                            'value'          => 'client.company2.short_name'
                        ],

                        [
                                    'attribute'      => 'a_id',
                                    'label'          => Yii::t('app', 'Project Class'),
                                    'contentOptions' => ['class' => 'text-center'],
                                    'headerOptions'  => ['class' => 'text-center'],
                                    'width' => '20%',
                                    'content'        => function ($model) {
                                        return Html::a(
                                            Html::img(
                                               UserHelpers::getProjectClassImage(32, 32, $model->project_class_id, true),
                                              ['class' => 'rounded-circle logo-marketing user_avatar',
                                                'style' => 'width:50px;height: 50px; 
                                                border: 1px solid #C00;
                                                border-color:#ebeff2;box-shadow:0 0 0 0
                                                !important;-moz-box-shadow: 0 0 0
                                                0;display:block; margin-left: auto;
                                                margin-right: auto;', 
                                                'alt' => 'user-img','title' => Yii::t('app',"Project Class"), 'data-toggle' => 'tooltip'])
                                        )."<br>".Html::a($model->quantity." ".$model->projectClass->unit_measure, ['/company2-project/update',
                                         'id' => $model->id]) . ' '; ;
                                    },
                                ],

                                               /*[
                            'attribute'      => 'a_id',
                            'label'          => Yii::t('app', 'Project Class'),
                            'headerOptions'  => ['class' => 'text-center'],
                            'width' => '20%',
                            'contentOptions' => ['class' => '',],
                            'content'        => function ($model) {



                                  $title = '<div class="row" style="border:0px">
                                        <div class="col-10">
                                            Project Class
                                        </div>
                                        <div class= "col-1 style="text-align:right">
                                            <span class="text-info"><strong><button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project" >X</button></strong></span> 
                                        </div>
                                    </div>';




                                return "<div align='center'>" . Html::a(
                                    Html::img(
                                         UserHelpers::getProjectClassImage(32, 32, $model->project_class_id, true),
                                        [
                                            'class' => 'rounded-circle logo-marketing user_avatar', 
                                            'style' => 'width:50px;height: 50px; border: 1px solid #C00; border-color:#ebeff2;box-shadow:0 0 0 0 !important;-moz-box-shadow: 0 0 0 0;display:block; margin-left: auto; margin-right: auto;', 'alt' => 'user-img', 
                                            'title'          => $title,
                                            'data-toggle' => 'tooltip',
                                            'data-toggle'    => 'tooltip',
                                            'tabindex'       => '0',
                                            'data-container' => 'body',
                                            'data-toggle'    => 'popover',
                                            'data-trigger'   => 'click focus',
                                            'data-placement' => 'auto',
                                            'data-html'      => 'true',
                                            'data-close'      => 'true',
                                            //'data-content'   => $data_insurance,
                                        ]
                                    )
                                )."<br>".Html::a($model->quantity." ".$model->projectClass->unit_measure, ['/company2-project/update',
                                         'id' => $model->id]) . ' '; 
                            },
                        ],*/

                               /* [
                                    'attribute'      => 'Quantity',
                                     'label'          => Yii::t('app', 'Quantity'),
                                    'contentOptions' => ['class' => ''],
                                    'content'        => function ($model) {
                                        return Html::a($model->quantity, ['/company2-project/update',
                                         'id' => $model->id]) . ' ';
                                    }
                                ],
                                [   
                                    'label' => Yii::t('app', 'UOM'),
                                    'attribute'      => 'projectClass.phone',
                                    'contentOptions' => ['class' => ''],
                                    'content' => function ($model) {
                                        $action = ($model);
                                        return $model->projectClass->unit_measure;
                                    },                    
                                ],*/

                              /*[
                                    'attribute'      => 'quantity',
                                    'contentOptions' => ['class' => ''],
                                    'content' => function ($model) {
                                        $action = ($model);
                                         return Html::a($model->quantity." ".$model->projectClass->unit_measure, ['/company2-project/update',
                                         'id' => $model->id]) . ' ';
                                    }


                            ],*/

                      
                        /*[
                            'attribute'      => 'claim_description',
                            'label'          => Yii::t('app', 'Notes'),
                            'contentOptions' => ['class' => 'text-center'],
                            'width' => '5%',
                            'content'        => function ($model) use ($claim_status)

                            {   
                                   $title = '<div class="row" style="border:0px">
                                        <div class="col-10">
                                            Notes
                                        </div>
                                        <div class= "col-1 style="text-align:right">
                                            <span class="text-info"><strong><button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project" >X</button></strong></span> 
                                        </div>
                                    </div>';


                                    $notes = Html::tag('textArea', Html::encode($model->claim_description), ['class' => 'claim_description',  'onChange' => 'change_claim_description(' . $model->id . ', this.value, this)', 'style'=>'width:250px; height:250px;',  ]);


                                    return Html::a('<i class="fas fa-comment-alt"></i> '."Notes", 'javascript:void(0)', [
                                    'class'          => 'btn btn-sm-project badge-open btn-popover-order btn-rounded-project',
                                    'title'          => $title,
                                    'data-toggle'    => 'tooltip',
                                    'tabindex'       => '0',
                                    'data-container' => 'body',
                                    'data-toggle'    => 'popover',
                                    'data-trigger'   => 'click focus',
                                    'data-placement' => 'button',
                                    'data-html'      => 'true',
                                    'data-close'      => 'true',
                                    'data-content'   => $notes,
                                ]);
                            }  

                        ],*/

                        [
                            'attribute'      => 'email',
                            'label'          => Yii::t('app', 'Client Contact'),
                            'contentOptions' => ['class' => ''],
                            'contentOptions' => ['class' => 'text-left'],
                            'headerOptions'  => ['class' => 'text-left'],
                            'width' => '20%',
                            'content' => function ($model) {

                                  return (Yii::$app->user->can('Staff1')) ? $model->client->name . ' ' . $model->client->last_name : Html::a($model->client->name . ' ' . $model->client->last_name, [$action . '/client/update', 'id' => $model->client->id], ['data-pjax' => '0']).
                                  "<br><font size=2>PH: </font>".$model->client->phone.
                                   "<br>".$model->client->email."<br>".$model->map2;


                            },
                        ],

                        [
                            'attribute'      => 'inspection_date',
                            'label'          => Yii::t('app', 'Project Info'),
                            'contentOptions' => ['class' => ''],
                            'contentOptions' => ['class' => 'text-left'],
                            'headerOptions'  => ['class' => 'text-left'],
                            'width' => '30%',
                            'content'        => function ($model) {


                                   $title = '<div class="row" style="border:0px">
                                        <div class="col-10">
                                            Notes
                                        </div>
                                        <div class= "col-1 style="text-align:right">
                                            <span class="text-info"><strong><button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project" >X</button></strong></span> 
                                        </div>
                                    </div>';


                                    $notes = Html::tag('textArea', Html::encode($model->claim_description), ['class' => 'claim_description box_popover',  'onChange' => 'change_claim_description(' . $model->id . ', this.value, this)', ]);


                                    $notes_final =  Html::a('<i class="fas fa-comment-alt"></i> '."Notes", 'javascript:void(0)', [
                                    'class'          => 'btn btn-sm-project badge-open btn-popover-order btn-rounded-project',
                                    'title'          => $title,
                                    'data-toggle'    => 'tooltip',
                                    'tabindex'       => '0',
                                    'data-container' => 'body',
                                    'data-toggle'    => 'popover',
                                    'data-trigger'   => 'click focus',
                                    'data-placement' => 'button',
                                    'data-html'      => 'true',
                                    'data-close'      => 'true',
                                    'data-content'   => $notes,
                                ]);
                         


                             $due_date = "Due Date: ".Yii::$app->formatter->asDate($model->due_date,'php:m-d-Y');

                             if($model->due_date == ""){

                                return "Due Date Pending 
                                <br><b>Supervisor: </b><br>".Html::a($model->staff->first_name . ' ' . $model->staff->last_name, ['/client/index', 'ClientSearch[season_id]' => $model->season->id, 'ClientSearch[user_id]' => $model->client->user->id, 'ClientSearch[is_active]' => '1', 'at' => 'at'], ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Clients'), 'data-toggle' => 'tooltip']) . '
                                <br>'.$notes_final;
                             }else{

                                return $due_date."
                                    <br><b>Supervisor: </b>
                                    <br>".Html::a($model->staff->first_name . ' ' . $model->staff->last_name, ['/client/index', 'ClientSearch[season_id]' => $model->season->id, 'ClientSearch[user_id]' => $model->client->user->id, 'ClientSearch[is_active]' => '1', 'at' => 'at'], ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Clients'), 'data-toggle' => 'tooltip']) . 
                                    '<br>'.$notes_final;
                             }

                            }
                        ],
                           
                        /*[
                            'attribute'      => 'created_at',
                            'label'          => Yii::t('app', 'Registration Date'),
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions'  => ['class' => 'text-center'],
                            'width' => '5%',
                            'contentOptions' => ['class' => ''],
                            'format'         => ['date', 'php:m-d-Y'],

                        ],*/

                        
                        [
                            'label'          => Yii::t('app', 'Alert'),
                            'attribute'      => 'id',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions'  => ['class' => 'text-center'],
                            'width' => '20%',
                            'content'        => function ($model) {


                            $projectTaskDocuments = Task::find()->joinWith('module')
                            ->joinWith('module.department')
                            ->where(['type'=>2])
                            ->andWhere(['task.is_active' => 1])->all(); 

                         
                            if($projectTaskDocuments)
                            {
                                  $donwloadDocumentsHtml='<ul>';
                                foreach ($projectTaskDocuments as $key => $documents) 
                                {
                                        $projectTaskDocumentss = Company2ProjectTask::find()
                                        ->where(['project_id'=>$model->id])
                                        ->andWhere(['task_id'=> $documents->id])
                                        ->one();

                                        //var_dump($projectTaskDocumentss); die();

                                    if($projectTaskDocumentss->id)
                                    {
                                        $create_model = date('d-m-Y', $model->created_at);
                                        $create_task  = date('d-m-Y', $projectTaskDocumentss->assigned_date); 

                                        //var_dump($create_model." ".$create_task); die();
                                    
                                        if($projectTaskDocumentss->task_status_id == 4)
                                        {
                                            if ($create_task == $create_model)
                                            {
                                                $html ='<b class="btn btn-completed-tasks-project btn-sm counter btn-rounded-project" data-toggle="tooltip" title="'.Yii::t('app', 'Completed Task') .' "style="font-size:100%"> on time</b>';
                                                $donwloadDocumentsHtml .= '<li>'. $documents->name."<bR>".$html.'</li>';
                                            }
                                            elseif ($create_task > $create_model)
                                            {
                                                $html ='<b class="btn btn-delayed-tasks-project btn-sm counter btn-rounded-project" data-toggle="tooltip" title="'.Yii::t('app', 'Pending Task') .' "tyle="font-size:100%">delayed</b>';
                                                $donwloadDocumentsHtml .= '<li>'. $documents->name."<bR>".$html.'</li>';
                                            }
                                        }
                                        else
                                        {
                                            $html ='<b class="btn btn-pending-tasks-project btn-sm counter btn-rounded-project" data-toggle="tooltip" title="'.Yii::t('app', 'Pending Task') .' "tyle="font-size:100%">pending</b>';
                                                $donwloadDocumentsHtml .= '<li>'. $documents->name."<bR>".$html.'</li>';
                                        }
                                    }
                                    else
                                    {
                                        $days_begin = $documents->assignment_date;
                                        $days_work= $documents->task_length;
                                        $total_days_task = $days_begin + $days_work;
                                        $create_model = date('d-m-Y', $model->created_at);

                                        $today= date('m-d-Y');

                                        $due_date = date("m-d-Y",strtotime($create_model."+ ".$total_days_task." days"));

                                        if($due_date <= $today){

                                            $html ='<b class="btn btn-warning btn-sm counter btn-rounded-project" data-toggle="tooltip" title="'.Yii::t('app', 'No Task') .' "tyle="font-size:100%">No Task</b>';
                                                $donwloadDocumentsHtml .= '<li>'. $documents->name.", due date: ".$due_date."<bR>".$html.'</li>';
                                        } 

                                    }
                                    
                                }

                                $today= date('m-d-Y');
                                $today_converted = DateTime::createFromFormat('m-d-Y H:i:s', $today . ' 00:00:00');
                                $today_converted2 = $today_converted->getTimestamp();
                                $today_converted2 = Yii::$app->formatter->asTimestamp($today_converted2);


                                $projectTask = Company2ProjectTask::find()
                                        ->where(['project_id'=>$model->id])
                                        ->andWhere(['assigned_date'=> $today_converted2])
                                        ->one();

                                    $title = '<div class="row" style="border:0px">
                                        <div class="col-10">
                                            Tasks
                                        </div>
                                        <div class= "col-1 style="text-align:right">
                                            <span class="text-info"><strong><button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project" >X</button></strong></span> 
                                        </div>
                                    </div>';
                                    if($projectTask)
                                    {

                                        if($projectTask->task_status_id == 4)
                                        {
                                            
                                        return Html::a('<i class="dripicons-bell noti-icon" data-toggle="tooltip" data-placement="top" title="View Status Pending"></i>', 'javascript:void(0)', [
                                        'class'          => 'btn btn-sm btn-completed-tasks-project',
                                        //'class'          => '',
                                        'title'          => $title,
                                        'data-toggle'    => 'tooltip',
                                        'tabindex'       => '0',
                                        'data-container' => 'body',
                                        'data-toggle'    => 'popover',
                                        'data-trigger'   => 'click focus',
                                        'data-placement' => 'auto',
                                        'data-html'      => 'true',
                                        'data-content'   => $donwloadDocumentsHtml,
                                        ]);
                                        }
                                        elseif($projectTask->task_status_id != 4)
                                        {
                                            return Html::a('<i class="dripicons-bell noti-icon" data-toggle="tooltip" data-placement="top" title="View Status Pending"></i>', 'javascript:void(0)', [
                                        'class'          => 'btn btn-sm btn-custom btn-warning',
                                        //'class'          => '',
                                        'title'          => $title,
                                        'data-toggle'    => 'tooltip',
                                        'tabindex'       => '0',
                                        'data-container' => 'body',
                                        'data-toggle'    => 'popover',
                                        'data-trigger'   => 'click focus',
                                        'data-placement' => 'auto',
                                        'data-html'      => 'true',
                                        'data-content'   => $donwloadDocumentsHtml,
                                        ]);
                                        }
                                           
                                    }
                                    else 
                                    {

                                        $days_begin = $documents->assignment_date;
                                        $days_work= $documents->task_length;
                                        $total_days_task = $days_begin + $days_work;
                                        $create_model = date('d-m-Y', $model->created_at);

                                        $today= date('m-d-Y');

                                        $due_date = date("m-d-Y",strtotime($create_model."+ ".$total_days_task." days"));

                                        if($due_date <= $today){

                                        return Html::a('<i class="dripicons-bell noti-icon" data-toggle="tooltip" data-placement="top" title="View Status Pending"></i>', 'javascript:void(0)', [
                                        'class'          => 'btn btn-sm btn-custom btn-danger',
                                        //'class'          => '',
                                        'title'          => $title,
                                        'data-toggle'    => 'tooltip',
                                        'tabindex'       => '0',
                                        'data-container' => 'body',
                                        'data-toggle'    => 'popover',
                                        'data-trigger'   => 'click focus',
                                        'data-placement' => 'auto',
                                        'data-html'      => 'true',
                                        'data-content'   => $donwloadDocumentsHtml,
                                        ]);
                                        }
                                        else
                                        {

                                        return Html::a('<i class="dripicons-bell noti-icon" data-toggle="tooltip" data-placement="top" title="View Status Pending"></i>', 'javascript:void(0)', [
                                        'class'          => 'btn btn-sm btn-custom btn-warning',
                                        //'class'          => '',
                                        'title'          => $title,
                                        'data-toggle'    => 'tooltip',
                                        'tabindex'       => '0',
                                        'data-container' => 'body',
                                        'data-toggle'    => 'popover',
                                        'data-trigger'   => 'click focus',
                                        'data-placement' => 'auto',
                                        'data-html'      => 'true',
                                        'data-content'   => $donwloadDocumentsHtml,
                                         ]);

                                        } 

                                    }     
                                    
                                }
                                else
                                {
                                    return   "No task";

                            
                                }

                      
                    }
                ],
                       /* [
                            'attribute'      => 'due_date',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions'  => ['class' => 'text-center'],
                            'width' => '8%',
                            'label'          => Yii::t('app', 'Due Date'),
                            'format'         => ['date', 'php:m-d-Y'],
                        ],
                        */

                        [
                            'attribute'      => 'id_status',
                            'label'          => Yii::t('app', 'Status'),
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions'  => ['class' => 'text-center'],
                            'width' => '10%',
                            'content'        => function ($model) use ($project_status)

                            {   

                            if($model->is_active == 1){

                                switch ($model->projectStatus->name) {
                                    case "Pending":
                                        $classProjectStatus = "btn-pending-project";
                                        $classProjectStatus = "btn-pending-project";
                                        break;
                                    case "Denied":
                                        $classProjectStatus = "btn-denied-project";
                                        break;
                                    case "Contested":
                                        $classProjectStatus = "btn-contested-project";
                                        break;
                                    case "Approved":
                                        $classProjectStatus = "btn-approved-project";
                                        break;
                                    case "In-progress":
                                        $classProjectStatus = "btn-progress-project";
                                        break;
                                    case "Completed":
                                        $classProjectStatus = "btn-completed-project";
                                        break;
                                        case "Lost":
                                        $classProjectStatus = "btn-danger";
                                        break;
                                    default:
                                        $classProjectStatus = "btn-new-project";
                                }


                                $claimStatusHtml = Html::dropDownList('id_status', $model->id_status, $project_status, ['prompt' => 'Select','class' => 'form-control', 'onChange' => 'change_project_status2(' . $model->id . ',' . $model->rcv . ',

                                 this.value, 
                                 ' . $model->id_status . ', this)']);
                                

                            
                            if($model->id_status == 5 || $model->id_status == 6)
                            { 

                                    return Html::a('<i class="fas fa-pencil-alt"></i> '.$model->projectStatus->name, 'javascript:void(0)', [
                                    'class'          => 'btn btn-sm-project '.$classProjectStatus.' btn-popover-order btn-rounded-project',
                                    'title'          => 'Change Project Status &nbsp;&nbsp; <span class="text-info"><strong></strong></span> <button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project">X</button>',
                                    'data-toggle'    => 'tooltip',
                                    'tabindex'       => '0',
                                    'data-container' => 'body',
                                    'data-toggle'    => 'popover',
                                    'data-trigger'   => 'click focus',
                                    'data-placement' => 'button',
                                    'data-html'      => 'true',
                                    'data-close'      => 'true',
                                    'data-content'   => $claimStatusHtml,
                                ]);

                            } 

                             if ($model->id_status == 1) {
                                    return "<p class = 'btn btn-sm-project btn-new-project btn-popover-order btn-rounded-project'>New</div>";
                                }


                                if ($model->id_status == 2) {
                                    return "<p class = 'btn btn-sm-project btn-pending-project btn-popover-order btn-rounded-project'>Pending</div>";
                                }

                                if ($model->id_status == 7) {
                                    return "<p class = 'btn btn-sm-project btn-completed-project btn-popover-order btn-rounded-project'>Completed</div>";
                                }

                                if ($model->id_status == 8) {
                                    return "<p class = 'btn btn-sm-project btn-lost-project btn-popover-order btn-danger'>Lost</div>";
                                }
                            }else{

                                    return "<b>Inactive<br>Project</b>";
                                }
 

                       

                            }  

                        ],
                       /* [
                            'attribute'      => 'city_id',
                            'label'          => Yii::t('app', 'City'),
                            'contentOptions' => ['class' => ''],
                            'content'        => function ($model) {
                                return Html::a($model->city->name, ['zipcode/index', 'ZipcodeSearch[season_id]' => $model->season->id, 'ZipcodeSearch[region_id]' => $model->region->id, 'ZipcodeSearch[city_id]' => $model->city->id, 'ZipcodeSearch[is_active]' => 1, 'ad' => 'ad'], ['data-pjax' => '0', 'title' => Yii::t('app', ' View Active Zip Codes'), 'data-toggle' => 'tooltip']);
                            }
                        ],*/
                  
                        /*[
                            'attribute'      => 'user_id',
                            'label'          => Yii::t('app', 'Supervisor Name'),
                            'contentOptions' => ['class' => ''],
                            'content'        => function ($model) {
                                  return Html::a($model->staff->first_name.' '.$model->staff->last_name , ['/client/index', 'ClientSearch[season_id]' => $model->season->id, 'ClientSearch[user_id]' => $model->client->user->id, 'ClientSearch[is_active]' => '1', 'at' => 'at'], ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Clients'), 'data-toggle' => 'tooltip']) . ' ';
                            }
                        ],*/
                        
                   
                       /* [
                            'attribute'      => 'email',
                            'label'          => Yii::t('app', 'Client Contact'),
                            'contentOptions' => ['class' => ''],
                            'content' => function ($model) {

                                  return (Yii::$app->user->can('Staff1')) ? $model->client->name . ' ' . $model->client->last_name : Html::a($model->client->name . ' ' . $model->client->last_name, [$action . '/client/update', 'id' => $model->client->id], ['data-pjax' => '0']).
                                  "<br><font size=2>PH: </font>".$model->client->phone.
                                   "<br>".$model->client->email;


                            },
                        ],

                          [
                            'attribute'      => 'city_id',
                            'label'          => Yii::t('app', 'City'),
                            'contentOptions' => ['class' => ''],
                            'content' => function ($model) {

                                  return $model->map2;

                            },
                        ],*/
                        [
                            'class'          => 'yii\grid\ActionColumn',
                            'template'       => '<div class="btn-group btn-group-sm">{material}{affiliate}{project}{active}{update}{download}{print}{login_link}</div>',
                            'header'         => Yii::t('app', 'Actions'),
                            'headerOptions'  => ['class' => 'text-center'],
                            'contentOptions' => ['class' => 'text-center', 'style' => 'min-width:' . (Yii::$app->user->can('Company2 Staff') ? '90' : '90') . 'px'],
                            'visibleButtons' => [
                                'active'     => Yii::$app->user->can('Company2 Staff'),
                                'update'     => Yii::$app->user->can('Company2 Staff'),
                                'login_link' => (Yii::$app->user->can('AS Staff') || Yii::$app->user->can('Company2 Staff')),
                            ],


                         


                            'buttons'        => [
                                'active' => function ($url, $model) {
                                    return Html::a('<i class="fa ' . ($model->is_active == '1' ? "fa-check" : "fa-close") . '"></i>', 'javascript:void(0)', ['class' => 'btn btn-sm btn-custom ' . ($model->is_active == '1' ? "btn-success" : "btn-danger"), 'onclick' => 'change_status_project(' . $model->id . ', ' . ($model->is_active == '1' ? "0" : "1") . ')', 'title' => $model->projectReverseStatusName, 'data-toggle' => 'tooltip']);
                                }, 


                               'material' => function ($url, $model) {

                            if(Yii::$app->user->can('Company2 Staff') || Yii::$app->user->can('Company1 Staff')){

                                return Html::a('<i class="icon-materials-pallet"></i>', 
                                    ['/material-project/index-material', 
                                    'project_id' => $model->id,
                                    'project_pid' => $model->p_id,
                                    'asDialog' => '1'],
                                     ['class'       => 'btn btn-sm btn-custom btn-primary ','onclick'     => 'return showModal("' . Yii::t('app', 'ADD MATERIALS') . '", $(this).attr("href"), 600, 900); return false;',
                                     'title' => Yii::t('app', 'Add Material'),
                                     'data-toggle' => 'tooltip'
                                 ]);
                             }
                                },

                                'affiliate' => function ($url, $model) {

                            if(Yii::$app->user->can('Company2 Staff') || Yii::$app->user->can('Company1 Staff')){
                                
                                return Html::a('<i class="icon-contractor"></i>', 
                                    ['/project-affiliate/index-affiliate', 
                                    'project_id' => $model->id,
                                    'project_pid' => $model->p_id,
                                    'asDialog' => '1'],
                                     ['class'       => 'btn btn-sm btn-custom btn-primary ','onclick'     => 'return showModal("' . Yii::t('app', 'ASSIGN PROJECT') . '", $(this).attr("href"), 600, 900); return false;',
                                     'title' => Yii::t('app', 'Assign Projects'),
                                     'data-toggle' => 'tooltip'
                                 ]);
                            }
                                },

                                'update' => function ($url, $model) {

                                  
                                if($model->id_status == 5)
                                {
                                    return Html::a('<i class="fas fa-eye"></i>', ['view', 'id' => $model->id], ['class' => 'btn btn-sm btn-custom btn-primary', 'title' => Yii::t('app', 'View Projects'), 'data-pjax' => '0', 'data-toggle' => 'tooltip']);
                                }else{

                                  
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-custom btn-primary', 'title' => Yii::t('app', 'Update Projects'), 'data-pjax' => '0', 'data-toggle' => 'tooltip']);
                                }
                                },
                    'download' => function ($url, $model) {



                                    $base = common\models\AdministrationHelper::getSiteUrl();
                                    $base = "'".$base."'";
                                    $file = "'".$documents->file."'";

                        $claimTaskDocuments= ProjectsAttachments::find()
                        ->joinWith(company2ProjectTask)
                        ->where(['project_id'=>$model->id])
                        ->all();
                    

                                   $printFormsHtml = '<ul>';

                                    foreach ($claimTaskDocuments as $key => $documents) {

                                        $name_d = str_replace ( '_', ' ', $documents->file);

                                        if ($documents->file != "") {
                                            $file = "'" . $documents->file . "'";

                                            $printFormsHtml .=
                                                '
                                                <div class="row pop">
                                                    <div class="col-8">
                                                        <li><font size=2>'.$name_d.'</font>
                                                     </div>
                                                     <td class="col-4" align="right">' . Html::a('<i class="glyphicon glyphicon-eye-open"></i>', Url::to('#'), [ 'class' => 'btn btn-sm btn-custom btn-info','onclick' => 'hola(' . $file . ',' . $base . ')', 'data-pjax' => '0', 'title' => Yii::t('app', 'View Document'), 'data-toggle' => 'tooltip']) . Html::a('<i class="dripicons-download"></i>', ['company2-project-task/download-document', 'document' => $documents->file, 'name' => $documents->file], ['class' => 'btn btn-sm btn-custom btn-info','data-pjax' => '0', 'title' => Yii::t('app', 'Download Document'), 'data-toggle' => 'tooltip']) 
                                                  . '</div>
                                                </div>
                                                </li>';
                                        }
                                    }

                                    $title = '<div class="row" style="border:0px">
                                        <div class="col-10">
                                           View Documents
                                        </div>
                                        <div class= "col-1 style="text-align:right">
                                            <span class="text-info"><strong><button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project" >X</button></strong></span> 
                                        </div>
                                    </div>';

                                    $printFormsHtml .= '</ul>';

                                   
                    return Html::a('<i class="fa fa-download fa-2" data-toggle="tooltip" data-placement="top" title="View and Download Documents"></i>', 'javascript:void(0)', [
                                'class'          => 'btn btn-sm btn-custom btn-primary',
                                'title'          => $title,
                                    'data-toggle'    => 'tooltip',
                                    'tabindex'       => '0',
                                    'data-container' => 'body',
                                    'data-toggle'    => 'popover',
                                    'data-trigger'   => 'click focus',
                                    'data-placement' => 'auto',
                                    'data-html'      => 'true',
                                    'data-content'   => $printFormsHtml,
                                    ]);
                                },


                    'print' => function ($url, $model) {

                                $printFormsHtml =
                                    '<ul>'.
                                     '<li>'. Html::a('Inspection Form', 
                                                ['/print-form/inspectionform', 
                                                'project_id' => $model->id,'client_id'=>$model->client->id, 'type'=>'contractor'
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'Inspection Form'), 'data-toggle' => 'tooltip'
                                            ]) .
                                        '</li>'.
                                        '<li>' . Html::a('Service Agreement',
                                                [
                                                    '/print-form/index','id' => $model->id, 'user_id' => $model->user_id, 'form' => 11
                                                ],
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                            ]) . 
                                        '</li>' .
                                          '<li>' . Html::a(
                                            'Third Party Authorization',
                                            [
                                                '/print-form/index',
                                                'id' => $model->id, 'user_id' => $model->user_id, 'form' => 3
                                            ],
                                            ['data-pjax' => '0', 'title' => Yii::t('app', 'Third Party Authorization'), 'data-toggle' => 'tooltip']
                                        ) . '</li>' .
                                        '<li>'. Html::a('Material Selection', 
                                                [
                                                    '/print-form/index', 'id' => $model->id,'user_id'=>$model->user_id, 'form'=> '5'
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                            ]) .
                                        '</li>'.
                                        '<li>'. Html::a('Referral Form', 
                                                ['/print-form/index', 'id' => $model->id,'user_id'=>$model->user_id, 'form'=>9
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                            ]) .
                                        '</li>'. 
                                        /*'<li>'. Html::a('Lient Acknowledgement', 
                                                ['/print-form/index', 'id' => $model->id,'user_id'=>$model->user_id, 'form'=>8
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                            ]) .
                                        '</li>'. */
                                        '<li>'. Html::a('Certificate of Completion', 
                                                ['/print-form/index', 
                                                'id' => $model->id,'user_id'=>$model->user_id, 'form'=>7
                                                ], 
                                                ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip'
                                            ]) .
                                        '</li>'.

                                        '<li>' . Html::a(
                                            'Invoice',
                                            /*[
                                                '/print-form/index',
                                                'id' => $model->id, 'user_id' => $model->user_id, 'form' => 8
                                            ],
                                            ['data-pjax' => '0', 'title' => Yii::t('app', 'Invoice'), 'data-toggle' => 'tooltip']*/
                                        ) . '</li>' .
                                       
                                    '</ul>'; 
                               
                                  /* '<li>'.Html::a('Homeowner info', 
                                    ['/print-form/index', 
                                    'id' => $model->id,'user_id'=>$model->user_id, 'form'=>''], ['data-pjax' => '0','target' => '_blank', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip']) . '</li>'.

                                  '<li>'. Html::a('Claim Deductible', 
                                    ['/print-form/index', 
                                    'id' => $model->id,'user_id'=>$model->user_id, 'form'=>2], ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip']) .'</li>'. 
                                  '<li>'. Html::a('Third Party Authorization', 
                                    ['/print-form/index', 
                                    'id' => $model->id,'user_id'=>$model->user_id, 'form'=>3], ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip']) .'</li>'. 
                                  '<li>'. Html::a('Supplements Authorization', 
                                    ['/print-form/index', 
                                    'id' => $model->id,'user_id'=>$model->user_id, 'form'=>4], ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip']) .'</li>'. 

                                  '<li>'. Html::a('Material Selection', 
                                    ['/print-form/index', 
                                    'id' => $model->id,'user_id'=>$model->user_id, 'form'=>5], ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip']) .'</li>'. 
                                  '<li>'. Html::a('Affidavit', 
                                    ['/print-form/index', 
                                    'id' => $model->id,'user_id'=>$model->user_id, 'form'=>6], ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip']) .'</li>'. 

                                  '<li>'. Html::a('Certificate of Completion', 
                                    ['/print-form/index', 
                                    'id' => $model->id,'user_id'=>$model->user_id, 'form'=>7], ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip']) .'</li>'. 
                                  '<li>'. Html::a('Lient Acknowledgement', 
                                    ['/print-form/index', 
                                    'id' => $model->id,'user_id'=>$model->user_id, 'form'=>8], ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip']) .'</li>'. 
                                  '<li>'. Html::a('Referral Form', 
                                    ['/print-form/index', 
                                    'id' => $model->id,'user_id'=>$model->user_id, 'form'=>9], ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip']) .'</li>'. 

                                  '<li>'. Html::a('Services and Responsabilities', 
                                    ['/print-form/index', 
                                    'id' => $model->id,'user_id'=>$model->user_id, 'form'=>10], ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip']) .'</li>'. 
                                  '<li>' . Html::a(
                                            'Service Agreement',
                                            [
                                                '/print-form/index',
                                                'id' => $model->id, 'user_id' => $model->user_id, 'form' => 11
                                            ],
                                            ['data-pjax' => '0', 'title' => Yii::t('app', 'View Active Claims'), 'data-toggle' => 'tooltip']
                                        ) . '</li>' .

                                  '</ul>';*/

                                    $title = '<div class="row" style= "border:0px;">
                                        <div class="col-9">
                                            Create Documents
                                        </div>
                                        <div class= "col-1 style="text-align:right">
                                            <span class="text-info"><strong><button type="button" id="close-popover" class="close-popover btn btn-sm-project btn-close-project" >X</button></strong></span> 
                                        </div>
                                    </div>';

                                if(Yii::$app->user->can('Company2 Admin')){
                                   
                                    return Html::a('<i class="fas fa-file-signature fa-2" data-toggle="tooltip" data-placement="top" title="Create Documents"></i>', 'javascript:void(0)', [
                                    'class'          => 'btn btn-sm btn-custom btn-primary ',
                                    'title'          => $title,
                                    'data-toggle'    => 'tooltip',
                                    'tabindex'       => '0',
                                    'data-container' => 'body',
                                    'data-toggle'    => 'popover',
                                    'data-trigger'   => 'click focus',
                                    'data-placement' => 'auto',
                                    'data-html'      => 'true',
                                    'data-content'   => $printFormsHtml,
                                ]);
                                }

                                },
                                'login_link' => function ($url, $model) {
                                    return Html::a('<i class="fas fa-sign-in-alt"></i>', ['site/login-2', 'user_id' => $model->staff_id], ['data-method' => 'post', 'class' => 'btn btn-sm btn-custom btn-info', 'title' => Yii::t('app', 'Signin as this Project Supervisor'), 'data-pjax' => '0', 'data-toggle' => 'tooltip']);
                                }
                            ],
                        ],
                    ]
                ]); ?>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
function hola(a, d)
{
              //a is project_id and b = task_id c = type d = baseurl
              var window_width = 750;
              var window_height = 830;
              var newfeatures= 'scrollbars=yes,resizable=yes';
              var window_top = (screen.height-window_height)/2;
              var window_left = (screen.width-window_width)/2;

             
                newWindow=window.open(""+d+"/uploads/company2project-task-documents/"+a+""+'?dt=' + new Date().getTime(),'_blank', 'titulo','width=' + window_width + ',height=' + window_height + ',top=' + window_top + ',left=' + window_left + ',features=' + newfeatures + '','clearcache=yes').focus;
             
}
</script> 

<?php


$this->registerJs('


function change_claim_description(id,notes,status_obj)
{
    title_status = "' . Yii::t('app', 'Are you sure update this Note?') . '";

    var message = "";
    
    swal({
        title              : title_status,
        text               : message,
        type               : "warning",
        showCancelButton   : true,
        confirmButtonClass : "btn-warning",
        confirmButtonText  : "Yes",
        cancelButtonText   : "No",
        closeOnConfirm     : true
    },
    function(isConfirm)
    {
        if(isConfirm)
        {

                $.ajax({
                    type: "GET",
                    url: "' . Url::to(["/company2-project/change-project-notes"]) . '",
                    data: {"id":id, "notes":notes},
                    success: function(response)
                    {
                        $.pjax.reload("#company2-project-index", {timeout : false}).done(function(){
                            $.pjax.reload("#main-alert-widget", {timeout : false});
                        });
                    }
                });

        }
        else
        {
             setTimeout(function(){
                        window.location.reload(1);
                        }, 1000);    
        }
    });
}



function change_status_project(id,status)
{
    var confirm_message = (status == 1) ? "' . Yii::t('app-sweet_alert', 'You want to Activate this Project?') . '" : "' . Yii::t('app-sweet_alert', 'You want to Deactivate this Project?') . '";

    confirm_change_status("#company2-project-index", {
        "id"                          : id,
        "status"                      : status,
        "url"                         : "' . Url::to(["/company2-project/change-status"]) . '",
        "confirm_title"               : "' . Yii::t('app-sweet_alert', 'Are you sure?') . '",
        "activated_title"             : "' . Yii::t('app-sweet_alert', 'Project is Activated') . '",
        "deactivated_title"           : "' . Yii::t('app-sweet_alert', 'Project is Deactivated') . '",
        "confirm_message"             : confirm_message,
        "activated_success_message"   : "' . Yii::t('app-sweet_alert', 'Project Activated Successfully.') . '",
        "deactivated_success_message" : "' . Yii::t('app-sweet_alert', 'Project Deactivated Successfully.') . '",
    });
}

var projectJsStatusArray = ' . json_encode($project_status) . ';


var formatter = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",

});


function change_project_status2(id,rcv,  status,old_status,status_obj)
{
    rcv_claim = formatter.format(rcv); 

    if(projectJsStatusArray[status] == "Approved")
    {
        var message = "' . Yii::t('app', 'If you want to approve it, click yes, if not, go to update and update the amount of RCV.') . '";
        message += " `" + projectJsStatusArray[status] + "`";
        title_status = "Are you sure you want to approve the collection of this project based on this amount?  ";
        title_status += "" + rcv_claim + "";

    }
    else
    {
        var message = "' . Yii::t('app', 'to change the status of Project to') . '";
        message += " `" + projectJsStatusArray[status] + "`";
        title_status = "' . Yii::t('app', 'Are you sure?') . '";


    }

    swal({
        title              : title_status,
        text               : message,
        type               : "warning",
        showCancelButton   : true,
        confirmButtonClass : "btn-warning",
        confirmButtonText  : "Yes",
        cancelButtonText   : "No",
        closeOnConfirm     : true
    },
    function(isConfirm)
    {
        if(isConfirm)
        {

                $.ajax({
                    type: "GET",
                    url: "' . Url::to(["/company2-project/change-project-status"]) . '",
                    data: {"id":id, "status":status},
                    success: function(response)
                    {
                        $.pjax.reload("#company2-project-index", {timeout : false}).done(function(){
                            $.pjax.reload("#main-alert-widget", {timeout : false});
                        });
                    }
                });

        }
        else
        {
            $(status_obj).val(old_status);
        }
    });
}



function change_project_status(id, status,old_status,status_obj)
{
    var message = "' . Yii::t('app', 'to change the status of Project to') . '";
        message += " `" + projectJsStatusArray[status] + "`";

    swal({
        title              : "' . Yii::t('app', 'Are you sure?') . '",
        text               : message,
        type               : "warning",
        showCancelButton   : true,
        confirmButtonClass : "btn-warning",
        confirmButtonText  : "Yes",
        cancelButtonText   : "No",
        closeOnConfirm     : true
    },
    function(isConfirm)
    {
        if(isConfirm)
        {

                $.ajax({
                    type: "GET",
                    url: "' . Url::to(["/company2-project/change-project-status"]) . '",
                    data: {"id":id, "status":status},
                    success: function(response)
                    {
                        $.pjax.reload("#company2-project-index", {timeout : false}).done(function(){
                            $.pjax.reload("#main-alert-widget", {timeout : false});
                        });
                    }
                });

        }
        else
        {
            $(status_obj).val(old_status);
        }
    });
}

function changeIdStatus(id,idObj)
{
    var status = $(idObj).val();
    $(idObj).parent().find(".fa-spinner").remove();
    $(idObj).parent().append(\'<i class="fa fa-spinner fa-spin" style="font-size: 20px;"></i>\');
    $(idObj).hide();

    //var message = "' . Yii::t('app', 'Are you sure?') . '";

    //if(confirm(message))
    //{
        $.ajax({
            type: "GET",
            url: "' . Url::to(["company2-project/change-id-status"]) . '",
            data: {"id":id, "status":status},
            success: function(response)
            {
                $(idObj).val(($(idObj).val() == 1) ? 0 : 1);
                $(idObj).parent().find(".fa-spinner").remove();
                $(idObj).show();

                //$.pjax.reload("#company2-project-index", {timeout : false});
                $.pjax.reload("#main-alert-widget", {timeout : false})
            }
        });
    //}
}', View::POS_END);

$this->registerJs('
$(document).ajaxComplete(function() {
    $("div.popover.fade.show").remove();
    $(\'[data-toggle="popover"]\').popover();
});


$("#modal-box").on("hide.bs.modal", function (e) {
    $.pjax.reload("#company2-project-index", {timeout : false});
})

$(document).click(function(event) {
  //if you click on anything except the modal itself or the "open modal" link, close the modal
  if (!$(event.target).closest(".popover,.btn-popover-order").length) {
    //$(".popover").popover("hide");
  }
});
$(document).on("click",".close-popover",function(){
  $(".popover").popover("hide");
});

$(document).ready(
function(){
  $(".kv-expand-header-icon span").removeClass("glyphicon glyphicon-expand")

  $("thead tr th").off("click")
 
});
', View::POS_READY);