<!--
> Muaz Khan     - www.MuazKhan.com
> MIT License   - www.WebRTC-Experiment.com/licence
> Documentation - github.com/muaz-khan/RecordRTC
> and           - RecordRTC.org
-->
<?php

use Carbon\Carbon;
use common\models\CompanyDepartments;
use common\models\DepartmentStaff;
use common\models\ProjectsAttachments;
use common\models\Module;
use common\models\Company2ProjectTask;
use common\models\ProjectsMessages;
use common\models\User;
use kartik\file\FileInput;
use kartik\grid\GridView;
use kartik\select2\Select2;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use backend\components\UserHelpers;
use common\models\Task;


$base = common\models\AdministrationHelper::getSiteUrl();
$base = "'".$base."'";

?>
            <div class="btn-group col-md-12 col-lg-3" style="padding-right:0px; padding-left:0px; display:block; margin-left:0px;" id = "voice_text" align="center">
                <select class="recording-media form-control" style=" margin-right:0px; display:none;">
                    <option value="record-audio">Audio</option>
                    <option value="record-video">Video</option>
                    <option value="record-screen">Screen</option>
                </select>

                <select class="media-container-format form-control" style=" margin-right:0px; display:none;">
                    <option>WebM</option>
                    <option disabled>WAV</option>
                    <option disabled>Mp4</option>
                    <option disabled>Ogg</option>
                    <option>Gif</option>
                </select>

              

                <?php 
                    $company2project_task= Company2ProjectTask::find()->where(['project_id'=>$model->id])->one();

                    $valid = "delete_audio";

                        if ($company2project_task)
                        {
                            echo  Html::a('<i class="fas fa-trash-alt"></i>', ['/company2-project/deleteaudio', 'id' => $id, 'valid' => $valid ], ['id'=> 'delete-audio', 'style' => 'display:none; padding-top:5px; padding-bottom:5px; padding-right:10px; padding-left:10px; width:39px; margin:2px; ', 'class' => 'btn btn-danger', 'title' => Yii::t('app', 'Delete Audio'), 'data-pjax' => '0', 'data-toggle' => 'tooltip'])
                            .Html::a('<i class="fe-paperclip"></i>', Url::to('#'), [ 'style' => 'margin-right:3px;','class' => 'btn btn-light','onclick' => 'openUpdate2('.$company2project_task->id.','.$model->id.',39, 2,'.$base.')', 'data-pjax' => '0',  'title' => (Yii::t('app', 'Add Files')), 'data-toggle' => 'tooltip']) ;
                        }
                        else
                        {
                            echo Html::a('<i class="fas fa-trash-alt"></i>', ['/company2-project/deleteaudio', 'id' => $id,'valid' => $valid  ], ['id'=> 'delete-audio', 'style' => 'display:none;', 'class' => 'btn btn-light', 'title' => Yii::t('app', 'Delete Audio'), 'data-pjax' => '0', 'data-toggle' => 'tooltip'])."" ;
                        }
                ?>
                <?= Html::submitButton('<i class="fe-send"></i>', ['tittle' => 'Upload Audio','style' => 'padding-top:8px; padding-bottom:8px; padding-right:10px; padding-left:10px', 'id'=>"upload-to-server", 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success',]) ?>
                                                                    <!--<button type="submit" class="btn btn-success chat-send w-100"><i class="fe-send"></i></button>-->
            <span style="text-align: center; display: none;">
                <button style="text-align: center; display: none;"class="btn btn-light btn-md" id="save-to-disk">Save To Disk</button>
                <button style="text-align: center; display: none;"class="btn btn-light btn-md" id="open-new-tab">Open New Tab</button> 
                <!-- <button title = 'Upload Audio' data-toggle = 'tooltip' class="btn btn-primary btn-md" style="padding-top:7px; padding-bottom:7px; padding-right:14px; padding-left:14px" id="upload-to-server"><i class="fas fa-file-upload"></i></button> -->
            </span>

            
            <video style="text-align: center; display: none;"controls playsinline autoplay muted=false volume=1></video>
        </div>

       
