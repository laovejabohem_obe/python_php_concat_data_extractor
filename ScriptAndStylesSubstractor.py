import re
from pprint import pprint
from re import search


class ScriptAndStylesSubstractor:

    def __init__(self,path):
            self.path = path
    def run(self):
        self.getBufferedLines()
        return 0
    def camel_case(self,s):
        s = re.sub(r"(_|-)+", " ", s).title().replace(" ", "")
        return ''.join([s[0].lower(), s[1:]])
    def getBufferedLines(self):
        lines=[]
        with open(self.path,"r") as file:
                file_read = file.read()
                script= re.findall('(\<script\S*(?:.*?)\>(?:.|\n)*?\<\/\S*script\S*>)',file_read)
                style= re.findall('(\<style\S*(?:.*?)\>(?:.|\n)*?\<\/\S*style\S*>)',file_read)
                print("%(style_count)s Style group" % {'style_count':len(style)}) 
                print("%(script_count)s Scripts group" % {'script_count':len(script)}) 
                final_str= "//" + self.file_name
                for sc in script:
                      sc=re.sub('\<script\s*(.*?)>|\<\/script\s*(.*?)>','',sc)
                      if(len(sc) == 0):
                            print('importation script')
                      else:
                            final_str += sc
                final_str +='//' + self.file_name + " end"
                self.file_name_filtered=self.camel_case( self.folder_name + "-" + re.sub('\.php','',self.file_name) + '.js')
                print(self.file_name_filtered)
process = ScriptAndStylesSubstractor('/opt/lampp/htdocs/administrationservices/backend/views/print-form/project_form11.php')
process.folder_name="print-form"
process.file_name="index.php"
process.run()