import re
class InputAndDataInsertion:

    def __init__(self,path,inputsArr,inputsNames,allRegisteredJs):
        self.path=path
        self.inputsArr=inputsArr
        self.inputsNames=inputsNames
        self.allRegisteredJs=allRegisteredJs
    def createScriptContent(self):
        string_template=""
        first_passed = False
        for input_name in self.inputsNames:
            if input_name is None:
                continue
            if(first_passed == False):
                string_template += '\n\tvar %(input_name)s = (document.querySelector("#%(input_name)s")).value\n' % {'input_name' : input_name}
                first_passed=True
            else:
                string_template += '\tvar %(input_name)s = (document.querySelector("#%(input_name)s")).value\n' % {'input_name' : input_name}
            print("input_name")
            print(input_name)
            continue
        for registerJs in self.allRegisteredJs:
            registerJs = re.findall('[\'|\"]([\s\S]*)[\"|\']',registerJs)
            string_template += '\t'+registerJs[0] + '\n'
            inputs=""
            for input in self.inputsArr:
                inputs+=input+"\n"
        return inputs+"<script>" + string_template + "</script>"